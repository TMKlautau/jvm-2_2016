public class Teste_double{
	public static void main(String[] args){
		double x,y,z;
		int i;
		float a;
		long k;	
	//teste dadd para elementos sem sinal
			x=10.9909;
			y=7.83;
			z=x+y;
	//teste dadd para elementos com sinal
			x=10.9909;
			y=-7.83;
			z=x+y;
			z=y+x;
	// teste dadd para elementos com sinal negativo
			x=-10.9909;
			y=-7.83;
			z=x+y;
	// teste dsub para elementos com sinal negativo
			x=-10.9909;
			y=-7.83;
			z=x-y;
			z=y-x;
	// teste dsub para elementos com sinais diferentes
			x=10.9909;
			y=-7.83;
			z=x-y;
			z=y-x;	
	//teste ddiv para infinito com um double
			x=1.79;
			z=(x/0);
	//teste ddiv para infinito negativo com um double
			x=1.79;
			z=-x/0.0;
	//teste ddiv para NaN com um double
			x=1.79;
			z=(x/0);
			z=z/z;
	//teste dadd para inifito com um double
			x=1.79;
			z=(x/0);
			z=z+z;
	//teste dsub para inifito com um double NaN
			x=1.79;
			z=(x/0);
			z=z-z;
	//teste dmul de numeros sem sinal
			x=1.79;
			y=10.0;
			z=x*y;
	//teste dmul de numeros negativos
			x=-1.79;
			y=-10.0;
			z=x*y;
	//teste fmul de numeros de sinais diferentes
			x=-1.79;
			y=10.0;
			z=x*y;
	//teste dmul para inifito com um double
			x=1.79;
			z=(x/0);
			z=z*z;
	//teste fdiv de numeros positivos
			x=100.0;
			y=5.0;
			z=x/y;
	//teste fdiv de numeros negativos
			x=-100.0;
			y=-5.0;
			z=x/y;
	//teste fdiv de numeros de sinais diferentes
			x=100.0;
			y=-5.0;
			z=x/y;
	//teste fdiv de numeros positivos nao exata
			x=100.0;
			y=3.0;
			z=x/y;
	//teste f2d  fazendo cast para double
			a=195;
			x=(double)a;
	//teste l2d  fazendo cast para double
			k=195;
			x=(double)k;
	//teste i2d  fazendo cast para double
	//concertar ta bugado
			i=195;
			x=(double)i;
	//teste incremento
			x=1;
			x++;
	//teste decremento
			x=5;
			x--;
		
	}

}