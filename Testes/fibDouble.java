class fibDouble
{
	public static double fib(int n)
	{	
		double prev = 0, curr = 1;
		for (int i = 1; i < n; i++) {
			double tmp = curr;
			curr = prev + curr;
			prev = tmp;
		}
		return curr;
	}
	
	public static void main(String[] args)
	{
		System.out.println(fib(42));
	}
}