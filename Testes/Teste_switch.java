//teste para a instrucao switch
public class Teste_switch{
	public static void main(String[] args){
		int op;
		op=8;
		//teste table switch
		switch (op) {
            case 1:
                System.out.println("Domingo");
                break;
            case 2:
                System.out.println("Segunda-feira");
                break;
            case 3:
                System.out.println("Terça-feira");
                break;
            case 4:
                System.out.println("Quarta-feira");
                break;
            case 5:
                System.out.println("Quinta-feira");
                break;
            case 6:
                System.out.println("Sexta-feira");
                break;
             case 7:
                System.out.println("Sábado");
                break;
            default:
                 System.out.println("Este não é um dia válido!");
         }
		//teste lookup switch
		switch (op) {
            case 10:
                System.out.println("Domingo");
                break;
            case 100:
                System.out.println("Segunda-feira");
                break;
            case 1000:
                System.out.println("Terça-feira");
                break;
            case 10000:
                System.out.println("Quarta-feira");
                break;
            case 100000:
                System.out.println("Quinta-feira");
                break;
            case 1000000:
                System.out.println("Sexta-feira");
                break;
             case 10000000:
                System.out.println("Sábado");
                break;
            default:
                 System.out.println("Este não é um dia válido!");
         }
	}
	
}