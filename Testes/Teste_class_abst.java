//teste para classe abstrata
abstract class Animal {
    protected String nome;
    protected int numeroPatas;
    
    public abstract void som();

}

class Vaca extends Animal {
    public Vaca(){
        this.nome = "Vaca";
        this.numeroPatas = 4;
        
    }
    @Override    
    public void som(){
        System.out.println("A " + this.nome + " que tem " + this.numeroPatas + " patas, faz MUUUU");
    }

}
/*
class Gato extends Animal{
    public Gato(){
        this.nome = "Gato";
        this.numeroPatas = 4;
        
    }
    @Override    
    public void som(){
        System.out.println("O " + this.nome + " que tem " + this.numeroPatas + " patas, faz MIAU");
    }
}

class Carneiro extends Animal{
    public Carneiro(){
        this.nome = "Carneiro";
        this.numeroPatas = 4;
        
    }
    
    @Override
    public void som(){
        System.out.println("O " + this.nome + " que tem " + this.numeroPatas + " patas, faz BÉÉÉ");

}
}
*/

public class Teste_class_abst{
	public static void main(String[] args){
		Vaca mimosa = new Vaca();
        //Gato bichano = new Gato();
        //Carneiro barnabe = new Carneiro();
        
        mimosa.som();
        //bichano.som();
        //barnabe.som();
	}
}