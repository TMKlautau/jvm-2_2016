public class Teste_float{
	public static void main(String[] args){
		float x,y,z;
		int i,a,b;
		double k;
//teste fadd para elementos sem sinal
		x=10;
		y=7;
		z=x+y;
	//teste fadd para elementos com sinal
		x=10;
		y=-7;
		z=x+y;
		z=y+x;
	// teste fadd para elementos com sinal negativo
		x=-10;
		y=-7;
		z=x+y;
	// teste fsub para elementos com sinal negativo
		x=10;
		y=-7;
		z=x-y;
		z=y-x;
	// teste fsub para elementos com sinal negativo
		x=-10;
		y=-7;
		z=x-y;
		z=y-x;
	//teste ddiv para infinito com um float
		x=10;
		y=7;
		z=x+y;
		z=(x/0);
	//teste ddiv para infinito negativo com um float
		x=8;
		z=-x/0;
	//teste dadd para infinito com um float
		x=7;
		z=(x/0);
		z=z+z;
	//teste dsub para NaN com um float
		x=7;
		z=(x/0);
		z=z-z;
	//teste dmul para infinito com um float
		x=7;
		z=(x/0);
		z=z*z;
	//teste ddiv para NaN com um float
		x=7;
		z=(x/0);
		z=z/z;
	//teste fmul de numeros sem sinal
		x=100000000;
		y=7000;
		z=x*y;
	//teste fmul de numeros negativos
		x=-1000000000;
		y=-700;
		z=x*y;
	//teste fmul de numeros de sinais diferentes
		x=-100000000;
		y=7000;
		z=x*y;
		
		x=100000000;
		y=-7000;
		z=x*y;
	//teste fdiv de numeros positivos
		x=100;
		y=5;
		z=x/y;
	//teste fdiv de numeros negativos
		x=-100;
		y=-5;
		z=x/y;
	//teste fdiv de numeros positivos nao exata
		x=100;
		y=3;
		z=x/y;
	//teste fdiv de inteiro fazendo cast para float i2f
		a=8;
		b=3;
		z=(float)(a/b);
	//teste fdiv de float e fazendo cast para double f2d
		x=10;
		y=3;
		k=(double)(x/y);
	//teste fazendo cast de double para float d2f
		//erro na conversao para float
		k=10.987;
		x=(float)(k);
	//iic incremento
		x=1;
		x++;
	// decremento
		x=5;
		x--;
		
		
		
	}

}