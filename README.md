------------------------------------------------------------------------

Atualmente version 1.0 feito no refactoring (participaram do refactoring: Tito (código) , Alexandro (testes e suporte para o código))

 Tito Marcos de Moraes Klautau - 11/0142390 - (código) 
 Alexandro G. da R. Gonçalves - 10/0023967 - (testes e suporte para o código)

Buglist:

multianewarray não funciona direito, verificar

jvm entrou em loop, ao testar a classe abstrata

 -----------------------------------------------------------------------------

Informações pertinentes:

DEBUGMODE encontra no arquivo toolbox.cpp, está setado em 1, para desabilitar coloque como 0 e recompile

DEBUGMODE coloca o seguinte header para cada instrução, seguido de informações sobre o que ela efetuou

std::cout << std::endl << " - CP - OPCODE -  NOME DA INSTRUCAO -- linha: LINHA EM QUE ELA ESTA NO ARQUIVO RuntimeEngine.cpp" << std::endl << std::endl;

todos os testes foram efetuados em java version "1.8.0_111"

Link para o repositorio: https://bitbucket.org/TMKlautau/jvm-2_2016

referencia utilizada para fazer as instruções: https://docs.oracle.com/javase/specs/jvms/se7/html/jvms-6.html

link para a pasta no drive com os testes criados pelo alexandro: https://drive.google.com/open?id=0B_CFKHzEbrsuX0x3UUczWE55SWs

-------------------------------------------------------------------------------------------------------------

Comando para compilar no G++ (coloque todos os arquivos .h e .cpp da pasta Source em uma unica pasta):

g++ -o JVM_2_2016 Main.cpp Attributes.cpp ClassReader.cpp ConstantPool.cpp FieldAndMethod.cpp Frame.cpp MethodArea.cpp RuntimeEngine.cpp toolbox.cpp

------------------------------------------------------------------------------------

Como utilizar:

o programa pode ser chamado diretamente na linha de comando

"nome do programa" "nome do arquivo .class" "modo"

modo pode ser 1 para o exibidor, 2 para executar

assim para executar o arquivo fatorial.class com o programa nomeado JVM_2_2016.exe o comando seria:

	JVM_2_2016.exe fatorial.class 2

------------------------------------------------------------------------------------

status no aprender: enviado

ultima modificação: quinta, 5 Jan 2017, 15:58

----------------------------------------------------------------------------------------