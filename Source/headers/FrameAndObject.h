/*
 * Frame.h
 *
 *  Created on: Dec 7, 2016
 *      Author: TitoKlautau
 */

#ifndef SOURCE_HEADERS_FRAMEANDOBJECT_H_
#define SOURCE_HEADERS_FRAMEANDOBJECT_H_

#include "ClassReader.h"
#include <stack>
	/**
     * @brief Classe Frame
     */
class Frame{
public:
	/**
     * @brief O Frame
     * @param Recebe como paramêtros o nome do Método, o nome da classe e o index da classe
     */
	Frame(std::string methodName, ClassReader* className, int objref);

	
    /**
     * @brief Declaração das variáveis usadas na classe Frame.
     */
	std::string bytecode;
	int cp;
	unsigned short maxSizeStack;
	unsigned short maxSizeLocalVariable;
	std::stack<int> operandStack;
	std::vector<int> arrayLocalVariable;
	std::vector<ObjConstantPool*>* constantPool;

};

class FieldObj{
public:

	int accessFlags;

	std::string name;

	std::string descriptor;

	int value[2];

};

class Object{
public:

	int classIndex;

	std::vector<FieldObj> fieldArray;

};

#endif /* SOURCE_HEADERS_FRAMEANDOBJECT_H_ */
