/*
 * MethodArea.h
 *
 *  Created on: Dec 7, 2016
 *      Author: TitoKlautau
 */

#ifndef SOURCE_HEADERS_METHODAREA_H_
#define SOURCE_HEADERS_METHODAREA_H_

#include "FrameAndObject.h"
#include "ClassReader.h"
#include <stack>

/*
 * Define a �rea de metodos da jvm, esta � unica.
 */
 
    
class MethodArea{
public:
   /**
     * @brief Obter a �nica inst�ncia do MethodArea.
     * @param classFileName
     */
	void insertNewClass(std::string classFileName);
    /**
     * @brief Declara��o das vari�veis usadas na classe MethodArea.
     */
	std::vector<ClassReader*> classArray;

};


#endif /* SOURCE_HEADERS_METHODAREA_H_ */
