/*
 * ClassReader.h
 *
 *  Created on: Nov 2, 2016
 *      Author: TitoKlautau
 */

#ifndef CLASSREADER
#define CLASSREADER

#include <fstream>
#include <bitset>
#include <vector>
#include "ConstantPool.h"
#include "toolbox.h"
#include "Attributes.h"
#include "FieldAndMethod.h"

/*
 *  ClassReader:
 *    public methods:
 * 		int loadClassFileInfo(std::string fileNameIn):
 * 			recebe a string do arquivo .class a ser aberto e o carrega nas variaves da classe
 * 			caso o valor retornado seja:
 * 				0  -- nenhum erro
 * 				1  -- o arquivo informado n�o pode ser aberto
 * 				2  -- o arquivo informado n�o � .class, n�o possui o magicNumber
 * 				<0 -- ocorreu um erro no load, quando ocorreu o erro encontra-se na variavel status flag
 *
 *
 *		void dumpClassFileInfo() :
 *			printa no command prompt as informa��es sobre o arquivo
 *
 *		void deleteClassFileInfo() :
 *			deleta as informa��es carregadas com loadClassFileInfo :
 *
 *		private methods:
 *		int loadClassFile(std::string fileName);
 *
 *			int getConstantPoolCount();
 *
 *			int loadConstantPoolArray();
 *
 *			int loadAccessFlags();
 *
 *			int loadThisClassIndex();
 *
 *			int loadSuperClassIndex();
 *
 *			int loadInterfaceCount();
 *
 *			int loadInterfaceArray();
 *
 *			int loadFieldCount();
 *
 *			int loadFieldArray();
 *
 *			int loadMethodCount();
 *
 *			int loadMethodArray();
 *
 *			int loadAttributeCount();
 *
 *			int loadAttributeArray();
 *
 *			int closeClassFile();
 *
 *
 */
    /**
     * @brief A classe Reader � respons�vel por ler o .class
     */
class ClassReader{
public:
    /**
     * @brief Carrega o nome do arquivo.
     */
	int loadClassFileInfo(std::string fileNameIn);
    /**
     * @brief Carrega as informa��es da classe.
     */
	void dumpClassFileInfo();
    /**
     * @brief Deleta as informa��es da classe.
     */
	void deleteClassFileInfo();

//private:
    /**
     * @brief Objetos da atributos.
     */
	std::vector<ObjAttribute*> loadObjAttributeArray(unsigned int attributesCountaux);
    /**
     * @brief Deleta o nome do arquivo.
     */
	int loadClassFile(std::string fileName);
    /**
     * @brief Implementa o contador de ConstantPoll.
     */
	int getConstantPoolCount();
   /**
     * @brief Carrega o vetor de ConstantPool.
     */
	int loadConstantPoolArray();
     /**
     * @brief Carrega as Flags de acesso.
     */
	int loadAccessFlags();
    /**
     * @brief Carrega os indices da Classe.
     */
	int loadThisClassIndex();
    /**
     * @brief Carrega os indices da Super Classe.
     */
	int loadSuperClassIndex();
    /**
     * @brief Carrega o vetor de Interfaces.
     */

	int loadInterfaceCount();
    /**
     * @brief Carrega o contador de Campos.
     */
	int loadInterfaceArray();
    /**
     * @brief Carrega ao vetor de Campos.
     */
	int loadFieldCount();
    /**
     * @brief Carrega o contador de M�todos.
     */
	int loadFieldArray();
    /**
     * @brief Carrega o vetor de M�todos.
     */
	int loadMethodCount();
    /**
     * @brief Carrega o vetor de M�todos.
     */
	int loadMethodArray();
   /**
     * @brief Carrega o contador de Atributos.
     */
	int loadAttributeCount();
    /**
     * @brief Carrega o vetor de Atributos.
     */
    
	int loadAttributeArray();
 /**
     * @brief Fecha o arquivo .class.
     */
	int closeClassFile();
     /**
     * @brief Carrega os elementos do vetor de atributos
     * @param contador de atributos
     * @param vetor de atributos do tipo objeto de atributos
     */
	void dumpAttributeArray(unsigned short attributeCountIn, std::vector<ObjAttribute*> attributeArrayIn);
    /**
     * @brief Carrega o Byte code
     * @param bytecode
     */
	void dumpBytecode(std::string bytecode);
    /**
     * @brief Declara��o das vari�veis usadas na classe ClassReader.
     */
	short statusFlag;

	unsigned int versionNumber;

	std::vector<ObjConstantPool*> constantPoolArray;

	unsigned short constPoolCount;

	unsigned short accessFlags;

	unsigned short thisClassIndex;

	unsigned short superClassIndex;

	unsigned short interfaceCount;

	std::vector<unsigned short> interfaceArray;

	unsigned short fieldCount;

	std::vector<field_method> fieldArray;

	unsigned short methodCount;

	std::vector<field_method> methodArray;

	unsigned short attributeCount;

	std::vector<ObjAttribute*> attributeArray;

	std::ifstream classFile;

};

#endif
