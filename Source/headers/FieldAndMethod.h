/*
 * FieldAndMethod.h
 *
 *  Created on: Nov 6, 2016
 *      Author: TitoKlautau
 */

#ifndef SOURCE_HEADERS_FIELDANDMETHOD_H_
#define SOURCE_HEADERS_FIELDANDMETHOD_H_

#include "Attributes.h"
#include <vector>

	/**
     * @brief Classe field_method.
     */
class field_method{


public:

	/**
     * @brief Declaração do Método setValues.
     * @param accessFlagsIn
     * @param nameIndexIn
     * @param descriptorIndexIn
     * @param attributesCountIn
     * @param <ObjAttribute*> attributesArrayIn
     */
	void setValues(unsigned short accessFlagsIn, unsigned short nameIndexIn, unsigned short descriptorIndexIn, unsigned short attributesCountIn, std::vector<ObjAttribute*> attributesArrayIn);

	/**
     * @brief Método que pega A flag de acesso.
     */
	unsigned short getAccessFlags();
	/**
     * @brief Método que pega o index do nome.
     */
	unsigned short getNameIndex();
	/**
     * @brief Método que pega o index do descritor.
     */
	unsigned short getDescriptorIndex();
	/**
     * @brief Método que pega o contador de atributos.
     */
	unsigned short getAttributesCount();
	/**
     * @brief Método que pega o contador de atributos.
     */
	std::vector<ObjAttribute*> getAttributesArray();

	int value[2];

private:
	/**
     * @brief Declaração das variáveis usadas na classe.
     */
	unsigned short accessFlags;

	unsigned short nameIndex;

	unsigned short descriptorIndex;

	unsigned short attributesCount;

	std::vector<ObjAttribute*> attributesArray;

};

#endif /* SOURCE_HEADERS_FIELDANDMETHOD_H_ */
