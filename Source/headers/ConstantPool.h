/*
 * ConstantPool.h
 *
 *  Created on: Nov 2, 2016
 *      Author: TitoKlautau
 */

#ifndef SOURCE_HEADERS_CONSTANTPOOL_H_
#define SOURCE_HEADERS_CONSTANTPOOL_H_

#include <iostream>

    /**
     * @brief  A Constant Pool é o local onde todas as constantes de uma classe se encontra.
     * @brief   Todo Int, Float, String, Referência de classe que a sua classe utilizar, terão um registro nessa estrutura.
     */



class ObjConstantPool{
public:
    /**
     * @brief tag da constant pool
     */
	int tag;
	/**
     * @brief posição da constant pool
     */
	int position;
};

class StringConstantPool: public ObjConstantPool{
public:
    /**
     * @brief Construtor para string.
     * @param pos
     * @param strIn
     */
	StringConstantPool(int pos, std::string strIn);

	std::string str;
};

class IntConstantPool: public ObjConstantPool{
public:
    /**
     * @brief Construtor para int.
     * @param pos
     * @param valueIn
     */
	IntConstantPool(int pos, int valueIn);
	int value;
};

class FloatConstantPool: public ObjConstantPool{
public:
    /**
     * @brief Construtor para float.
     * @param pos
     * @param valueIn
     */
	FloatConstantPool(int pos, float valueIn);
	float value;
};

class LongConstantPool: public ObjConstantPool{
public:
    /**
     * @brief Construtor para long.
     * @param pos
     * @param valueIn
     */
	LongConstantPool(int pos, long long valueIn);
	long long value;
};

class DoubleConstantPool: public ObjConstantPool{
public:
    /**
     * @brief Construtor para double.
     * @param pos
     * @param valueIn
     */
	DoubleConstantPool(int pos, double valueIn);
	double value;
};

class ClassRefConstantPool: public ObjConstantPool{
public:
    /**
     * @brief Construtor para referencias.
     * @param pos
     * @param valueIn
     */
	ClassRefConstantPool(int pos, unsigned short nameIndexIn);
	unsigned short nameIndex;
};

class StringRefConstantPool: public ObjConstantPool{
public:
    /**
     * @brief Construtor para referencias de strings.
     * @param pos
     * @param strIndexIn
     */
	StringRefConstantPool(int pos, unsigned short strIndexIn);
	unsigned short strIndex;

	bool init;

	std::string str;
};

class FieldRefConstantPool: public ObjConstantPool{
public:
    /**
     * @brief Construtor para referencias de fields.
     * @param pos
     * @param classRefIndexIn
     * @param nameTypeIndexIn
     */
	FieldRefConstantPool(int pos, unsigned short classRefIndexIn, unsigned short nameTypeIndexIn);
	unsigned short classRefIndex;
	unsigned short nameTypeIndex;
};

class MethodRefConstantPool: public ObjConstantPool{
public:
    /**
     * @brief Construtor para referencias de metodos.
     * @param pos
     * @param classRefIndexIn
     * @param nameTypeIndexIn
     */
	MethodRefConstantPool(int pos, unsigned short classRefIndexIn, unsigned short nameTypeIndexIn);
	unsigned short classRefIndex;
	unsigned short nameTypeIndex;
};

class InterMethodRefConstantPool: public ObjConstantPool{
public:
    /**
     * @brief Construtor para referencias de interfaces.
     * @param pos
     * @param classRefIndexIn
     * @param nameTypeIndexIn
     */
	InterMethodRefConstantPool(int pos, unsigned short classRefIndexIn, unsigned short nameTypeIndexIn);
	unsigned short classRefIndex;
	unsigned short nameTypeIndex;
};

class NameTypeConstantPool: public ObjConstantPool{
public:
    /**
     * @brief Construtor para nome de tipo.
     * @param pos
     * @param nameIndexIn
     * @param typeIn
     */
	NameTypeConstantPool(int pos, unsigned short nameIndexIn, unsigned short typeIn);
	unsigned short nameIndex;
	unsigned short type;
};

class HandleConstantPool: public ObjConstantPool{
public:
    /**
     * @brief Construtor para o handle.
     * @param pos
     * @param typeIn
     * @param indexIn
     */
	HandleConstantPool(int pos, char typeIn, unsigned short indexIn);
	char type;
	unsigned short index;
};

class TypeConstantPool: public ObjConstantPool{
public:
    /**
     * @brief Construtor para o tipo.
     * @param pos
     * @param typeIn
     * @param indexIn
     */
	TypeConstantPool(int pos, unsigned short indexIn);

	unsigned short index;
};

class InvokeDynamicConstantPool: public ObjConstantPool{
public:
    /**
     * @brief Construtor para o invoke dinamico.
     * @param pos
     * @param bootstrapMethodIndexIn
     * @param nameTypeIndexIn
     */
	InvokeDynamicConstantPool(int pos, unsigned short bootstrapMethodIndexIn, unsigned short nameTypeIndexIn);
	unsigned short bootstrapMethodIndex;
	unsigned short nameTypeIndex;
};

#endif /* SOURCE_HEADERS_CONSTANTPOOL_H_ */
