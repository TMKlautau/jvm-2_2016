/*
 * toolbox.h
 *
 *  Created on: Nov 5, 2016
 *      Author: TitoKlautau
 */

#ifndef SOURCE_HEADERS_TOOLBOX_H_
#define SOURCE_HEADERS_TOOLBOX_H_

#include <string>

/*
 * DEBUGMODE 1 para colocar em debugmode, 0 para execu��o sem feedback
 */
#define DEBUGMODE 1

/**
     * @brief Declaração do Método usado em toolbox.
     * @param strBufferIn
     * @return decodeUtf8Java
     */

std::string decodeUtf8Java(std::string strBufferIn);

void copyBitsetDword(char* to, char* from);

void splitQWord(char* quadword, char* upperAux, char* lowerAux);

void uniteDWords(char* quadword, char* upperAux, char* lowerAux);

#endif /* SOURCE_HEADERS_TOOLBOX_H_ */
