/*
 * Attributes.h
 *
 *  Created on: Nov 6, 2016
 *      Author: TitoKlautau
 */

#ifndef SOURCE_HEADERS_ATTRIBUTES_H_
#define SOURCE_HEADERS_ATTRIBUTES_H_

#include <iostream>
#include <vector>
	/**
     * @brief Classe de atributos.
     */
class ObjAttribute{
public:
	/**
     * @brief Método que retirna um objeto de atributo.
     * @param nameIndexIn
     * @param infoLengthIn
     * @param infoIn
     * @return ObjAttribute
     */
	ObjAttribute(unsigned short nameIndexIn, unsigned int infoLengthIn, std::string  infoIn);
	/**
     * @brief Método que pega o index do nome.
     */

	unsigned short getNameIndex();
	
	/**
     * @brief Método que pega o tamanho de info.
     */
	unsigned int getInfoLength();
	/**
     * @brief Método que pega a informação.
     */
	std::string getInfo();


protected:
	/**
     * @brief Declaração de variáveis.
     */
	unsigned short nameIndex;
	unsigned int infoLength;
	std::string info;

};

	/**
     * @brief Classe para atributos constantes.
     */
class ConstantValueAttribute: public ObjAttribute{
public:
	
	 /**
     * @brief Método que devolve um atributo constante
     *@param nameIndexIn.
     * @param infoLengthIn
     * @param  infoIn
     * @param constantIndexIn
     * @return ConstantValueAttribute
     */

	ConstantValueAttribute(unsigned short nameIndexIn, unsigned int infoLengthIn, std::string  infoIn, unsigned short constantIndexIn);
	/**
     * @brief Método que pega o index de uma constante
     * @return short
     */
	unsigned short getConstantIndex();

private:
	/**
     * @brief Declação de variáveis
     *
     */
	unsigned short constantIndex;

};
	/**
     * @brief Declação da Classe ObjExceptionTable
     *
     */

class ObjExceptionTable{
public:
	/**
    * @brief Método que devolve um objeto de tabela de exceção
    * @param startPcIn
    * @param endPcIn
    * @param handlerPcIn
    * @param catchTypeIn
    * @return ObjExceptionTable
    */
	ObjExceptionTable(unsigned short startPcIn ,unsigned short endPcIn, unsigned short handlerPcIn, unsigned short catchTypeIn);

	/**
    * @brief Declaração de variáveis
    *
    */
	unsigned short startPc;
	unsigned short endPc;
	unsigned short handlerPc;
	unsigned short catchType;

};
	/**
    * @brief Declaração da classe CodeAttribute
    *
    */

class CodeAttribute: public ObjAttribute{
public:
	/**
    * @brief Método que devolve um codigo do atributo
    * @param nameIndexIn
    * @param infoLengthIn
    * @param infoIn
    * @param maxStackIn
    * @param maxLocalsIn
    * @param maxLocalsIn
    * @param codeArrayCountIn
    * @param codeArrayIn
    * @param exceptionTableCountIn
    * @return CodeAttribute
    */
	CodeAttribute(unsigned short nameIndexIn, unsigned int infoLengthIn, std::string  infoIn, unsigned short maxStackIn, unsigned short maxLocalsIn, unsigned int codeArrayCountIn, std::string codeArrayIn, unsigned short exceptionTableCountIn, std::vector<ObjExceptionTable> exceptionTableArrayIn, unsigned short attributesCountIn, std::vector<ObjAttribute*> attributesArrayIn);
	/**
    * @brief método que retorna o tamamho maximo da stack
    *
    */

	unsigned short getMaxStack();
	/**
    * @brief Métod que retorna o maximo de locais
    *
    */

	unsigned short getMaxLocals();
	/**
    * @brief Métod que conta o número codigos no arrayCount
    *
    */
	unsigned int getCodeArrayCount();
	/**
    * @brief Método que devolve o um vetor de codigos
    *
    */
	std::string getCodeArray();
/**
    * @brief Método que devolve o uma excessão
    *
    */

	unsigned short getExceptionTableCount();
	
	/**
    * @brief Método que devolve o um vetor
    *
    */
	std::vector<ObjExceptionTable> getExceptionTableArray();
	/**
    * @brief Método que devolve a contagem dos atributos
    *
    */
	unsigned short getAttributesCount();
	/**
    * @brief Método que devolve o um vetor de objetos de atributos
    *
    */
	std::vector<ObjAttribute*> getAttributesArray();

private:
	 /**
    * @brief Declaração de variáveis
    *
    */
	unsigned short maxStack;

	unsigned short maxLocals;

	unsigned int codeArrayCount;

	std::string codeArray;

	unsigned short exceptionTableCount;

	std::vector<ObjExceptionTable> exceptionTableArray;

	unsigned short attributesCount;

	std::vector<ObjAttribute*> attributesArray;


};

class ObjVerificationTypeItems{
	 /**
    * @brief Declaração de variáveis
    *
    */
	unsigned char tag;
	unsigned short info;

};

class ObjStackMapTable{
	 /**
    * @brief Declaração de variáveis
    *
    */
	unsigned char  frameType;
	unsigned short offsetDelta;
	unsigned short localsCount;
	std::vector<ObjVerificationTypeItems> localsArray;
	unsigned short stackItensCount;
	std::vector<ObjVerificationTypeItems> stackItensArray;
};

class StackMapTableAttribute: public ObjAttribute{
	 /**
    * @brief Declaração de variáveis
    *
    */
	unsigned short entriesCount;
	std::vector<ObjStackMapTable*> entriesArray;


};

class ExceptionsAttribute: public ObjAttribute{
	 /**
    * @brief Declaração de variáveis
    *
    */
	unsigned short exceptionsCount;
	std::vector<unsigned short> exceptionsIndexArray;

};

class ObjInnerClass{
	 /**
    * @brief Declaração de variáveis
    *
    */
	unsigned short innerClassIndex;
	unsigned short outerClassIndex;
	unsigned short innerNameIndex;
	unsigned short innerClassAccessFlags;

};

class InnerClassesAttribute: public ObjAttribute{
	 /**
    * @brief Declaração de variáveis
    *
    */
	unsigned short innerClassCount;
	std::vector<ObjInnerClass> innerClassArray;

};

class EnclosingMethodAttribute: public ObjAttribute{
	 /**
    * @brief Declaração de variáveis
    *
    */
	unsigned short classIndex;
	unsigned short methodIndex;

};

class SyntheticAttribute: public ObjAttribute{
 //vazio
};

class SignatureAttribute: public ObjAttribute{
	 /**
    * @brief Declaração de variáveis
    *
    */
	unsigned short signatureIndex;

};

class SourceFileAttribute: public ObjAttribute{
public:
	
	SourceFileAttribute(unsigned short nameIndexIn, unsigned int infoLengthIn, std::string  info, unsigned short sourceFileIndexIn);

	unsigned short getSourceFileIndex();

private:
	unsigned short sourceFileIndex;

};

class SourceDebugExtensionAttribute: public ObjAttribute{
	 /**
    * @brief Declaração de variáveis
    *
    */
	std::string debugInfo;

};

class ObjLineNumberTable{
	 /**
    * @brief Declaração de variáveis
    *
    */
	unsigned short startPC;
	unsigned short lineNumber;

};

class LineNumberTableAttribute: public ObjAttribute{
	 /**
    * @brief Declaração de variáveis
    *
    */
	unsigned short lineNumberTableCount;
	std::vector<ObjLineNumberTable> lineNumberTableArray;

};

class ObjLocalVariableTable{
	 /**
    * @brief Declaração de variáveis
    *
    */
	unsigned short startPC;
	unsigned short length;
	unsigned short descriptorIndex;
	unsigned short index;
};

class LocalVariableTableAttribute: public ObjAttribute{
	 /**
    * @brief Declaração de variáveis
    *
    */
	unsigned short localVariableTableCount;
	std::vector<ObjLocalVariableTable> localVariableTableArray;

};

class LocalVariableTypeTableAttribute: public ObjAttribute{
	 /**
    * @brief Declaração de variáveis
    *
    */
	unsigned short localVariableTableCount;
	std::vector<ObjLocalVariableTable> localVariableTableArray;

};

class DeprecatedAttribute: public ObjAttribute{
 //vazio
};

class RuntimeVisibleAnnotationsAttribute: public ObjAttribute{
 //implementar depois
};

class RuntimeInvisibleAnnotationsAttribute: public ObjAttribute{
 //implementar depois
};

class RuntimeVisibleParameterAnnotationsAttribute: public ObjAttribute{

};

class RuntimeInvisibleParameterAnnotationsAttribute: public ObjAttribute{

};

class AnnotationDefaultAttribute: public ObjAttribute{

};

class ObjBootstrapMethod{
	 /**
    * @brief Declaração de variáveis
    *
    */
	unsigned short bootstrapMethodRef;
	unsigned short bootstrapArgumentCount;
	std::vector<unsigned short> bootstrapArgumentArray;

};

class BootstrapMethodsAttribute: public ObjAttribute{
	 /**
    * @brief Declaração de variáveis
    *
    */
	unsigned short bootstrapMethodsCount;
	std::vector<ObjBootstrapMethod> bootstrapMethodsArray;

};

#endif /* SOURCE_HEADERS_ATTRIBUTES_H_ */
