/*
 * ClassReader.cpp
 *
 *  Created on: Nov 2, 2016
 *      Author: TitoKlautau
 */

#include "ClassReader.h"

int ClassReader::loadClassFileInfo(std::string fileNameIn){
	return  this->loadClassFile(fileNameIn)
			+3
			* this->getConstantPoolCount()
			* this->loadConstantPoolArray()
			* this->loadAccessFlags()
			* this->loadThisClassIndex()
			* this->loadSuperClassIndex()
			* this->loadInterfaceCount()
			* this->loadInterfaceArray()
			* this->loadFieldCount()
			* this->loadFieldArray() + this->loadMethodCount()
			* this->loadMethodArray()
			* this->loadAttributeCount()
			* this->loadAttributeArray()
			* this->closeClassFile();
}


std::vector<ObjAttribute*> ClassReader::loadObjAttributeArray(unsigned int attributesCountaux){

	std::vector<ObjAttribute*> attributesArrayaux;

	for(unsigned short j = 0; j < attributesCountaux; j++){

		char auxuchar;
		char auxchar[2];

		unsigned short nameIndexattaux;
		unsigned int infoLengthattaux;

		char* auxshort = ( char* ) &nameIndexattaux;
		for(int k=0; k<2; k++){
			this->classFile.read(&auxchar[k], sizeof(char));
		}
		auxshort[0] = auxchar[1];
		auxshort[1] = auxchar[0];

		char auxcharint[4];
		char* auxint = ( char* ) &infoLengthattaux;
		for(int k=0; k<4; k++){
			this->classFile.read(&auxcharint[k], sizeof(char));
		}
		auxint[0] = auxcharint[3];
		auxint[1] = auxcharint[2];
		auxint[2] = auxcharint[1];
		auxint[3] = auxcharint[0];

		std::string infoattaux = "";

		std::string typeOfAtt = static_cast<StringConstantPool*>(this->constantPoolArray[nameIndexattaux-1])->str;

		if(typeOfAtt == "ConstantValue"){

			unsigned short constantIndexAux;

			char* auxshort = ( char* ) &constantIndexAux;
			for(unsigned int k = 0; k < 2; k++){
				this->classFile.read(&auxchar[k], sizeof(char));
				infoattaux+= auxchar;
			}
			auxshort[0] = auxchar[1];
			auxshort[1] = auxchar[0];

			attributesArrayaux.push_back(new ConstantValueAttribute(nameIndexattaux, infoLengthattaux, infoattaux, constantIndexAux));
		}else if(typeOfAtt == "Code"){

			unsigned short maxStackAux;

			unsigned short maxLocalsAux;

			unsigned int codeArrayCountAux;

			std::string codeArrayAux;

			unsigned short exceptionTableCountAux;

			std::vector<ObjExceptionTable> exceptionTableArrayAux;

			unsigned short codeAttributesCountAux;

			std::vector<ObjAttribute*> codeAttributesArrayAux;

			char* auxshort = ( char* ) &maxStackAux;
			for(unsigned int k = 0; k < 2; k++){
				this->classFile.read(&auxchar[k], sizeof(char));
				infoattaux+= auxchar[k];
			}
			auxshort[0] = auxchar[1];
			auxshort[1] = auxchar[0];

			auxshort = ( char* ) &maxLocalsAux;
			for(unsigned int k = 0; k < 2; k++){
				this->classFile.read(&auxchar[k], sizeof(char));
				infoattaux+= auxchar[k];
			}
			auxshort[0] = auxchar[1];
			auxshort[1] = auxchar[0];

			auxint = ( char* ) &codeArrayCountAux;
			for(int k=0; k<4; k++){
				this->classFile.read(&auxcharint[k], sizeof(char));
				infoattaux+= auxcharint[k];
			}
			auxint[0] = auxcharint[3];
			auxint[1] = auxcharint[2];
			auxint[2] = auxcharint[1];
			auxint[3] = auxcharint[0];

			for(unsigned int k = 0; k < codeArrayCountAux; k++){
				this->classFile.read(&auxuchar, sizeof(char));
				codeArrayAux += auxuchar;
				infoattaux+= auxuchar;
			}

			auxshort = ( char* ) &exceptionTableCountAux;
			for(unsigned int k = 0; k < 2; k++){
				this->classFile.read(&auxchar[k], sizeof(char));
				infoattaux+= auxchar[k];
			}
			auxshort[0] = auxchar[1];
			auxshort[1] = auxchar[0];

			for(unsigned int k = 0; k < exceptionTableCountAux; k++){

				unsigned short startPcAux;
				unsigned short endPcAux;
				unsigned short handlerPcAux;
				unsigned short catchTypeAux;

				auxshort = ( char* ) &startPcAux;
				for(unsigned int l = 0; l < 2; l++){
					this->classFile.read(&auxchar[k], sizeof(char));
					infoattaux+= auxchar[k];
				}
				auxshort[0] = auxchar[1];
				auxshort[1] = auxchar[0];

				auxshort = ( char* ) &endPcAux;
				for(unsigned int l = 0; l < 2; l++){
					this->classFile.read(&auxchar[k], sizeof(char));
					infoattaux+= auxchar[k];
				}
				auxshort[0] = auxchar[1];
				auxshort[1] = auxchar[0];

				auxshort = ( char* ) &handlerPcAux;
				for(unsigned int l = 0; l < 2; l++){
					this->classFile.read(&auxchar[k], sizeof(char));
					infoattaux+= auxchar[k];
				}
				auxshort[0] = auxchar[1];
				auxshort[1] = auxchar[0];

				auxshort = ( char* ) &catchTypeAux;
				for(unsigned int l = 0; l < 2; l++){
					this->classFile.read(&auxchar[k], sizeof(char));
					infoattaux+= auxchar[k];
				}
				auxshort[0] = auxchar[1];
				auxshort[1] = auxchar[0];

				exceptionTableArrayAux.push_back(ObjExceptionTable(startPcAux, endPcAux, handlerPcAux, catchTypeAux));

			}

			auxshort = ( char* ) &codeAttributesCountAux;
			for(unsigned int k = 0; k < 2; k++){
				this->classFile.read(&auxchar[k], sizeof(char));
				infoattaux+= auxchar[k];
			}
			auxshort[0] = auxchar[1];
			auxshort[1] = auxchar[0];

			std::vector<ObjAttribute*> codeAttributesArrayaux = loadObjAttributeArray(codeAttributesCountAux);

//			this->dumpAttributeArray(codeAttributesCountAux, codeAttributesArrayaux);

			attributesArrayaux.push_back(new CodeAttribute(nameIndexattaux, infoLengthattaux, infoattaux, maxStackAux, maxLocalsAux, codeArrayCountAux, codeArrayAux, exceptionTableCountAux, exceptionTableArrayAux, codeAttributesCountAux, codeAttributesArrayAux));

		}else if(typeOfAtt == "SourceFile"){

			unsigned short sourceFileIndexAux;

			char* auxshort = ( char* ) &sourceFileIndexAux;
			for(unsigned int k = 0; k < 2; k++){
				this->classFile.read(&auxchar[k], sizeof(char));
				infoattaux+= auxchar;
			}
			auxshort[0] = auxchar[1];
			auxshort[1] = auxchar[0];

			attributesArrayaux.push_back(new SourceFileAttribute(nameIndexattaux, infoLengthattaux, infoattaux, sourceFileIndexAux));
		}else{
			std::string infoattaux = "";

			for(unsigned int k = 0; k < infoLengthattaux; k++){
				this->classFile.read(&auxuchar, sizeof(char));
				infoattaux += auxuchar;
			}

			attributesArrayaux.push_back(new ObjAttribute(nameIndexattaux, infoLengthattaux, infoattaux));
		}
	}

	return attributesArrayaux;

}


int ClassReader::loadClassFile(std::string fileName){

	this->classFile.open(fileName.c_str(), std::ifstream::in | std::ios::binary);

	if(!this->classFile.is_open()){
		return 1;
	}
	unsigned int magicNumber;

	this->classFile.read(reinterpret_cast<char*>(&magicNumber), sizeof(int));

	if(magicNumber != 0xBEBAFECA){
		return 2;
	}

	this->versionNumber = 0;

	char auxchar[4];
	char *auxint = ( char* ) & this->versionNumber;
	for(int j=0; j<4; j++){
		this->classFile.read(&auxchar[j], sizeof(char));
	}
	auxint[0] = auxchar[3];
	auxint[1] = auxchar[2];
	auxint[2] = auxchar[1];
	auxint[3] = auxchar[0];

	this->statusFlag = 1;

	return -3;
}

int ClassReader::getConstantPoolCount(){

	if(statusFlag != 1){
		return 0;
	}

	this->constPoolCount = 0;

	char auxchar[2];
	char *auxshort = ( char* ) & this->constPoolCount;
	for(int j=0; j<2; j++){
		this->classFile.read(&auxchar[j], sizeof(char));
	}
	auxshort[0] = auxchar[1];
	auxshort[1] = auxchar[0];

	this->statusFlag = 2;
	return 1;
}

int ClassReader::loadConstantPoolArray(){

	if(statusFlag != 2){
		return 0;
	}

	char buffer;



	for(int i = 0; i<(this->constPoolCount-1); i++){

		this->classFile.read(&buffer, sizeof(char));

		switch(buffer){
			case 1:{
				char buffer2;
				std::string strbuffer = "";

				unsigned short aux = 0;
				char auxchar[2];
				char *auxshort = ( char* ) & aux;
				for(int j=0; j<2; j++){
					this->classFile.read(&auxchar[j], sizeof(char));
				}
				auxshort[0] = auxchar[1];
				auxshort[1] = auxchar[0];

				for(int j=0; j<aux; j++){
					this->classFile.read(&buffer2, sizeof(char));
					strbuffer += buffer2;
				}

				std::string strbufferDecoded = decodeUtf8Java(strbuffer);

				this->constantPoolArray.push_back(new StringConstantPool(i+1, strbufferDecoded));

			}break;

			case 3:{
				int aux = 0;
				char auxchar[4];
				char *auxint = ( char* ) & aux;
				for(int j=0; j<4; j++){
					this->classFile.read(&auxchar[j], sizeof(char));
				}
				auxint[0] = auxchar[3];
				auxint[1] = auxchar[2];
				auxint[2] = auxchar[1];
				auxint[3] = auxchar[0];
				this->constantPoolArray.push_back(new IntConstantPool(i+1, aux));

			}break;

			case 4:{
				float aux = 0;
				char auxchar[4];
				char *auxfloat = ( char* ) & aux;
				for(int j=0; j<4; j++){
					this->classFile.read(&auxchar[j], sizeof(char));
				}
				auxfloat[0] = auxchar[3];
				auxfloat[1] = auxchar[2];
				auxfloat[2] = auxchar[1];
				auxfloat[3] = auxchar[0];
				this->constantPoolArray.push_back(new FloatConstantPool(i+1, aux));
			}break;

			case 5:{
				long long aux = 0;
				char auxchar[8];
				char *auxlong = ( char* ) & aux;
				for(int j=0; j<8; j++){
					this->classFile.read(&auxchar[j], sizeof(char));
				}
				auxlong[0] = auxchar[7];
				auxlong[1] = auxchar[6];
				auxlong[2] = auxchar[5];
				auxlong[3] = auxchar[4];
				auxlong[4] = auxchar[3];
				auxlong[5] = auxchar[2];
				auxlong[6] = auxchar[1];
				auxlong[7] = auxchar[0];
				this->constantPoolArray.push_back(new LongConstantPool(i+1, aux));
				this->constantPoolArray.push_back(this->constantPoolArray[i]);
				i++;
			}break;

			case 6:{
				double aux = 0;
				char auxchar[8];
				char *auxdouble = ( char* ) & aux;
				for(int j=0; j<8; j++){
					this->classFile.read(&auxchar[j], sizeof(char));
				}
				auxdouble[0] = auxchar[7];
				auxdouble[1] = auxchar[6];
				auxdouble[2] = auxchar[5];
				auxdouble[3] = auxchar[4];
				auxdouble[4] = auxchar[3];
				auxdouble[5] = auxchar[2];
				auxdouble[6] = auxchar[1];
				auxdouble[7] = auxchar[0];
				this->constantPoolArray.push_back(new DoubleConstantPool(i+1, aux));
				this->constantPoolArray.push_back(this->constantPoolArray[i]);
				i++;
			}break;

			case 7:{

				unsigned short aux = 0;
				char auxchar[2];
				char *auxshort = ( char* ) & aux;
				for(int j=0; j<2; j++){
					this->classFile.read(&auxchar[j], sizeof(char));
				}
				auxshort[0] = auxchar[1];
				auxshort[1] = auxchar[0];

				this->constantPoolArray.push_back(new ClassRefConstantPool(i+1, aux));

			}break;

			case 8:{

				unsigned short aux = 0;
				char auxchar[2];
				char *auxshort = ( char* ) & aux;
				for(int j=0; j<2; j++){
					this->classFile.read(&auxchar[j], sizeof(char));
				}
				auxshort[0] = auxchar[1];
				auxshort[1] = auxchar[0];

				this->constantPoolArray.push_back(new StringRefConstantPool(i+1, aux));
			}break;

			case 9:{
				unsigned short aux1 = 0, aux2 = 0;
				char auxchar[2];
				char *auxshort1 = ( char* ) & aux1;
				for(int j=0; j<2; j++){
					this->classFile.read(&auxchar[j], sizeof(char));
				}
				auxshort1[0] = auxchar[1];
				auxshort1[1] = auxchar[0];
				char *auxshort2 = ( char* ) & aux2;
				for(int j=0; j<2; j++){
					this->classFile.read(&auxchar[j], sizeof(char));
				}
				auxshort2[0] = auxchar[1];
				auxshort2[1] = auxchar[0];

				this->constantPoolArray.push_back(new FieldRefConstantPool(i+1, aux1, aux2));
			}break;

			case 10:{

				unsigned short aux1 = 0, aux2 = 0;
				char auxchar[2];
				char *auxshort1 = ( char* ) & aux1;
				for(int j=0; j<2; j++){
					this->classFile.read(&auxchar[j], sizeof(char));
				}
				auxshort1[0] = auxchar[1];
				auxshort1[1] = auxchar[0];
				char *auxshort2 = ( char* ) & aux2;
				for(int j=0; j<2; j++){
					this->classFile.read(&auxchar[j], sizeof(char));
				}
				auxshort2[0] = auxchar[1];
				auxshort2[1] = auxchar[0];

				this->constantPoolArray.push_back(new MethodRefConstantPool(i+1, aux1, aux2));
			}break;

			case 11:{

				unsigned short aux1 = 0, aux2 = 0;
				char auxchar[2];
				char *auxshort1 = ( char* ) & aux1;
				for(int j=0; j<2; j++){
					this->classFile.read(&auxchar[j], sizeof(char));
				}
				auxshort1[0] = auxchar[1];
				auxshort1[1] = auxchar[0];
				char *auxshort2 = ( char* ) & aux2;
				for(int j=0; j<2; j++){
					this->classFile.read(&auxchar[j], sizeof(char));
				}
				auxshort2[0] = auxchar[1];
				auxshort2[1] = auxchar[0];

				this->constantPoolArray.push_back(new InterMethodRefConstantPool(i+1, aux1, aux2));
			}break;

			case 12:{

				unsigned short aux1 = 0, aux2 = 0;
				char auxchar[2];
				char *auxshort1 = ( char* ) & aux1;
				for(int j=0; j<2; j++){
					this->classFile.read(&auxchar[j], sizeof(char));
				}
				auxshort1[0] = auxchar[1];
				auxshort1[1] = auxchar[0];
				char *auxshort2 = ( char* ) & aux2;
				for(int j=0; j<2; j++){
					this->classFile.read(&auxchar[j], sizeof(char));
				}
				auxshort2[0] = auxchar[1];
				auxshort2[1] = auxchar[0];

				this->constantPoolArray.push_back(new NameTypeConstantPool(i+1, aux1, aux2));
			}break;

			case 15:{
				char aux1;
				char buffer2;
				this->classFile.read(&buffer2, sizeof(char));
				aux1 = buffer2;

				unsigned short aux2 = 0;
				char auxchar[2];
				char *auxshort = ( char* ) & aux2;
				for(int j=0; j<2; j++){
					this->classFile.read(&auxchar[j], sizeof(char));
				}
				auxshort[0] = auxchar[1];
				auxshort[1] = auxchar[0];

				this->constantPoolArray.push_back(new HandleConstantPool(i+1, aux1, aux2));
			}break;

			case 16:{

				unsigned short aux = 0;
				char auxchar[2];
				char *auxshort = ( char* ) & aux;
				for(int j=0; j<2; j++){
					this->classFile.read(&auxchar[j], sizeof(char));
				}
				auxshort[0] = auxchar[1];
				auxshort[1] = auxchar[0];

				this->constantPoolArray.push_back(new TypeConstantPool(i+1, aux));
			}break;

			case 18:{

				unsigned short aux1 = 0, aux2 = 0;
				char auxchar[2];
				char *auxshort1 = ( char* ) & aux1;
				for(int j=0; j<2; j++){
					this->classFile.read(&auxchar[j], sizeof(char));
				}
				auxshort1[0] = auxchar[1];
				auxshort1[1] = auxchar[0];
				char *auxshort2 = ( char* ) & aux2;
				for(int j=0; j<2; j++){
					this->classFile.read(&auxchar[j], sizeof(char));
				}
				auxshort2[0] = auxchar[1];
				auxshort2[1] = auxchar[0];

				this->constantPoolArray.push_back(new InvokeDynamicConstantPool(i+1, aux1, aux2));
			}break;
		}
	}



	this->statusFlag = 3;
	return 1;
}

int ClassReader::loadAccessFlags(){

	if(statusFlag != 3){
		return 0;
	}

	this->accessFlags = 0;

	char auxchar[2];
	char *auxshort = ( char* ) & accessFlags;
	for(int j=0; j<2; j++){
		this->classFile.read(&auxchar[j], sizeof(char));
	}
	auxshort[0] = auxchar[1];
	auxshort[1] = auxchar[0];

	this->statusFlag = 4;
	return 1;
}

int ClassReader::loadThisClassIndex(){

	if(statusFlag != 4){
		return 0;
	}

	this->thisClassIndex = 0;

	char auxchar[2];
	char *auxshort = ( char* ) & thisClassIndex;
	for(int j=0; j<2; j++){
		this->classFile.read(&auxchar[j], sizeof(char));
	}
	auxshort[0] = auxchar[1];
	auxshort[1] = auxchar[0];

	this->statusFlag = 5;
	return 1;
}

int ClassReader::loadSuperClassIndex(){

	if(statusFlag != 5){
		return 0;
	}

	this->superClassIndex = 0;

	char auxchar[2];
	char *auxshort = ( char* ) & superClassIndex;
	for(int j=0; j<2; j++){
		this->classFile.read(&auxchar[j], sizeof(char));
	}
	auxshort[0] = auxchar[1];
	auxshort[1] = auxchar[0];

	this->statusFlag = 6;
	return 1;
}

int ClassReader::loadInterfaceCount(){

	if(statusFlag != 6){
		return 0;
	}

	this->interfaceCount = 0;

	char auxchar[2];
	char *auxshort = ( char* ) & this->interfaceCount;
	for(int j=0; j<2; j++){
		this->classFile.read(&auxchar[j], sizeof(char));
	}
	auxshort[0] = auxchar[1];
	auxshort[1] = auxchar[0];

	this->statusFlag = 7;
	return 1;
}

int ClassReader::loadInterfaceArray(){

	if(statusFlag != 7){
		return 0;
	}
	unsigned short aux;
	for(int i =0; i<this->interfaceCount; i++){
		char auxchar[2];
		char *auxshort = ( char* ) & aux;
		for(int j=0; j<2; j++){
			this->classFile.read(&auxchar[j], sizeof(char));
		}
		auxshort[0] = auxchar[1];
		auxshort[1] = auxchar[0];
	}
	this->interfaceArray.push_back(aux);
	this->statusFlag = 8;
	return 1;
}

int ClassReader::loadFieldCount(){

	if(statusFlag != 8){
		return 0;
	}

	this->fieldCount = 0;

	char auxchar[2];
	char *auxshort = ( char* ) & this->fieldCount;
	for(int j=0; j<2; j++){
		this->classFile.read(&auxchar[j], sizeof(char));
	}
	auxshort[0] = auxchar[1];
	auxshort[1] = auxchar[0];

	this->statusFlag = 9;
	return 1;
}

int ClassReader::loadFieldArray(){

	if(statusFlag != 9){
		return 0;
	}

	field_method auxobj;

	for(int i =0; i < this->fieldCount; i++){

		unsigned short accessFlagsaux, nameIndexaux, descriptorIndexaux, attributesCountaux;

		char auxchar[2];
		char *auxshort = ( char* ) &accessFlagsaux;
		for(int j=0; j<2; j++){
			this->classFile.read(&auxchar[j], sizeof(char));
		}
		auxshort[0] = auxchar[1];
		auxshort[1] = auxchar[0];

		auxshort = ( char* ) &nameIndexaux;
		for(int j=0; j<2; j++){
			this->classFile.read(&auxchar[j], sizeof(char));
		}
		auxshort[0] = auxchar[1];
		auxshort[1] = auxchar[0];

		auxshort = ( char* ) &descriptorIndexaux;
		for(int j=0; j<2; j++){
			this->classFile.read(&auxchar[j], sizeof(char));
		}
		auxshort[0] = auxchar[1];
		auxshort[1] = auxchar[0];

		auxshort = ( char* ) &attributesCountaux;
		for(int j=0; j<2; j++){
			this->classFile.read(&auxchar[j], sizeof(char));
		}
		auxshort[0] = auxchar[1];
		auxshort[1] = auxchar[0];

		std::vector<ObjAttribute*> attributesArrayaux = loadObjAttributeArray(attributesCountaux);

		auxobj.setValues(accessFlagsaux, nameIndexaux, descriptorIndexaux, attributesCountaux, attributesArrayaux);
		this->fieldArray.push_back(auxobj);
	}

	this->statusFlag = 10;
	return 1;
}

int ClassReader::loadMethodCount(){

	if(statusFlag != 10){
		return 0;
	}

	this->methodCount = 0;

	char auxchar[2];
	char *auxshort = ( char* ) & this->methodCount;
	for(int j=0; j<2; j++){
		this->classFile.read(&auxchar[j], sizeof(char));
	}
	auxshort[0] = auxchar[1];
	auxshort[1] = auxchar[0];

	this->statusFlag = 11;
	return 1;
}

int ClassReader::loadMethodArray(){

	if(statusFlag != 11){
		return 0;
	}

	field_method auxObjFieldMethod;

	for(int i =0; i < this->methodCount; i++){

		unsigned short accessFlagsaux, nameIndexaux, descriptorIndexaux, attributesCountaux;

		char auxchar[2];
		char *auxshort = ( char* ) &accessFlagsaux;
		for(int j=0; j<2; j++){
			this->classFile.read(&auxchar[j], sizeof(char));
		}
		auxshort[0] = auxchar[1];
		auxshort[1] = auxchar[0];

		auxshort = ( char* ) &nameIndexaux;
		for(int j=0; j<2; j++){
			this->classFile.read(&auxchar[j], sizeof(char));
		}
		auxshort[0] = auxchar[1];
		auxshort[1] = auxchar[0];

		auxshort = ( char* ) &descriptorIndexaux;
		for(int j=0; j<2; j++){
			this->classFile.read(&auxchar[j], sizeof(char));
		}
		auxshort[0] = auxchar[1];
		auxshort[1] = auxchar[0];

		auxshort = ( char* ) &attributesCountaux;
		for(int j=0; j<2; j++){
			this->classFile.read(&auxchar[j], sizeof(char));
		}
		auxshort[0] = auxchar[1];
		auxshort[1] = auxchar[0];

		std::vector<ObjAttribute*> attributesArrayaux = loadObjAttributeArray(attributesCountaux);

		auxObjFieldMethod.setValues(accessFlagsaux, nameIndexaux, descriptorIndexaux, attributesCountaux, attributesArrayaux);
		this->methodArray.push_back(auxObjFieldMethod);

	}

	this->statusFlag = 12;
	return 1;
}

int ClassReader::loadAttributeCount(){

	if(statusFlag != 12){
		return 0;
	}

	this->attributeCount = 0;

	char auxchar[2];
	char *auxshort = ( char* ) & this->attributeCount;
	for(int j=0; j<2; j++){
		this->classFile.read(&auxchar[j], sizeof(char));
	}
	auxshort[0] = auxchar[1];
	auxshort[1] = auxchar[0];

	this->statusFlag = 13;
	return 1;
}

int ClassReader::loadAttributeArray(){

	if(statusFlag != 13){
		return 0;
	}

	this->attributeArray = this->loadObjAttributeArray(this->attributeCount);


	this->statusFlag = 14;
	return 1;
}

int ClassReader::closeClassFile(){
	this->classFile.close();
	this->statusFlag = 0;
	return 0;
}

void ClassReader::dumpAttributeArray(unsigned short attributeCountIn, std::vector<ObjAttribute*> attributeArrayIn){

	for(int j = 0; j < attributeCountIn; j++){
		std::cout << "	  atribute " << j+1 << ": "  << std::endl;
		std::cout << "		Name Index  : #" << attributeArrayIn[j]->getNameIndex()  << " // " << static_cast<StringConstantPool*>(constantPoolArray[attributeArrayIn[j]->getNameIndex()-1])->str << std::endl;

		if(static_cast<StringConstantPool*>(constantPoolArray[attributeArrayIn[j]->getNameIndex()-1])->str == "Code"){

			std::cout << "			MaxStack             : " << static_cast<CodeAttribute*>(attributeArrayIn[j])->getMaxStack() << std::endl;
			std::cout << "			MaxLocals            : " << static_cast<CodeAttribute*>(attributeArrayIn[j])->getMaxLocals() << std::endl;
			std::cout << "			Code Length          : " << static_cast<CodeAttribute*>(attributeArrayIn[j])->getCodeArrayCount() << std::endl;
			std::cout << "			Java Bytecode        :" ;
//			for(unsigned int k=0; k <  static_cast<CodeAttribute*>(attributeArrayIn[j])->getCodeArrayCount(); k++){
//				std::cout << " - " << std::hex << (int) (unsigned char)  static_cast<CodeAttribute*>(attributeArrayIn[j])->getCodeArray()[k]<< std::dec;
//			}
			std::cout << std::endl;
			dumpBytecode(static_cast<CodeAttribute*>(attributeArrayIn[j])->getCodeArray());

			std::cout << " - " << std::endl;
			std::cout << "			Exception Table Length : " << static_cast<CodeAttribute*>(attributeArrayIn[j])->getExceptionTableCount() << std::endl;

			if (static_cast<CodeAttribute*>(attributeArrayIn[j])->getExceptionTableCount() != 0){
				std::cout << "				implementar exception table dump" << std::endl;
			}

			std::cout << "			Number of attributes : " << static_cast<CodeAttribute*>(attributeArrayIn[j])->getAttributesCount() << std::endl;
			if((static_cast<CodeAttribute*>(attributeArrayIn[j])->getAttributesCount()) != 0){
				for(int k = 0; k < static_cast<CodeAttribute*>(attributeArrayIn[j])->getAttributesCount(); k++){
						std::cout << "				  atribute " << k+1 << ": "  << std::endl;
//						std::cout << "					Name Index  : #" << (static_cast<CodeAttribute*>(attributeArrayIn[j]))->getAttributesArray()[k]->getNameIndex()  << std::endl;
				}
			}

		}else if(static_cast<StringConstantPool*>(constantPoolArray[attributeArrayIn[j]->getNameIndex()-1])->str == "ConstantValue"){
			std::cout << "			Constant Index: #" << static_cast<ConstantValueAttribute*>(attributeArrayIn[j])->getConstantIndex() << std::endl;
		}else if(static_cast<StringConstantPool*>(constantPoolArray[attributeArrayIn[j]->getNameIndex()-1])->str == "SourceFile"){
			std::cout << "			Source File Index: #" << static_cast<SourceFileAttribute*>(attributeArrayIn[j])->getSourceFileIndex()  << " // " << static_cast<StringConstantPool*>(constantPoolArray[static_cast<SourceFileAttribute*>(attributeArrayIn[j])->getSourceFileIndex()-1])->str << std::endl;
		}else{

			std::cout << "		Info Length : " << attributeArrayIn[j]->getInfoLength() << std::endl;
			std::cout << "		Info array  :" ;
			for(unsigned int k=0; k < attributeArrayIn[j]->getInfoLength(); k++){
				std::cout << " - " << std::hex << (int) (unsigned char) attributeArrayIn[j]->getInfo()[k]<< std::dec;
			}
			std::cout << std::endl;

		}
	}


}

static const std::string instructionsList[] = {
    "nop",
    "aconst_null",
    "iconst_m1",
    "iconst_0",
    "iconst_1",
    "iconst_2",
    "iconst_3",
    "iconst_4",
    "iconst_5",
    "lconst_0",
    "lconst_1",
    "fconst_0",
    "fconst_1",
    "fconst_2",
    "dconst_0",
    "dconst_1",
    "bipush",
    "sipush",
    "ldc",
    "ldc_w",
    "ldc2_w",
    "iload",
    "lload",
    "fload",
    "dload",
    "aload",
    "iload_0",
    "iload_1",
    "iload_2",
    "iload_3",
    "lload_0",
    "lload_1",
    "lload_2",
    "lload_3",
    "fload_0",
    "fload_1",
    "fload_2",
    "fload_3",
    "dload_0",
    "dload_1",
    "dload_2",
    "dload_3",
    "aload_0",
    "aload_1",
    "aload_2",
    "aload_3",
    "iaload",
    "laload",
    "faload",
    "daload",
    "aaload",
    "baload",
    "caload",
    "saload",
    "istore",
    "lstore",
    "fstore",
    "dstore",
    "astore",
    "istore_0",
    "istore_1",
    "istore_2",
    "istore_3",
    "lstore_0",
    "lstore_1",
    "lstore_2",
    "lstore_3",
    "fstore_0",
    "fstore_1",
    "fstore_2",
    "fstore_3",
    "dstore_0",
    "dstore_1",
    "dstore_2",
    "dstore_3",
    "astore_0",
    "astore_1",
    "astore_2",
    "astore_3",
    "iastore",
    "lastore",
    "fastore",
    "dastore",
    "aastore",
    "bastore",
    "castore",
    "sastore",
    "pop",
    "pop2",
    "dup",
    "dup_x1",
    "dup_x2",
    "dup2",
    "dup2_x1",
    "dup2_x2",
    "swap",
    "iadd",
    "ladd",
    "fadd",
    "dadd",
    "isub",
    "lsub",
    "fsub",
    "dsub",
    "imul",
    "lmul",
    "fmul",
    "dmul",
    "idiv",
    "ldiv",
    "fdiv",
    "ddiv",
    "irem",
    "lrem",
    "frem",
    "drem",
    "ineg",
    "lneg",
    "fneg",
    "dneg",
    "ishl",
    "lshl",
    "ishr",
    "lshr",
    "iushr",
    "lushr",
    "iand",
    "land",
    "ior",
    "lor",
    "ixor",
    "lxor",
    "iinc",
    "i2l",
    "i2f",
    "i2d",
    "l2i",
    "l2f",
    "l2d",
    "f2i",
    "f2l",
    "f2d",
    "d2i",
    "d2l",
    "d2f",
    "i2b",
    "i2c",
    "i2s",
    "lcmp",
    "fcmpl",
    "fcmpg",
    "dcmpl",
    "dcmpg",
    "ifeq",
    "ifne",
    "iflt",
    "ifge",
    "ifgt",
    "ifle",
    "if_icmpeq",
    "if_icmpne",
    "if_icmplt",
    "if_icmpge",
    "if_icmpgt",
    "if_icmple",
    "if_acmpeq",
    "if_acmpne",
    "goto",
    "jsr",
    "ret",
    "tableswitch",
    "lookupswitch",
    "ireturn",
    "lreturn",
    "freturn",
    "dreturn",
    "areturn",
    "return",
    "getstatic",
    "putstatic",
    "getfield",
    "putfield",
    "invokevirtual",
    "invokespecial",
    "invokestatic",
    "invokeinterface",
    "UNUSED",
    "new",
    "newarray",
    "anewarray",
    "arraylength",
    "athrow",
    "checkcast",
    "instanceof",
    "monitorenter",
    "monitorexit",
    "wide",
    "multianewarray",
    "ifnull",
    "ifnonnull",
    "goto_w",
    "jsr_w"
};

void ClassReader::dumpBytecode(std::string bytecode){



	for(unsigned int i = 0; i < bytecode.length(); i++){
		std::cout << "				" << i << " - " << instructionsList[(unsigned char) bytecode[i]];
		switch((unsigned char) bytecode[i]){
		case 0x19:
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			break;
		case 0xbd:
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			break;
		case 0x3a:
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			break;
		case 0x10:
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			break;
		case 0xc0:
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			break;
		case 0x18:
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			break;
		case 0x39:
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			break;
		case 0x17:
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			break;
		case 0x15:
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			break;
		case 0x36:
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			break;
		case 0x12:
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			break;
		case 0x16:
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			break;
		case 0x37:
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			break;
		case 0xbc:
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			break;
		case 0xa9:
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			break;
		case 0xb4:
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			break;
		case 0xb2:
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			break;
		case 0xa7:
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			break;
		case 0xc8:
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			break;
		case 0xa5:
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			break;
		case 0xa6:
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			break;
		case 0x9f:
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			break;
		case 0xa2:
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			break;
		case 0xa3:
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			break;
		case 0xa4:
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			break;
		case 0xa1:
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			break;
		case 0xa0:
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			break;
		case 0x99:
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			break;
		case 0x9c:
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			break;
		case 0x9d:
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			break;
		case 0x9e:
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			break;
		case 0x9b:
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			break;
		case 0x9a:
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			break;
		case 0xc7:
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			break;
		case 0xc6:
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			break;
		case 0x84:
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			break;
		case 0xc1:
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			break;
		case 0xba:
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			break;
		case 0xb9:
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			break;
		case 0xb7:
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			break;
		case 0xb8:
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			break;
		case 0xb6:
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			break;
		case 0xa8:
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			break;
		case 0xc9:
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			break;
		case 0x13:
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			break;
		case 0x14:
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			break;
		case 0xab:
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			break;
		case 0xc5:
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			break;
		case 0xb5:
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			break;
		case 0xb3:
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			break;
		case 0x11:
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			break;
		case 0xbb:
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			i++;
			std::cout << " - " << std::hex << (int) (unsigned char) bytecode[i];
			break;
		}

		std::cout << std::dec << std::endl;
	}
}

void ClassReader::dumpClassFileInfo(){
	std::cout << "Version Number: " << this->versionNumber << std::endl;

	std::cout << "Number of Constants: " << this->constPoolCount - 1 << std::endl;

	for(int i=0; i<this->constPoolCount - 1; i++){
		if(i<9){
			std::cout << " ";
		}
		std::cout << "#" << this->constantPoolArray[i]->position  << " = ";
		switch (this->constantPoolArray[i]->tag){
			case 1:{
				std::cout << "Utf8           "	<< static_cast<StringConstantPool*>(constantPoolArray[i])->str;
			}break;
			case 3:{
				std::cout << "Integer        "	<< static_cast<IntConstantPool*>(constantPoolArray[i])->value;
			}break;
			case 4:{
				std::cout << "Float          "	<< static_cast<FloatConstantPool*>(constantPoolArray[i])->value << "f";
			}break;
			case 5:{
				std::cout << "Long           "	<< static_cast<LongConstantPool*>(constantPoolArray[i])->value;
				i++;
			}break;
			case 6:{
				std::cout << "Double         "	<< static_cast<DoubleConstantPool*>(constantPoolArray[i])->value << "d";
				i++;
			}break;
			case 7:{
				std::cout << "Class          #"	<< static_cast<ClassRefConstantPool*>(constantPoolArray[i])->nameIndex;
			}break;
			case 8:{
				std::cout << "String         #"	<< static_cast<StringRefConstantPool*>(constantPoolArray[i])->strIndex;
			}break;
			case 9:{
				std::cout << "Fieldref       #"	<< static_cast<FieldRefConstantPool*>(constantPoolArray[i])->classRefIndex << ".#" << static_cast<FieldRefConstantPool*>(constantPoolArray[i])->nameTypeIndex;
			}break;
			case 10:{
				std::cout << "Methodref      #"	<< static_cast<MethodRefConstantPool*>(constantPoolArray[i])->classRefIndex << ".#" << static_cast<MethodRefConstantPool*>(constantPoolArray[i])->nameTypeIndex;
			}break;
			case 11:{
				std::cout << "Interfaceref   #"	<< static_cast<InterMethodRefConstantPool*>(constantPoolArray[i])->classRefIndex << ".#" << static_cast<InterMethodRefConstantPool*>(constantPoolArray[i])->nameTypeIndex;
			}break;
			case 12:{
				std::cout << "NameAndType    #"	<< static_cast<NameTypeConstantPool*>(constantPoolArray[i])->nameIndex << ":#" << static_cast<NameTypeConstantPool*>(constantPoolArray[i])->type;
			}break;
			case 15:{
				std::cout << "Methodhandle   #"	<< static_cast<HandleConstantPool*>(constantPoolArray[i])->type << ":#" << static_cast<HandleConstantPool*>(constantPoolArray[i])->index;
			}break;
			case 16:{
				std::cout << "Methodtype     #"	<< static_cast<TypeConstantPool*>(constantPoolArray[i])->index;
			}break;
			case 18:{
				std::cout << "InvokeDynamic    #"	<< static_cast<InvokeDynamicConstantPool*>(constantPoolArray[i])->bootstrapMethodIndex << ":#" << static_cast<InvokeDynamicConstantPool*>(constantPoolArray[i])->nameTypeIndex;
			}break;
		}

		std::cout << std::endl;
	}

	std::cout << "AccessFlags -- Hex representation: " << std::hex << this->accessFlags << std::dec << " -- Bitset representation: " << std::bitset<16>(this->accessFlags) << std::endl;

	std::cout << "Flags: -";

	if((this->accessFlags & 0x0001) != 0){
		std::cout << " ACC_PUBLIC -";
	}
	if((this->accessFlags & 0x0010) != 0){
		std::cout << " ACC_FINAL -";
	}
	if((this->accessFlags & 0x0020) != 0){
		std::cout << " ACC_SUPER -";
	}
	if((this->accessFlags & 0x0200) != 0){
		std::cout << " ACC_INTERFACE -";
	}
	if((this->accessFlags & 0x0400) != 0){
		std::cout << " ACC_ABSTRACT -";
	}
	if((this->accessFlags & 0x1000) != 0){
		std::cout << " ACC_SYNTHETIC -";
	}
	if((this->accessFlags & 0x2000) != 0){
		std::cout << " ACC_ANNOTATION -";
	}
	if((this->accessFlags & 0x4000) != 0){
		std::cout << " ACC_ENUM -";
	}

	std::cout << std::endl;

	std::cout << "This class Index in constant pool: #" << this->thisClassIndex << std::endl;

	if(this->superClassIndex != 0){
		std::cout << "The super class Index in constant pool: #" << this->superClassIndex << std::endl;
	}

	std::cout << "The number of interfaces: " << this->interfaceCount << std::endl;

	if(this->interfaceCount != 0){

		std::cout << "interfaces index constant pool: - ";

		for(int i =0; i<this->interfaceCount; i++){
			std::cout << "#" << this->interfaceArray[i] << " - ";
		}
	}

	std::cout << std::endl;

	std::cout << "The number of fields: " << this->fieldCount << std::endl;

	if(this->fieldCount != 0){

		std::cout << "Field Array:" << std::endl;

		for(int i = 0; i < this->fieldCount; i++){
			std::cout << "Field " << i+1 << ": "  << std::endl;
			std::cout << "    AccessFlags          -- Hex representation: " << std::hex << this->fieldArray[i].getAccessFlags() << std::dec << " -- Bitset representation: " << std::bitset<16>(this->fieldArray[i].getAccessFlags()) << std::endl;
			std::cout << "    Name Index           -- #" << this->fieldArray[i].getNameIndex()<< " // " << static_cast<StringConstantPool*>(constantPoolArray[this->fieldArray[i].getNameIndex()-1])->str << std::endl;
			std::cout << "    Descriptor Index     -- #" << this->fieldArray[i].getDescriptorIndex() << " // " << static_cast<StringConstantPool*>(constantPoolArray[this->fieldArray[i].getDescriptorIndex()-1])->str << std::endl;
			std::cout << "    Number of attributes -- " << this->fieldArray[i].getAttributesCount() << std::endl;
			if((this->fieldArray[i].getAttributesCount()) != 0){
				this->dumpAttributeArray(this->fieldArray[i].getAttributesCount(), this->methodArray[i].getAttributesArray());
			}
		}
	}

	std::cout << "The number of methods: " << this->methodCount << std::endl;

	if(this->methodCount != 0){

		std::cout << "Method Array:" << std::endl;

		for(int i = 0; i < this->methodCount; i++){
			std::cout << "Method " << i+1 << ": "  << std::endl;
			std::cout << "    AccessFlags          -- Hex representation: " << std::hex << this->methodArray[i].getAccessFlags() << std::dec << " -- Bitset representation: " << std::bitset<16>(this->methodArray[i].getAccessFlags()) << std::endl;
			std::cout << "    Name Index           -- #" << this->methodArray[i].getNameIndex() << " // " << static_cast<StringConstantPool*>(constantPoolArray[this->methodArray[i].getNameIndex()-1])->str << std::endl;
			std::cout << "    Descriptor Index     -- #" << this->methodArray[i].getDescriptorIndex() << " // " << static_cast<StringConstantPool*>(constantPoolArray[this->methodArray[i].getDescriptorIndex()-1])->str << std::endl;
			std::cout << "    Number of attributes -- " << this->methodArray[i].getAttributesCount() << std::endl;
			if((this->methodArray[i].getAttributesCount()) != 0){
				this->dumpAttributeArray(this->methodArray[i].getAttributesCount(), this->methodArray[i].getAttributesArray());
			}
		}
	}

	std::cout << "The number of attributes: " << this->attributeCount << std::endl;
	if(this->attributeCount != 0){
		if((this->attributeCount) != 0){
			this->dumpAttributeArray(this->attributeCount, this->attributeArray);
		}
	}

}
