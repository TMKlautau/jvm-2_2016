/*
 * FieldAndMethod.cpp
 *
 *  Created on: Nov 6, 2016
 *      Author: TitoKlautau
 */

#include "FieldAndMethod.h"



void field_method::setValues(unsigned short accessFlagsIn, unsigned short nameIndexIn, unsigned short descriptorIndexIn, unsigned short attributesCountIn, std::vector<ObjAttribute*> attributesArrayIn){

	this->value[0] = 0;
	this->value[1] = 0;
	this->accessFlags = accessFlagsIn;
	this->nameIndex = nameIndexIn;
	this->descriptorIndex = descriptorIndexIn;
	this->attributesCount = attributesCountIn;
	this->attributesArray = attributesArrayIn;
}

unsigned short field_method::getAccessFlags(){
	return this->accessFlags;
}

unsigned short field_method::getNameIndex(){
	return this->nameIndex;
}

unsigned short field_method::getDescriptorIndex(){
	return this->descriptorIndex;
}

unsigned short field_method::getAttributesCount(){
	return this->attributesCount;
}

std::vector<ObjAttribute*> field_method::getAttributesArray(){
	return this->attributesArray;
}
