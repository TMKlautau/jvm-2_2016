/*
 * Frame.cpp
 *
 *  Created on: Dec 7, 2016
 *      Author: TitoKlautau
 */

#include "FrameAndObject.h"

Frame::Frame(std::string methodName, ClassReader* className, int objref){
	this->cp = 0;
	this->constantPool = &(className->constantPoolArray);
	for(int i = 0; i < className->methodCount; i++){
		if (static_cast<StringConstantPool*>((*constantPool)[className->methodArray[i].getNameIndex()-1])->str == methodName){
			for(int j = 0; j < className->methodArray[i].getAttributesCount(); j++){
				if (static_cast<StringConstantPool*>((*constantPool)[className->methodArray[i].getAttributesArray()[j]->getNameIndex()-1])->str == "Code"){
					this->maxSizeLocalVariable = (unsigned short) static_cast<CodeAttribute*>(className->methodArray[i].getAttributesArray()[j])->getMaxLocals();
					this->maxSizeStack = static_cast<CodeAttribute*>(className->methodArray[i].getAttributesArray()[j])->getMaxStack();
					this->bytecode = static_cast<CodeAttribute*>(className->methodArray[i].getAttributesArray()[j])->getCodeArray();
					this->arrayLocalVariable.resize((unsigned int) this->maxSizeLocalVariable);
					this->arrayLocalVariable[0] = (objref);
					//std::cout << this->arrayLocalVariable[0];
				}
			}
		}
	}
}


