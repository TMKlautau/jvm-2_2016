//============================================================================
// Name        : JVM.cpp
// Author      : Tito Klautau
// Version     : 1.0
// Copyright   : 
// Description : JVM para a materia SB
//============================================================================



#include "MethodArea.h"
#include "RuntimeEngine.h"

int main(int argc, char* argv[]) {

	std::string fileName;

	if(argc == 3){
		fileName = argv[1];

		if(*argv[2] == '1'){
			int code;
			ClassReader test;

			code = test.loadClassFileInfo(fileName);;
			if(code==0){
				test.dumpClassFileInfo();
			}else if(code==1){
				std::cout << "arquivo nao pode ser aberto";
			}else if(code==2){
				std::cout << "arquivo nao eh .class";
			}else if(code < 0){
				std::cout << "ocorreu erro no load, favor verificar status flag";
			}
		}else if(*argv[2] == '2'){
			//aqui começa a execução do .class
			MethodArea methodArea;
			RuntimeEngine runtimeEngine(&methodArea);

			methodArea.classArray.push_back(new ClassReader);
			int code = methodArea.classArray.back()->loadClassFileInfo(fileName);
			if(code==0){

				#if DEBUGMODE == 1
					std::cout << std::endl << "DEBUG MODE SELECIONADO, PARA DESATIVAR RECOMPILE COM A VARIAVEL DEBUGMODE NO ARQUIVO toolbox.h LINHA 16 SETADA EM 0" << std::endl << std::endl;

					std::cout << std::endl << " - CP - OPCODE -  NOME DA INSTRUCAO -- linha: LINHA EM QUE ELA ESTA NO ARQUIVO RuntimeEngine.cpp" << std::endl << std::endl;

				#endif

				#if DEBUGMODE == 1
					std::cout << std::endl << "		<init>" << std::endl << std::endl;
				#endif

				runtimeEngine.frameStack.push(Frame("<init>", methodArea.classArray[0], 0));

				runtimeEngine.startExecution();
				#if DEBUGMODE == 1
					std::cout << std::endl << "		 main" << std::endl << std::endl;
				#endif
				runtimeEngine.frameStack.push(Frame("main", methodArea.classArray[0], 0));

				runtimeEngine.startExecution();

				#if DEBUGMODE == 1
					std::cout << std::endl << "DEBUG MODE SELECIONADO, PARA DESATIVAR RECOMPILE COM A VARIAVEL DEBUGMODE NO ARQUIVO toolbox.h LINHA 16 SETADA EM 0" << std::endl << std::endl;

					std::cout << std::endl << " - CP - OPCODE -  NOME DA INSTRUCAO -- linha: LINHA EM QUE ELA ESTA NO ARQUIVO RuntimeEngine.cpp" << std::endl << std::endl;

				#endif

			}else if(code==1){
				std::cout << "arquivo nao pode ser aberto";
			}else if(code==2){
				std::cout << "arquivo nao eh .class";
			}else if(code < 0){
				std::cout << "ocorreu erro no load, favor verificar status flag";
			}


		}
	}else{

		int code, option;
		ClassReader test;

		std::cout << "digite 1 para exibidor, 2 para executar:" << std::endl;

		std::cin >> option;

		if(option == 1){

			std::cout << "exibidor selecionado, favor inserir nome do arquivo:" << std::endl;

			std::cin >> fileName;

			code = test.loadClassFileInfo(fileName);;
			if(code==0){
				test.dumpClassFileInfo();
			}else if(code==1){
				std::cout << "arquivo nao pode ser aberto";
			}else if(code==2){
				std::cout << "arquivo nao eh .class";
			}else if(code < 0){
				std::cout << "ocorreu erro no load, favor verificar status flag";
			}
		}else if(option == 2){
			std::cout << "executar selecionado, favor inserir nome do arquivo:" << std::endl;

			std::cin >> fileName;

			//aqui começa a execução do .class
			MethodArea methodArea;
			RuntimeEngine runtimeEngine(&methodArea);

			methodArea.classArray.push_back(new ClassReader);
			int code = methodArea.classArray.back()->loadClassFileInfo(fileName);
			if(code==0){

				#if DEBUGMODE == 1
					std::cout << std::endl << "DEBUG MODE SELECIONADO, PARA DESATIVAR RECOMPILE COM A VARIAVEL DEBUGMODE NO ARQUIVO toolbox.h LINHA 16 SETADA EM 0" << std::endl << std::endl;

					std::cout << std::endl << " - CP - OPCODE -  NOME DA INSTRUCAO -- linha: LINHA EM QUE ELA ESTA NO ARQUIVO RuntimeEngine.cpp" << std::endl << std::endl;

				#endif

				#if DEBUGMODE == 1
					std::cout << std::endl << "		<init>" << std::endl << std::endl;
				#endif

				runtimeEngine.frameStack.push(Frame("<init>", methodArea.classArray[0], 0));

				runtimeEngine.startExecution();
				#if DEBUGMODE == 1
					std::cout << std::endl << "		 main" << std::endl << std::endl;
				#endif
				runtimeEngine.frameStack.push(Frame("main", methodArea.classArray[0], 0));

				runtimeEngine.startExecution();

				#if DEBUGMODE == 1
					std::cout << std::endl << "DEBUG MODE SELECIONADO, PARA DESATIVAR RECOMPILE COM A VARIAVEL DEBUGMODE NO ARQUIVO toolbox.h LINHA 16 SETADA EM 0" << std::endl << std::endl;

					std::cout << std::endl << " - CP - OPCODE -  NOME DA INSTRUCAO -- linha: LINHA EM QUE ELA ESTA NO ARQUIVO RuntimeEngine.cpp" << std::endl << std::endl;

				#endif

			}else if(code==1){
				std::cout << "arquivo nao pode ser aberto";
			}else if(code==2){
				std::cout << "arquivo nao eh .class";
			}else if(code < 0){
				std::cout << "ocorreu erro no load, favor verificar status flag";
			}

		}


	}

	return 0;
}
