/*
 * ConstantPool.cpp
 *
 *  Created on: Nov 2, 2016
 *      Author: TitoKlautau
 */

#include "ConstantPool.h"

StringConstantPool::StringConstantPool(int pos, std::string strIn){
	this->str = strIn;
	this->position = pos;
	this->tag = 1;
}

IntConstantPool::IntConstantPool(int pos, int valueIn){
	this->value = valueIn;
	this->position = pos;
	this->tag = 3;
}

FloatConstantPool::FloatConstantPool(int pos, float valueIn){
	this->value = valueIn;
	this->position = pos;
	this->tag = 4;
}

LongConstantPool::LongConstantPool(int pos, long long valueIn){
	this->value = valueIn;
	this->position = pos;
	this->tag = 5;
}

DoubleConstantPool::DoubleConstantPool(int pos, double valueIn){
	this->value = valueIn;
	this->position = pos;
	this->tag = 6;
}

ClassRefConstantPool::ClassRefConstantPool(int pos, unsigned short nameIndexIn){
	this->nameIndex = nameIndexIn;
	this->position = pos;
	this->tag = 7;
}

StringRefConstantPool::StringRefConstantPool(int pos, unsigned short strIndexIn){
	this->strIndex = strIndexIn;
	this->position = pos;
	this->tag = 8;
	this->init = false;
	this->str = "";
}

FieldRefConstantPool::FieldRefConstantPool(int pos, unsigned short classRefIndexIn, unsigned short nameTypeIndexIn){
	this->classRefIndex = classRefIndexIn;
	this->nameTypeIndex = nameTypeIndexIn;
	this->position = pos;
	this->tag = 9;
}

MethodRefConstantPool::MethodRefConstantPool(int pos, unsigned short classRefIndexIn, unsigned short nameTypeIndexIn){
	this->classRefIndex = classRefIndexIn;
	this->nameTypeIndex = nameTypeIndexIn;
	this->position = pos;
	this->tag = 10;
}

InterMethodRefConstantPool::InterMethodRefConstantPool(int pos, unsigned short classRefIndexIn, unsigned short nameTypeIndexIn){
	this->classRefIndex = classRefIndexIn;
	this->nameTypeIndex = nameTypeIndexIn;
	this->position = pos;
	this->tag = 11;
}

NameTypeConstantPool::NameTypeConstantPool(int pos, unsigned short nameIndexIn, unsigned short typeIn){
	this->nameIndex = nameIndexIn;
	this->type = typeIn;
	this->position = pos;
	this->tag = 12;
}

HandleConstantPool::HandleConstantPool(int pos, char typeIn, unsigned short indexIn){
	this->index = indexIn;
	this->type = typeIn;
	this->position = pos;
	this->tag = 15;
}

TypeConstantPool::TypeConstantPool(int pos, unsigned short indexIn){
	this->index = indexIn;
	this->position = pos;
	this->tag = 16;
}

InvokeDynamicConstantPool::InvokeDynamicConstantPool(int pos, unsigned short bootstrapMethodIndexIn, unsigned short nameTypeIndexIn){
	this->bootstrapMethodIndex = bootstrapMethodIndexIn;
	this->nameTypeIndex = nameTypeIndexIn;
	this->position = pos;
	this->tag = 18;
}
