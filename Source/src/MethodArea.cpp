/*
 * MethodArea.cpp
 *
 *  Created on: Dec 7, 2016
 *      Author: TitoKlautau
 */

#include "MethodArea.h"

void MethodArea::insertNewClass(std::string classFileName){

	this->classArray.push_back(new ClassReader);

	this->classArray.back()->loadClassFileInfo(classFileName);

}

