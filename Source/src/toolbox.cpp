/*
 * toolbox.cpp
 *
 *  Created on: Nov 5, 2016
 *      Author: TitoKlautau
 */

#include "toolbox.h"

/*
 * terminar esse paranaue, como eu detesto java, pqp
 */
std::string decodeUtf8Java(std::string strBufferIn){

	std::string strbuffer = "";

	char auxchar[3] = "";

	for (unsigned int i = 0; i < strBufferIn.length(); i++){
		if((strBufferIn[i] & 0x80) == 0){
			strbuffer += strBufferIn[i];
			continue;
		}
		if((strBufferIn[i] & 0xE0) == 0xE0){
			auxchar[0] = strBufferIn[i];
			auxchar[1] = strBufferIn[i+1];
			auxchar[2] = strBufferIn[i+1];
			char aux = auxchar[1];

			i += 2;

			auxchar[0] <<= 4;

			auxchar[1] >>= 2;

			auxchar[1] &= 0x0F;

			auxchar[0] |= auxchar[1];

			strbuffer += auxchar[0];

			aux <<= 6;

			auxchar[2] &= 0x3F;

			auxchar[2] |= aux;

			strbuffer += auxchar[2];

			continue;
		}
		if((strBufferIn[i] & 0xC0) == 0xC0){
			auxchar[0] = strBufferIn[i];
			auxchar[1] = strBufferIn[i+1];
			i++;

			auxchar[1] &= 0x3F;
			auxchar[1] |= (auxchar[0] << 6);

			auxchar[0] >>= 2;
			auxchar[0] &= 0x07;

			if(auxchar[0] != 0){
				strbuffer += auxchar[0];
			}

			strbuffer += auxchar[1];
			continue;
		}
	}

	return strbuffer;
}
void copyBitsetDword(char* to, char* from){
	to[0] = from[0];
	to[1] = from[1];
	to[2] = from[2];
	to[3] = from[3];

}

void splitQWord(char* quadword, char* upperAux, char* lowerAux){
	upperAux[0] = quadword[0];
	upperAux[1] = quadword[1];
	upperAux[2] = quadword[2];
	upperAux[3] = quadword[3];

	lowerAux[0] = quadword[4];
	lowerAux[1] = quadword[5];
	lowerAux[2] = quadword[6];
	lowerAux[3] = quadword[7];
}

void uniteDWords(char* quadword, char* upperAux, char* lowerAux){
	quadword[0] = upperAux[0];
	quadword[1] = upperAux[1];
	quadword[2] = upperAux[2];
	quadword[3] = upperAux[3];

	quadword[4] = lowerAux[0];
	quadword[5] = lowerAux[1];
	quadword[6] = lowerAux[2];
	quadword[7] = lowerAux[3];
}

