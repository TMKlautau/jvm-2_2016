/*
 * RuntimeEngine.cpp
 *
 *  Created on: Dec 7 , 2016
 *      Author: TitoKlautau
 */

#include "RuntimeEngine.h"


RuntimeEngine::RuntimeEngine(MethodArea* methodAreaIn){
	this->methodArea = methodAreaIn;
	this->wideFlag = false;
}

void RuntimeEngine::startExecution(){
	this->initInstructionsVector();
	while (!this->frameStack.empty()){
		while((this->frameStack.top().cp >=0) && (this->frameStack.top().cp < (int) this->frameStack.top().bytecode.length())){
#if DEBUGMODE == 1
			std::cout << " - " << this->frameStack.top().cp;
			std::cout << " - " << std::hex << (int) (unsigned char) this->frameStack.top().bytecode[this->frameStack.top().cp]<< std::dec << " - ";
#endif
			(this->*instructionsVector[(unsigned char)this->frameStack.top().bytecode[this->frameStack.top().cp]])();
		}
		this->frameStack.pop();
	}
}

/*
 * intructions em orden alfabetica
 */

void RuntimeEngine::i_aaload(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_aaload -- completo -- linha: " << __LINE__ << std::endl;
#endif

	unsigned int index = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	if(this->frameStack.top().operandStack.top() == 0){
		std::cout << " ------NullPointerException------ " << std::endl;
	}

	std::vector<int>* arrayref = (std::vector<int>*) this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	this->frameStack.top().operandStack.push((*arrayref)[index+1]);

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	Index no array: " << index << " -- Topo do operand stack(referencia carregada): " << this->frameStack.top().operandStack.top() << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_aastore(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_aastore -- completo -- linha: " << __LINE__ << std::endl;
#endif

	int value = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	unsigned int index = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	if(this->frameStack.top().operandStack.top() == 0){
		std::cout << " ------NullPointerException------ " << std::endl;
	}

	std::vector<int>* arrayref = (std::vector<int>*) this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	(*arrayref)[index+1] = value;

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	Index no array: " << index << " -- Refer�ncia carregada no array: " << (*arrayref)[index+1] << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_aconst_null(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_aconst_null -- completo -- linha: " << __LINE__ << std::endl;
#endif

	this->frameStack.top().operandStack.push(0);

#if DEBUGMODE == 1
	std::cout << "	dado push de null reference no operand stack, (referencia nula � tradada como 0)" << std::endl << std::endl;
#endif

	this->frameStack.top().cp ++;
}

void RuntimeEngine::i_aload(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_aload -- completo -- linha: " << __LINE__  << std::endl;
#endif

	unsigned short index;

	if(this->wideFlag){
		index = 0;
   		char* auxshort = ( char* ) &index;
   		auxshort[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+1];
   		auxshort[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+2];
	}else{
		index = (unsigned short) this->frameStack.top().bytecode[this->frameStack.top().cp+1];
	}

	int value = this->frameStack.top().arrayLocalVariable[(unsigned short) index];
	this->frameStack.top().operandStack.push(value);
	if(this->wideFlag){
		this->frameStack.top().cp +=3;
		this->wideFlag = false;
	}else{
		this->frameStack.top().cp +=2;
	}
#if DEBUGMODE == 1
	std::cout << "	Index da local vaiable: " << (unsigned short) index <<  " -  reference carregada da variavel (topo do operandStack)"  << this->frameStack.top().operandStack.top() << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_aload_0(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_aload_0 -- completo -- linha: " << __LINE__  << std::endl;
#endif
	this->frameStack.top().operandStack.push(this->frameStack.top().arrayLocalVariable[0]);
	this->frameStack.top().cp ++;
#if DEBUGMODE == 1
	std::cout << "	reference carregada da variavel (topo do operandStack): "  << this->frameStack.top().operandStack.top() << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_aload_1(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_aload_1 -- completo -- linha: " << __LINE__  << std::endl;
#endif
	this->frameStack.top().operandStack.push(this->frameStack.top().arrayLocalVariable[1]);
	this->frameStack.top().cp ++;
#if DEBUGMODE == 1
	std::cout << "	reference carregada da variavel (topo do operandStack): "  << this->frameStack.top().operandStack.top() << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_aload_2(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_aload_2 -- completo -- linha: " << __LINE__  << std::endl;
#endif
	this->frameStack.top().operandStack.push(this->frameStack.top().arrayLocalVariable[2]);
	this->frameStack.top().cp ++;
#if DEBUGMODE == 1
	std::cout << "	reference carregada da variavel (topo do operandStack): "  << this->frameStack.top().operandStack.top() << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_aload_3(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_aload_3 -- completo -- linha: " << __LINE__  << std::endl;
#endif
	this->frameStack.top().operandStack.push(this->frameStack.top().arrayLocalVariable[3]);
	this->frameStack.top().cp ++;
#if DEBUGMODE == 1
	std::cout << "	reference carregada da variavel (topo do operandStack): "  << this->frameStack.top().operandStack.top() << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_anewarray(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_anewarray -- linha: " << __LINE__  << std::endl;
#endif
	unsigned short index = 0;
	char* auxshort = ( char* ) &index;
	auxshort[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+1];
	auxshort[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+2];

	/*
	 * como n�o se implementou a heap, todas as referencias s�o tratadas como int
	 */

	int count = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	std::vector<int>* refarray = new std::vector<int>;

	refarray->resize(count+1);

	refarray->data()[0] = 1;

	this->frameStack.top().operandStack.push((int) refarray);

	this->frameStack.top().cp+=3;
#if DEBUGMODE == 1
	std::cout<< "	count retirado do operandStack: " << count << " - reference para o novo array (topo do operandStack): "  << this->frameStack.top().operandStack.top() << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_areturn(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_areturn -- completo -- linha: " << __LINE__   << std::endl;
#endif
    int retorno = this->frameStack.top().operandStack.top();
    this->frameStack.pop();
    this->frameStack.top().operandStack.push(retorno);
#if DEBUGMODE == 1
   std::cout << "	reference retornada: " << this->frameStack.top().operandStack.top() << std::endl;
	std::cout << " ++++++++++++++++++++++++++++ " << this->frameStack.size()+1 << " ++++++++++++++++++++++++++++	" << std::endl;
#endif
}

void RuntimeEngine::i_arraylength(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_arraylength -- completo -- linha: " << __LINE__   << std::endl;
#endif

	if(this->frameStack.top().operandStack.top() == 0){
		std::cout << " ------NullPointerException------ " << std::endl;
	}

	std::vector<int>* arrayref = (std::vector<int>*) this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	if(arrayref->data()[0] == 1){
		this->frameStack.top().operandStack.push((arrayref->size())-1);
	}else if(arrayref->data()[0] == 2){
		this->frameStack.top().operandStack.push(((arrayref->size())-1)/2);
	}



	this->frameStack.top().cp++;
#if DEBUGMODE == 1
   std::cout << "	tamanho do array: " << this->frameStack.top().operandStack.top() << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_astore(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_astore -- linha: " << __LINE__   << std::endl;
#endif

	unsigned short index;

	if(this->wideFlag){
		index = 0;
   		char* auxshort = ( char* ) &index;
   		auxshort[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+1];
   		auxshort[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+2];
	}else{
		index = (unsigned short) this->frameStack.top().bytecode[this->frameStack.top().cp+1];
	}

	int value = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	this->frameStack.top().arrayLocalVariable[index]= value;

	if(this->wideFlag){
		this->frameStack.top().cp +=3;
		this->wideFlag = false;
	}else{
		this->frameStack.top().cp +=2;
	}

#if DEBUGMODE == 1
	std::cout << "	index da variavel: " << (unsigned short) index  << " - reference carregada na variavel: "  << this->frameStack.top().arrayLocalVariable[index] << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_astore_0(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_astore_0 -- linha: " << __LINE__   << std::endl;
#endif

	int value = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	this->frameStack.top().arrayLocalVariable[0]= value;

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	reference carregada na variavel: "  << this->frameStack.top().arrayLocalVariable[0] << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_astore_1(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_astore_1 -- linha: " << __LINE__   << std::endl;
#endif

	int value = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	this->frameStack.top().arrayLocalVariable[1]= value;

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	reference carregada na variavel: "  << this->frameStack.top().arrayLocalVariable[1] << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_astore_2(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_astore_2 -- linha: " << __LINE__   << std::endl;
#endif

	int value = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	this->frameStack.top().arrayLocalVariable[2]= value;

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	reference carregada na variavel: "  << this->frameStack.top().arrayLocalVariable[2] << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_astore_3(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_astore_3 -- linha: " << __LINE__   << std::endl;
#endif

	int value = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	this->frameStack.top().arrayLocalVariable[3]= value;

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	reference carregada na variavel: "  << this->frameStack.top().arrayLocalVariable[3] << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_athrow(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_athrow -- linha: " << __LINE__   << std::endl;
#endif
	this->frameStack.top().cp++;
#if DEBUGMODE == 1
	std::cout << "	intruction opcional " << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_baload(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_baload -- linha: " << __LINE__   << std::endl;
#endif

	unsigned int index = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	if(this->frameStack.top().operandStack.top() == 0){
		std::cout << " ------NullPointerException------ " << std::endl;
	}

	std::vector<int>* arrayref = (std::vector<int>*) this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	this->frameStack.top().operandStack.push((*arrayref)[index+1]);

	this->frameStack.top().cp ++;
#if DEBUGMODE == 1
	std::cout << "	Index no array: " << index << " -- Topo do operand stack(byte estendido carregado): " << this->frameStack.top().operandStack.top() << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_bastore(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_bastore -- completo -- linha: " << __LINE__ << std::endl;
#endif

	int value = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	unsigned int index = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	if(this->frameStack.top().operandStack.top() == 0){
		std::cout << " ------NullPointerException------ " << std::endl;
	}

	std::vector<int>* arrayref = (std::vector<int>*) this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	(*arrayref)[index+1] = value;

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	Index no array: " << index << " -- byte extendido carregado no array: " << (*arrayref)[index+1] << std::endl << std::endl ;
#endif
}

void RuntimeEngine::i_bipush(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_bipush -- completo -- linha: " << __LINE__ << std::endl;
#endif

	this->frameStack.top().operandStack.push((int) this->frameStack.top().bytecode[(int) this->frameStack.top().cp+1]);
	this->frameStack.top().cp +=2;

#if DEBUGMODE == 1
	   std::cout << "	byte extendido pushed (topo do operandStack): " << this->frameStack.top().operandStack.top() << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_caload(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_caload -- completo -- linha: " << __LINE__ << std::endl;
#endif

	unsigned int index = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	if(this->frameStack.top().operandStack.top() == 0){
		std::cout << " ------NullPointerException------ " << std::endl;
	}

	std::vector<int>* arrayref = (std::vector<int>*) this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	int auxChar = (((*arrayref)[index+1]) & 0x000000FF);


	this->frameStack.top().operandStack.push(auxChar);

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	Index no array: " << index << " -- Topo do operand stack(char carregado): " << (char) this->frameStack.top().operandStack.top()  << std::endl;
#endif
}

void RuntimeEngine::i_castore(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_castore -- completo -- linha: " << __LINE__ << std::endl;
#endif

	int value = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	unsigned int index = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	if(this->frameStack.top().operandStack.top() == 0){
		std::cout << " ------NullPointerException------ " << std::endl;
	}

	std::vector<int>* arrayref = (std::vector<int>*) this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	(*arrayref)[index+1] = value & 0x000000FF;

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	Index no array: " << index << " -- char carregado no index: " << (char) (*arrayref)[index+1]  << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_checkcast(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_checkcast -- linha: " << __LINE__ << std::endl;
#endif

	this->frameStack.top().cp+=3;

#if DEBUGMODE == 1
	std::cout << "	intruction opcional " << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_d2f(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_d2f -- linha: " << __LINE__ << std::endl;
#endif

	int upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	int lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	double value;

	float intermedAux;

	char* floatAux;
	char* doubleAux;
	char* intAux;

	doubleAux =(char*) &value;
	intAux =(char*) &upperAux;

	doubleAux[0] = intAux[0];
	doubleAux[1] = intAux[1];
	doubleAux[2] = intAux[2];
	doubleAux[3] = intAux[3];

	intAux =(char*) &lowerAux;

	doubleAux[4] = intAux[0];
	doubleAux[5] = intAux[1];
	doubleAux[6] = intAux[2];
	doubleAux[7] = intAux[3];

	intermedAux = (float) value;

	int result;

	intAux =(char*) &result;
	floatAux = (char*) &intermedAux;

	intAux[0] = floatAux[0];
	intAux[1] = floatAux[1];
	intAux[2] = floatAux[2];
	intAux[3] = floatAux[3];

	this->frameStack.top().operandStack.push(result);

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	float colocado no operandStack: " << intermedAux << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_d2i(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_d2i -- linha: " << __LINE__ << std::endl;
#endif

	int upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	int lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	double value;
	int result;

	char* doubleAux;
	char* intAux;

	doubleAux =(char*) &value;
	intAux =(char*) &upperAux;

	doubleAux[0] = intAux[0];
	doubleAux[1] = intAux[1];
	doubleAux[2] = intAux[2];
	doubleAux[3] = intAux[3];

	intAux =(char*) &lowerAux;

	doubleAux[4] = intAux[0];
	doubleAux[5] = intAux[1];
	doubleAux[6] = intAux[2];
	doubleAux[7] = intAux[3];

	if(std::isnan(value)){
		result = 0;
	}else if(std::isinf(value)){
		if(value < 0){
			result = INT_MIN;
		}else if(value > 0){
			result = INT_MAX;
		}

	}else{
		result = (int) value;
	}

	this->frameStack.top().operandStack.push(result);

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	int colocado no operandStack: " << result << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_d2l(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_d2l -- linha: " << __LINE__ << std::endl;
#endif

	int upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	int lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	double value;
	long long result;

	char* doubleAux;
	char* intAux;
	char* longAux;

	doubleAux =(char*) &value;
	intAux =(char*) &upperAux;

	doubleAux[0] = intAux[0];
	doubleAux[1] = intAux[1];
	doubleAux[2] = intAux[2];
	doubleAux[3] = intAux[3];

	intAux =(char*) &lowerAux;

	doubleAux[4] = intAux[0];
	doubleAux[5] = intAux[1];
	doubleAux[6] = intAux[2];
	doubleAux[7] = intAux[3];

	if(std::isnan(value)){
		result = 0;
	}else if(std::isinf(value)){
		if(value < 0){
			result = LLONG_MIN;
		}else if(value > 0){
			result = LLONG_MAX;
		}

	}else{
		result = (long long) value;
	}

	longAux = (char*) result;
	intAux = (char*) &upperAux;

	intAux[0] = longAux[0];
	intAux[1] = longAux[1];
	intAux[2] = longAux[2];
	intAux[3] = longAux[3];

	intAux = (char*) &lowerAux;

	intAux[0] = longAux[4];
	intAux[1] = longAux[5];
	intAux[2] = longAux[6];
	intAux[3] = longAux[7];

	this->frameStack.top().operandStack.push(lowerAux);
	this->frameStack.top().operandStack.push(upperAux);

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	long colocado no operandStack: " << result << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_dadd(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_dadd -- linha: " << __LINE__ << std::endl;
#endif

	int upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	int lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	double value2;

	char* doubleAux;
	char* intAux;

	doubleAux =(char*) &value2;
	intAux =(char*) &upperAux;

	doubleAux[0] = intAux[0];
	doubleAux[1] = intAux[1];
	doubleAux[2] = intAux[2];
	doubleAux[3] = intAux[3];

	intAux =(char*) &lowerAux;

	doubleAux[4] = intAux[0];
	doubleAux[5] = intAux[1];
	doubleAux[6] = intAux[2];
	doubleAux[7] = intAux[3];

	upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	double value1;

	doubleAux =(char*) &value1;
	intAux =(char*) &upperAux;

	doubleAux[0] = intAux[0];
	doubleAux[1] = intAux[1];
	doubleAux[2] = intAux[2];
	doubleAux[3] = intAux[3];

	intAux =(char*) &lowerAux;

	doubleAux[4] = intAux[0];
	doubleAux[5] = intAux[1];
	doubleAux[6] = intAux[2];
	doubleAux[7] = intAux[3];

	double result = value1 + value2;

	doubleAux =(char*) &result;
	intAux = (char*) &upperAux;

	intAux[0] = doubleAux[0];
	intAux[1] = doubleAux[1];
	intAux[2] = doubleAux[2];
	intAux[3] = doubleAux[3];

	intAux = (char*) &lowerAux;

	intAux[0] = doubleAux[4];
	intAux[1] = doubleAux[5];
	intAux[2] = doubleAux[6];
	intAux[3] = doubleAux[7];

	this->frameStack.top().operandStack.push(lowerAux);
	this->frameStack.top().operandStack.push(upperAux);

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	doubles utilizados(double1 + double2) - double1: " << value1 << " - double2: " << value2 << " - resultado empilhado: " << result <<  std::endl << std::endl;
#endif
}


void RuntimeEngine::i_daload(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_daload -- linha: " << __LINE__ << std::endl;
#endif

	unsigned int index = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	if(this->frameStack.top().operandStack.top() == 0){
		std::cout << " ------NullPointerException------ " << std::endl;
	}

	std::vector<int>* arrayref = (std::vector<int>*) this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	/*
	 * o double � colocado em 2 ints no array, na posi��o index*2 � o upperHalf e index*2+1 � o lowerHalf
	 */

	this->frameStack.top().operandStack.push((*arrayref)[(index*2)+2]);
	this->frameStack.top().operandStack.push((*arrayref)[(index*2)+1]);

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	double value;
	int upperAux = (*arrayref)[index*2];
	int lowerAux = (*arrayref)[index*2+1];
	uniteDWords((char*)&value, (char*)&upperAux, (char*)&lowerAux);
	std::cout << "	Index no array: " << index << " -- valor double dado push no operand stack(valor carregado): " << value << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_dastore(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_dastore -- linha: " << __LINE__ << std::endl;
#endif

	int upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	int lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	unsigned int index = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	if(this->frameStack.top().operandStack.top() == 0){
		std::cout << " ------NullPointerException------ " << std::endl;
	}

	std::vector<int>* arrayref = (std::vector<int>*) this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	/*
	 * o double � colocado em 2 ints no array, na posi��o index*2 � o upperHalf e index*2+1 � o lowerHalf
	 */

	(*arrayref)[(index*2)+1] = upperAux;

	(*arrayref)[(index*2)+2] = lowerAux;

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	double value;
	uniteDWords((char*)&value, (char*)&upperAux, (char*)&lowerAux);
	std::cout << "	Index no array: " << index << " -- valor double colocado no array(valor carregado): " << value << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_dcmpg(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_dcmpg -- linha: " << __LINE__ << std::endl;
#endif

	int upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	int lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	double value2;

	uniteDWords((char*)&value2, (char*)&upperAux, (char*)&lowerAux);

	upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	double value1;

	uniteDWords((char*)&value1, (char*)&upperAux, (char*)&lowerAux);

	if((std::isnan(value1)) || (std::isnan(value2))){
		this->frameStack.top().operandStack.push(1);
	}else if(value1 > value2){
		this->frameStack.top().operandStack.push(1);
	}else if(value1 == value2){
		this->frameStack.top().operandStack.push(0);
	}else if(value1 < value2){
		this->frameStack.top().operandStack.push(-1);
	}

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	doubles comparados - double1: " << value1 << " - double2: " << value2 << " - resultado empilhado(topo do operand stack): " << this->frameStack.top().operandStack.top() <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_dcmpl(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_dcmpl -- linha: " << __LINE__ << std::endl;
#endif

	int upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	int lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	double value2;

	uniteDWords((char*)&value2, (char*)&upperAux, (char*)&lowerAux);

	upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	double value1;

	uniteDWords((char*)&value1, (char*)&upperAux, (char*)&lowerAux);

	if((std::isnan(value1)) || (std::isnan(value2))){
		this->frameStack.top().operandStack.push(-1);
	}else if(value1 > value2){
		this->frameStack.top().operandStack.push(1);
	}else if(value1 == value2){
		this->frameStack.top().operandStack.push(0);
	}else if(value1 < value2){
		this->frameStack.top().operandStack.push(-1);
	}

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	doubles comparados - double1: " << value1 << " - double2: " << value2 << " - resultado empilhado(topo do operand stack): " << this->frameStack.top().operandStack.top() <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_dconst_0(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_dconst_0 -- linha: " << __LINE__ << std::endl;
#endif

	double value = 0.0;
	int upperAux, lowerAux;

	splitQWord((char*)&value, (char*)&upperAux, (char*)&lowerAux);

	this->frameStack.top().operandStack.push(lowerAux);
	this->frameStack.top().operandStack.push(upperAux);

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	valor empilhado(topo do operand stack): " << value <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_dconst_1(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_dconst_1 -- linha: " << __LINE__ << std::endl;
#endif

	double value = 1.0;
	int upperAux, lowerAux;

	splitQWord((char*)&value, (char*)&upperAux, (char*)&lowerAux);

	this->frameStack.top().operandStack.push(lowerAux);
	this->frameStack.top().operandStack.push(upperAux);

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	valor empilhado(topo do operand stack): " << value <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_ddiv(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_ddiv -- linha: " << __LINE__ << std::endl;
#endif

	int upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	int lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	double value2;

	uniteDWords((char*)&value2, (char*)&upperAux, (char*)&lowerAux);

	upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	double value1;

	uniteDWords((char*)&value1, (char*)&upperAux, (char*)&lowerAux);

	double result = value1/value2;

	splitQWord((char*)&result, (char*)&upperAux, (char*)&lowerAux);

	this->frameStack.top().operandStack.push(lowerAux);
	this->frameStack.top().operandStack.push(upperAux);

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	doubles utilizados(double1/double2) - double1: " << value1 << " - double2: " << value2 << " - resultado empilhado: " << result <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_dload(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_dload -- completo -- linha: " << __LINE__  << std::endl;
#endif
	unsigned short index;

	if(this->wideFlag){
		index = 0;
   		char* auxshort = ( char* ) &index;
   		auxshort[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+1];
   		auxshort[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+2];
	}else{
		index = (unsigned short) this->frameStack.top().bytecode[this->frameStack.top().cp+1];
	}
	int upperAux = this->frameStack.top().arrayLocalVariable[(unsigned short) index];
	int lowerAux = this->frameStack.top().arrayLocalVariable[(unsigned short) index+1];
	this->frameStack.top().operandStack.push(lowerAux);
	this->frameStack.top().operandStack.push(upperAux);
	if(this->wideFlag){
		this->frameStack.top().cp +=3;
		this->wideFlag = false;
	}else{
		this->frameStack.top().cp +=2;
	}
#if DEBUGMODE == 1
	double value;
	uniteDWords((char*)&value, (char*)&upperAux, (char*)&lowerAux);
	std::cout << "	Index da local variable: " << (unsigned short) index <<  " -  double carregado da variavel (topo do operandStack)"  << value << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_dload_0(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_dload_0 -- completo -- linha: " << __LINE__  << std::endl;
#endif
	int upperAux = this->frameStack.top().arrayLocalVariable[(unsigned short) 0];
	int lowerAux = this->frameStack.top().arrayLocalVariable[(unsigned short) 1];
	this->frameStack.top().operandStack.push(lowerAux);
	this->frameStack.top().operandStack.push(upperAux);
	this->frameStack.top().cp ++;
#if DEBUGMODE == 1
	double value;
	uniteDWords((char*)&value, (char*)&upperAux, (char*)&lowerAux);
	std::cout << "	Index da local variable: " << 0 <<  " -  double carregada da variavel (topo do operandStack)"  << value << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_dload_1(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_dload_1 -- completo -- linha: " << __LINE__  << std::endl;
#endif
	int upperAux = this->frameStack.top().arrayLocalVariable[(unsigned short) 1];
	int lowerAux = this->frameStack.top().arrayLocalVariable[(unsigned short) 2];
	this->frameStack.top().operandStack.push(lowerAux);
	this->frameStack.top().operandStack.push(upperAux);
	this->frameStack.top().cp ++;
#if DEBUGMODE == 1
	double value;
	uniteDWords((char*)&value, (char*)&upperAux, (char*)&lowerAux);
	std::cout << "	Index da local variable: " << 1 <<  " -  double carregada da variavel (topo do operandStack)"  << value << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_dload_2(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_dload_2 -- completo -- linha: " << __LINE__  << std::endl;
#endif
	int upperAux = this->frameStack.top().arrayLocalVariable[(unsigned short) 2];
	int lowerAux = this->frameStack.top().arrayLocalVariable[(unsigned short) 3];
	this->frameStack.top().operandStack.push(lowerAux);
	this->frameStack.top().operandStack.push(upperAux);
	this->frameStack.top().cp ++;
#if DEBUGMODE == 1
	double value;
	uniteDWords((char*)&value, (char*)&upperAux, (char*)&lowerAux);
	std::cout << "	Index da local variable: " << 2 <<  " -  double carregada da variavel (topo do operandStack)"  << value << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_dload_3(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_dload_3 -- completo -- linha: " << __LINE__  << std::endl;
#endif
	int upperAux = this->frameStack.top().arrayLocalVariable[(unsigned short) 3];
	int lowerAux = this->frameStack.top().arrayLocalVariable[(unsigned short) 4];
	this->frameStack.top().operandStack.push(lowerAux);
	this->frameStack.top().operandStack.push(upperAux);
	this->frameStack.top().cp ++;
#if DEBUGMODE == 1
	double value;
	uniteDWords((char*)&value, (char*)&upperAux, (char*)&lowerAux);
	std::cout << "	Index da local variable: " << 3 <<  " -  double carregada da variavel (topo do operandStack)"  << value << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_dmul(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_dmul -- linha: " << __LINE__ << std::endl;
#endif

	int upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	int lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	double value2;

	uniteDWords((char*)&value2, (char*)&upperAux, (char*)&lowerAux);

	upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	double value1;

	uniteDWords((char*)&value1, (char*)&upperAux, (char*)&lowerAux);

	double result = value1*value2;

	splitQWord((char*)&result, (char*)&upperAux, (char*)&lowerAux);

	this->frameStack.top().operandStack.push(lowerAux);
	this->frameStack.top().operandStack.push(upperAux);

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	doubles utilizados(double1*double2) - double1: " << value1 << " - double2: " << value2 << " - resultado empilhado: " << result <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_dneg(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_dneg -- linha: " << __LINE__ << std::endl;
#endif

	int upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	int lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	double value;

	uniteDWords((char*)&value, (char*)&upperAux, (char*)&lowerAux);

	double result = value * -1;

	splitQWord((char*)&result, (char*)&upperAux, (char*)&lowerAux);

	this->frameStack.top().operandStack.push(lowerAux);
	this->frameStack.top().operandStack.push(upperAux);

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	double utilizado(double * -1) - double: " << value << " - resultado empilhado: " << result <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_drem(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_drem -- linha: " << __LINE__ << std::endl;
#endif

	int upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	int lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	double value2;

	uniteDWords((char*)&value2, (char*)&upperAux, (char*)&lowerAux);

	upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	double value1;

	uniteDWords((char*)&value1, (char*)&upperAux, (char*)&lowerAux);

	double result =  std::fmod(value1,value2);

	splitQWord((char*)&result, (char*)&upperAux, (char*)&lowerAux);

	this->frameStack.top().operandStack.push(lowerAux);
	this->frameStack.top().operandStack.push(upperAux);

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	doubles utilizados(std::fmod(double1, double2) - double1: " << value1 << " - double2: " << value2 << " - resultado empilhado: " << result <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_dreturn(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_dreturn -- completo -- linha: " << __LINE__   << std::endl;
#endif
	int upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();
	int lowerAux = this->frameStack.top().operandStack.top();

    this->frameStack.pop();

    this->frameStack.top().operandStack.push(lowerAux);
	this->frameStack.top().operandStack.push(upperAux);
#if DEBUGMODE == 1
	double value;
	uniteDWords((char*)&value, (char*)&upperAux, (char*)&lowerAux);
    std::cout << "	double retornado: " << value  << std::endl;
	std::cout << " ++++++++++++++++++++++++++++ " << this->frameStack.size()+1 << " ++++++++++++++++++++++++++++	" << std::endl;
#endif
}

void RuntimeEngine::i_dstore(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_dstore -- linha: " << __LINE__   << std::endl;
#endif

	unsigned short index;

	if(this->wideFlag){
		index = 0;
   		char* auxshort = ( char* ) &index;
   		auxshort[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+1];
   		auxshort[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+2];
	}else{
		index = (unsigned short) this->frameStack.top().bytecode[this->frameStack.top().cp+1];
	}

	int upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();
	int lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	this->frameStack.top().arrayLocalVariable[index]= upperAux;

	this->frameStack.top().arrayLocalVariable[index+1]= lowerAux;

	if(this->wideFlag){
		this->frameStack.top().cp +=3;
		this->wideFlag = false;
	}else{
		this->frameStack.top().cp +=2;
	}

#if DEBUGMODE == 1
	double value;
	uniteDWords((char*)&value, (char*)&this->frameStack.top().arrayLocalVariable[index], (char*)&this->frameStack.top().arrayLocalVariable[index+1]);
	std::cout << "	index da variavel: " << (unsigned short) index  << " - double carregada nas variaveis index e index+1: "  << value << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_dstore_0(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_dstore_0 -- linha: " << __LINE__   << std::endl;
#endif

	int upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();
	int lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	this->frameStack.top().arrayLocalVariable[0]= upperAux;

	this->frameStack.top().arrayLocalVariable[1]= lowerAux;

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	double value;
	uniteDWords((char*)&value, (char*)&this->frameStack.top().arrayLocalVariable[0], (char*)&this->frameStack.top().arrayLocalVariable[1]);
	std::cout << "	index da variavel: " << (unsigned short) 0  << " - double carregada nas variaveis index e index+1: "  << value << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_dstore_1(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_dstore_1 -- linha: " << __LINE__   << std::endl;
#endif

	int upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();
	int lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	this->frameStack.top().arrayLocalVariable[1]= upperAux;

	this->frameStack.top().arrayLocalVariable[2]= lowerAux;

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	double value;
	uniteDWords((char*)&value, (char*)&this->frameStack.top().arrayLocalVariable[1], (char*)&this->frameStack.top().arrayLocalVariable[2]);
	std::cout << "	index da variavel: " << (unsigned short) 1  << " - double carregada nas variaveis index e index+1: "  << value << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_dstore_2(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_dstore_2 -- linha: " << __LINE__   << std::endl;
#endif

	int upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();
	int lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	this->frameStack.top().arrayLocalVariable[2]= upperAux;

	this->frameStack.top().arrayLocalVariable[3]= lowerAux;

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	double value;
	uniteDWords((char*)&value, (char*)&this->frameStack.top().arrayLocalVariable[2], (char*)&this->frameStack.top().arrayLocalVariable[3]);
	std::cout << "	index da variavel: " << (unsigned short) 2  << " - double carregada nas variaveis index e index+1: "  << value << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_dstore_3(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_dstore_3 -- linha: " << __LINE__   << std::endl;
#endif

	int upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();
	int lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	this->frameStack.top().arrayLocalVariable[3]= upperAux;

	this->frameStack.top().arrayLocalVariable[4]= lowerAux;

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	double value;
	uniteDWords((char*)&value, (char*)&this->frameStack.top().arrayLocalVariable[3], (char*)&this->frameStack.top().arrayLocalVariable[4]);
	std::cout << "	index da variavel: " << (unsigned short) 3  << " - double carregada nas variaveis index e index+1: "  << value << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_dsub(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_dsub -- linha: " << __LINE__ << std::endl;
#endif

	int upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	int lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	double value2;

	uniteDWords((char*)&value2, (char*)&upperAux, (char*)&lowerAux);

	upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	double value1;

	uniteDWords((char*)&value1, (char*)&upperAux, (char*)&lowerAux);

	double result = value1 - value2;

	splitQWord((char*)&result, (char*)&upperAux, (char*)&lowerAux);

	this->frameStack.top().operandStack.push(lowerAux);
	this->frameStack.top().operandStack.push(upperAux);

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	doubles utilizados(double1 - double2) - double1: " << value1 << " - double2: " << value2 << " - resultado empilhado: " << result <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_dup(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_dup -- linha: " << __LINE__ << std::endl;
#endif

	this->frameStack.top().operandStack.push(this->frameStack.top().operandStack.top());
	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	valor duplicado " << this->frameStack.top().operandStack.top() << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_dup_x1(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_dup_x1 -- linha: " << __LINE__ << std::endl;
#endif

	int value1 = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	int value2 = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	this->frameStack.top().operandStack.push(value1);
	this->frameStack.top().operandStack.push(value2);
	this->frameStack.top().operandStack.push(value1);

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	ultimos 3 valores do operando stack (da base para o top): " <<  value1 << " - " << value2 << " - " << this->frameStack.top().operandStack.top() << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_dup_x2(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_dup_x2 -- linha: " << __LINE__ << std::endl;
#endif

	int value1 = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	int value2 = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	int value3 = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	this->frameStack.top().operandStack.push(value1);
	this->frameStack.top().operandStack.push(value3);
	this->frameStack.top().operandStack.push(value2);
	this->frameStack.top().operandStack.push(value1);

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	ultimos 4 valores do operando stack (da base para o top): " <<  value1 << " - " << value3 << " - "  << value2 << " - " << this->frameStack.top().operandStack.top() << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_dup2(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_dup2 -- linha: " << __LINE__ << std::endl;
#endif

	int value1 = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	int value2 = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	this->frameStack.top().operandStack.push(value2);
	this->frameStack.top().operandStack.push(value1);
	this->frameStack.top().operandStack.push(value2);
	this->frameStack.top().operandStack.push(value1);

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	ultimos 2 valores do operando stack (da base para o top): " << value2 << " - " << this->frameStack.top().operandStack.top() << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_dup2_x1(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_dup2_x1 -- linha: " << __LINE__ << std::endl;
#endif

	int value1 = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	int value2 = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	int value3 = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	this->frameStack.top().operandStack.push(value2);
	this->frameStack.top().operandStack.push(value1);
	this->frameStack.top().operandStack.push(value3);
	this->frameStack.top().operandStack.push(value2);
	this->frameStack.top().operandStack.push(value1);

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	ultimos 5 valores do operando stack (da base para o top): " <<  value2 << " - " << value1 << " - "  << value3 << " - " << value2 << " - " << this->frameStack.top().operandStack.top() << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_dup2_x2(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_dup2_x2 -- linha: " << __LINE__ << std::endl;
#endif

	int value1 = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	int value2 = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	int value3 = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	int value4 = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	this->frameStack.top().operandStack.push(value2);
	this->frameStack.top().operandStack.push(value1);
	this->frameStack.top().operandStack.push(value4);
	this->frameStack.top().operandStack.push(value3);
	this->frameStack.top().operandStack.push(value2);
	this->frameStack.top().operandStack.push(value1);

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	ultimos 6 valores do operando stack (da base para o top): " <<  value2 << " - " << value1 << " - "  << value4 << " - " << value3 << " - " << value2 << " - " << this->frameStack.top().operandStack.top() << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_f2d(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_f2d -- linha: " << __LINE__ << std::endl;
#endif

	int aux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	float value;

	copyBitsetDword((char*) &value, (char*) &aux);

	double result;

	result = (double) value;

	int upperAux, lowerAux;

	splitQWord((char*)&result, (char*)&upperAux, (char*)&lowerAux);

	this->frameStack.top().operandStack.push(lowerAux);
	this->frameStack.top().operandStack.push(upperAux);

	this->frameStack.top().cp++;

#if DEBUGMODE == 1
	std::cout << "	double empilhado: " << result << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_f2i(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_f2i -- linha: " << __LINE__ << std::endl;
#endif

	int aux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	float value;

	copyBitsetDword((char*) &value, (char*) &aux);

	int result;

	result = (int) value;

	this->frameStack.top().operandStack.push(result);

	this->frameStack.top().cp++;

#if DEBUGMODE == 1
	std::cout << "	int empilhado: " << result << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_f2l(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_f2l -- linha: " << __LINE__ << std::endl;
#endif

	int aux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	float value;

	copyBitsetDword((char*) &value, (char*) &aux);

	long long result;

	result = (long long) value;

	int upperAux, lowerAux;

	splitQWord((char*)&result, (char*)&upperAux, (char*)&lowerAux);

	this->frameStack.top().operandStack.push(lowerAux);
	this->frameStack.top().operandStack.push(upperAux);

	this->frameStack.top().cp++;

#if DEBUGMODE == 1
	std::cout << "	long empilhado: " << result << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_fadd(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_fadd -- linha: " << __LINE__ << std::endl;
#endif

	float value1, value2;

	int aux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	copyBitsetDword((char*) &value2, (char*) &aux);

	aux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	copyBitsetDword((char*) &value1, (char*) &aux);

	float result;

	result =  value1 + value2;

	copyBitsetDword((char*) &aux, (char*) &result);

	this->frameStack.top().operandStack.push(aux);

	this->frameStack.top().cp++;

#if DEBUGMODE == 1
	std::cout << "	floats utilizados(float1 + float2) - float1: " << value1 << " - float2: " << value2 << " - resultado empilhado: " << result <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_faload(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_faload -- completo -- linha: " << __LINE__ << std::endl;
#endif

	unsigned int index = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	if(this->frameStack.top().operandStack.top() == 0){
		std::cout << " ------NullPointerException------ " << std::endl;
	}

	std::vector<int>* arrayref = (std::vector<int>*) this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	this->frameStack.top().operandStack.push((*arrayref)[index+1]);

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	float value;
	copyBitsetDword((char*) &value, (char*) &this->frameStack.top().operandStack.top());
	std::cout << "	Index no array: " << index << " -- Topo do operand stack(float carregado): " << value << std::endl;
#endif
}

void RuntimeEngine::i_fastore(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_fastore -- completo -- linha: " << __LINE__ << std::endl;
#endif

	int value = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	unsigned int index = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	if(this->frameStack.top().operandStack.top() == 0){
		std::cout << " ------NullPointerException------ " << std::endl;
	}

	std::vector<int>* arrayref = (std::vector<int>*) this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	(*arrayref)[index+1] = value;

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	float valuef;
	copyBitsetDword((char*) &valuef, (char*) &(*arrayref)[index+1]);
	std::cout << "	Index no array: " << index << " -- Topo do operand stack(float carregado): " << valuef << std::endl;
#endif
}

void RuntimeEngine::i_fcmpg(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_fcmpg -- linha: " << __LINE__ << std::endl;
#endif


	float value1, value2;

	int aux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	copyBitsetDword((char*) &value2, (char*) &aux);

	aux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	copyBitsetDword((char*) &value1, (char*) &aux);

	if((std::isnan(value1)) || (std::isnan(value2))){
		this->frameStack.top().operandStack.push(1);
	}else if(value1 > value2){
		this->frameStack.top().operandStack.push(1);
	}else if(value1 == value2){
		this->frameStack.top().operandStack.push(0);
	}else if(value1 < value2){
		this->frameStack.top().operandStack.push(-1);
	}

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	floats comparados - float1: " << value1 << " - float2: " << value2 << " - resultado empilhado(topo do operand stack): " << this->frameStack.top().operandStack.top() <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_fcmpl(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_fcmpl -- linha: " << __LINE__ << std::endl;
#endif


	float value1, value2;

	int aux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	copyBitsetDword((char*) &value2, (char*) &aux);

	aux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	copyBitsetDword((char*) &value1, (char*) &aux);

	if((std::isnan(value1)) || (std::isnan(value2))){
		this->frameStack.top().operandStack.push(-1);
	}else if(value1 > value2){
		this->frameStack.top().operandStack.push(1);
	}else if(value1 == value2){
		this->frameStack.top().operandStack.push(0);
	}else if(value1 < value2){
		this->frameStack.top().operandStack.push(-1);
	}

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	floats comparados - float1: " << value1 << " - float2: " << value2 << " - resultado empilhado(topo do operand stack): " << this->frameStack.top().operandStack.top() <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_fconst_0(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_fconst_0 -- linha: " << __LINE__ << std::endl;
#endif

	float value = 0.0;
	int aux;

	copyBitsetDword((char*) &aux, (char*) &value);

	this->frameStack.top().operandStack.push(aux);


	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	valor empilhado(topo do operand stack): " << value <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_fconst_1(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_fconst_1 -- linha: " << __LINE__ << std::endl;
#endif

	float value = 1.0;
	int aux;

	copyBitsetDword((char*) &aux, (char*) &value);

	this->frameStack.top().operandStack.push(aux);


	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	valor empilhado(topo do operand stack): " << value <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_fconst_2(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_fconst_2 -- linha: " << __LINE__ << std::endl;
#endif

	float value = 2.0;
	int aux;

	copyBitsetDword((char*) &aux, (char*) &value);

	this->frameStack.top().operandStack.push(aux);


	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	valor empilhado(topo do operand stack): " << value <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_fdiv(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_fdiv -- linha: " << __LINE__ << std::endl;
#endif

	float value1, value2;

	int aux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	copyBitsetDword((char*) &value2, (char*) &aux);

	aux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	copyBitsetDword((char*) &value1, (char*) &aux);

	float result;

	result =  value1 / value2;

	copyBitsetDword((char*) &aux, (char*) &result);

	this->frameStack.top().operandStack.push(aux);

	this->frameStack.top().cp++;

#if DEBUGMODE == 1
	std::cout << "	floats utilizados(float1 / float2) - float1: " << value1 << " - float2: " << value2 << " - resultado empilhado: " << result <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_fload(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_fload -- completo -- linha: " << __LINE__  << std::endl;
#endif

	unsigned short index;

	if(this->wideFlag){
		index = 0;
   		char* auxshort = ( char* ) &index;
   		auxshort[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+1];
   		auxshort[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+2];
	}else{
		index = (unsigned short) this->frameStack.top().bytecode[this->frameStack.top().cp+1];
	}

	int value = this->frameStack.top().arrayLocalVariable[(unsigned short) index];

	this->frameStack.top().operandStack.push(value);

	if(this->wideFlag){
		this->frameStack.top().cp +=3;
		this->wideFlag = false;
	}else{
		this->frameStack.top().cp +=2;
	}

#if DEBUGMODE == 1
	float aux;
	copyBitsetDword((char*) &aux, (char*) &this->frameStack.top().operandStack.top());
	std::cout << "	Index da local variable: " << (unsigned short) index <<  " -  float carregado da variavel (topo do operandStack)"  << aux << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_fload_0(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_fload_0 -- completo -- linha: " << __LINE__  << std::endl;
#endif
	this->frameStack.top().operandStack.push(this->frameStack.top().arrayLocalVariable[0]);
	this->frameStack.top().cp ++;
#if DEBUGMODE == 1
	float aux;
	copyBitsetDword((char*) &aux, (char*) &this->frameStack.top().operandStack.top());
	std::cout << "	float carregado da variavel (topo do operandStack): "  << aux << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_fload_1(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_fload_1 -- completo -- linha: " << __LINE__  << std::endl;
#endif
	this->frameStack.top().operandStack.push(this->frameStack.top().arrayLocalVariable[1]);
	this->frameStack.top().cp ++;
#if DEBUGMODE == 1
	float aux;
	copyBitsetDword((char*) &aux, (char*) &this->frameStack.top().operandStack.top());
	std::cout << "	float carregado da variavel (topo do operandStack): "  << aux << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_fload_2(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_fload_2 -- completo -- linha: " << __LINE__  << std::endl;
#endif
	this->frameStack.top().operandStack.push(this->frameStack.top().arrayLocalVariable[2]);
	this->frameStack.top().cp ++;
#if DEBUGMODE == 1
	float aux;
	copyBitsetDword((char*) &aux, (char*) &this->frameStack.top().operandStack.top());
	std::cout << "	float carregado da variavel (topo do operandStack): "  << aux << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_fload_3(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_fload_3 -- completo -- linha: " << __LINE__  << std::endl;
#endif
	this->frameStack.top().operandStack.push(this->frameStack.top().arrayLocalVariable[3]);
	this->frameStack.top().cp ++;
#if DEBUGMODE == 1
	float aux;
	copyBitsetDword((char*) &aux, (char*) &this->frameStack.top().operandStack.top());
	std::cout << "	float carregado da variavel (topo do operandStack): "  << aux << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_fmul(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_fmul -- linha: " << __LINE__ << std::endl;
#endif

	float value1, value2;

	int aux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	copyBitsetDword((char*) &value2, (char*) &aux);

	aux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	copyBitsetDword((char*) &value1, (char*) &aux);

	float result;

	result =  value1 * value2;

	copyBitsetDword((char*) &aux, (char*) &result);

	this->frameStack.top().operandStack.push(aux);

	this->frameStack.top().cp++;

#if DEBUGMODE == 1
	std::cout << "	floats utilizados(float1 * float2) - float1: " << value1 << " - float2: " << value2 << " - resultado empilhado: " << result <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_fneg(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_fneg -- linha: " << __LINE__ << std::endl;
#endif

	float value;

	int aux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	copyBitsetDword((char*) &value, (char*) &aux);

	float result;

	result =  value * -1.0;

	copyBitsetDword((char*) &aux, (char*) &result);

	this->frameStack.top().operandStack.push(aux);

	this->frameStack.top().cp++;

#if DEBUGMODE == 1
	std::cout << "	float utilizado(float * -1.0) - float1: " << value  << " - resultado empilhado: " << result <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_frem(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_frem -- linha: " << __LINE__ << std::endl;
#endif

	float value1, value2;

	int aux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	copyBitsetDword((char*) &value2, (char*) &aux);

	aux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	copyBitsetDword((char*) &value1, (char*) &aux);

	float result;

	result =  std::fmod(value1,value2);

	copyBitsetDword((char*) &aux, (char*) &result);

	this->frameStack.top().operandStack.push(aux);

	this->frameStack.top().cp++;

#if DEBUGMODE == 1
	std::cout << "	floats utilizados(std::fmod(float1,float2)) - float1: " << value1 << " - float2: " << value2 << " - resultado empilhado: " << result <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_freturn(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_freturn -- completo -- linha: " << __LINE__   << std::endl;
#endif
    int retorno = this->frameStack.top().operandStack.top();

    this->frameStack.pop();

    this->frameStack.top().operandStack.push(retorno);

#if DEBUGMODE == 1
	float aux;
	copyBitsetDword((char*) &aux, (char*) &this->frameStack.top().operandStack.top());
   std::cout << "	float retornada: " << aux  << std::endl;
	std::cout << " ++++++++++++++++++++++++++++ " << this->frameStack.size()+1 << " ++++++++++++++++++++++++++++	" << std::endl;
#endif
}

void RuntimeEngine::i_fstore(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_fstore -- linha: " << __LINE__   << std::endl;
#endif

	unsigned short index;

	if(this->wideFlag){
		index = 0;
   		char* auxshort = ( char* ) &index;
   		auxshort[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+1];
   		auxshort[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+2];
	}else{
		index = (unsigned short) this->frameStack.top().bytecode[this->frameStack.top().cp+1];
	}

	int value = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	this->frameStack.top().arrayLocalVariable[index]= value;

	if(this->wideFlag){
		this->frameStack.top().cp +=3;
		this->wideFlag = false;
	}else{
		this->frameStack.top().cp +=2;
	}

#if DEBUGMODE == 1
	float aux;
	copyBitsetDword((char*) &aux, (char*) &this->frameStack.top().arrayLocalVariable[index]);
	std::cout << "	Index da local variable: " << (unsigned short) index <<  " -  float carregado na variavel: "  << aux << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_fstore_0(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_fstore_0 -- linha: " << __LINE__   << std::endl;
#endif

	int value = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	this->frameStack.top().arrayLocalVariable[0]= value;

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	float aux;
	copyBitsetDword((char*) &aux, (char*) &this->frameStack.top().arrayLocalVariable[0]);
	std::cout << "	float carregado na variavel: "  << aux << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_fstore_1(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_fstore_1 -- linha: " << __LINE__   << std::endl;
#endif

	int value = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	this->frameStack.top().arrayLocalVariable[1]= value;

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	float aux;
	copyBitsetDword((char*) &aux, (char*) &this->frameStack.top().arrayLocalVariable[1]);
	std::cout << "	float carregado na variavel: "  << aux << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_fstore_2(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_fstore_2 -- linha: " << __LINE__   << std::endl;
#endif

	int value = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	this->frameStack.top().arrayLocalVariable[2]= value;

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	float aux;
	copyBitsetDword((char*) &aux, (char*) &this->frameStack.top().arrayLocalVariable[2]);
	std::cout << "	float carregado na variavel: "  << aux << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_fstore_3(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_fstore_3 -- linha: " << __LINE__   << std::endl;
#endif

	int value = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	this->frameStack.top().arrayLocalVariable[3]= value;

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	float aux;
	copyBitsetDword((char*) &aux, (char*) &this->frameStack.top().arrayLocalVariable[3]);
	std::cout << "	float carregado na variavel: "  << aux << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_fsub(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_fsub -- linha: " << __LINE__ << std::endl;
#endif

	float value1, value2;

	int aux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	copyBitsetDword((char*) &value2, (char*) &aux);

	aux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	copyBitsetDword((char*) &value1, (char*) &aux);

	float result;

	result =  value1 - value2;

	copyBitsetDword((char*) &aux, (char*) &result);

	this->frameStack.top().operandStack.push(aux);

	this->frameStack.top().cp++;

#if DEBUGMODE == 1
	std::cout << "	floats utilizados(float1 - float2) - float1: " << value1 << " - float2: " << value2 << " - resultado empilhado: " << result <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_goto(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_goto -- linha: " << __LINE__ << std::endl;
#endif

	short branchOffset = 0;
   	char* auxshort = ( char* ) &branchOffset;
   	auxshort[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+1];
   	auxshort[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+2];
    this->frameStack.top().cp += branchOffset;

#if DEBUGMODE == 1
	std::cout << "	Branch offset do Goto " << branchOffset << " - valor final do PC: " << this->frameStack.top().cp <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_goto_w(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_goto_w -- linha: " << __LINE__ << std::endl;
#endif

	int branchOffset = 0;
   	char* auxshort = ( char* ) &branchOffset;
   	auxshort[3] = this->frameStack.top().bytecode[this->frameStack.top().cp+1];
   	auxshort[2] = this->frameStack.top().bytecode[this->frameStack.top().cp+2];
   	auxshort[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+3];
   	auxshort[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+4];
    this->frameStack.top().cp += branchOffset;

#if DEBUGMODE == 1
	std::cout << "	Branch offset do Goto_w " << branchOffset << " - valor final do PC: " << this->frameStack.top().cp <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_i2b(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_i2b -- linha: " << __LINE__ << std::endl;
#endif

	int value = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	char aux;

	aux = (char) value;

	int result;

	result = (int) aux;

    this->frameStack.top().operandStack.push(result);

	this->frameStack.top().cp++;

#if DEBUGMODE == 1
	std::cout << "	byte pushed (topo do operand stack) " << this->frameStack.top().operandStack.top() << " - valor retirado da pilha(int): " << value <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_i2c(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_i2c -- linha: " << __LINE__ << std::endl;
#endif

	int value = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	unsigned char aux;

	aux = (unsigned char) value;

	int result;

	result = (int) aux;

    this->frameStack.top().operandStack.push(result);

	this->frameStack.top().cp++;

#if DEBUGMODE == 1
	std::cout << "	char pushed (topo do operand stack) " << (char) this->frameStack.top().operandStack.top() << " - valor retirado da pilha(int): " << value <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_i2d(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_i2d -- linha: " << __LINE__ << std::endl;
#endif

	int value = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	double result;

	result = (double) value;

	int upperAux, lowerAux;

	splitQWord((char*)&result, (char*)&upperAux, (char*)&lowerAux);

	this->frameStack.top().operandStack.push(lowerAux);
	this->frameStack.top().operandStack.push(upperAux);

	this->frameStack.top().cp++;

#if DEBUGMODE == 1
	std::cout << "	double pushed: " <<  result << " - valor retirado da pilha(int): " << value <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_i2f(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_i2f -- linha: " << __LINE__ << std::endl;
#endif

	int value = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	float result;

	result = (float) value;

	int aux;

	copyBitsetDword((char*) &aux, (char*) &result);

	this->frameStack.top().operandStack.push(aux);

	this->frameStack.top().cp++;

#if DEBUGMODE == 1
	std::cout << "	float pushed: " <<  result << " - valor retirado da pilha(int): " << value <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_i2l(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_i2l -- linha: " << __LINE__ << std::endl;
#endif

	int value = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	long long result;

	result = (long long) value;

	int upperAux, lowerAux;

	splitQWord((char*)&result, (char*)&upperAux, (char*)&lowerAux);

	this->frameStack.top().operandStack.push(lowerAux);
	this->frameStack.top().operandStack.push(upperAux);

	this->frameStack.top().cp++;

#if DEBUGMODE == 1
	std::cout << "	long pushed: " <<  result << " - valor retirado da pilha(int): " << value <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_i2s(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_i2s -- linha: " << __LINE__ << std::endl;
#endif

	int value = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	short aux;

	aux = (short) value;

	int result;

	result = (int) aux;

    this->frameStack.top().operandStack.push(result);

	this->frameStack.top().cp++;

#if DEBUGMODE == 1
	std::cout << "	short pushed (topo do operand stack) " << this->frameStack.top().operandStack.top() << " - valor retirado da pilha(int): " << value <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_iadd(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_iadd -- linha: " << __LINE__ << std::endl;
#endif

	int value1, value2;

	value2 = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	value1 = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	int result;

	result =  value1 + value2;

	this->frameStack.top().operandStack.push(result);

	this->frameStack.top().cp++;

#if DEBUGMODE == 1
	std::cout << "	ints utilizados(int1 + int2) - int1: " << value1 << " - int2: " << value2 << " - resultado empilhado: " << result <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_iaload(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_iaload -- completo -- linha: " << __LINE__ << std::endl;
#endif

	unsigned int index = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	if(this->frameStack.top().operandStack.top() == 0){
		std::cout << " ------NullPointerException------ " << std::endl;
	}

	std::vector<int>* arrayref = (std::vector<int>*) this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	this->frameStack.top().operandStack.push((*arrayref)[index+1]);

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	int value;
	copyBitsetDword((char*) &value, (char*) &this->frameStack.top().operandStack.top());
	std::cout << "	Index no array: " << index << " -- Topo do operand stack(int carregado): " << value << std::endl;
#endif
}

void RuntimeEngine::i_iand(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_iand -- linha: " << __LINE__ << std::endl;
#endif

	int value1, value2;

	value2 = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	value1 = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	int result;

	result =  value1 & value2;

	this->frameStack.top().operandStack.push(result);

	this->frameStack.top().cp++;

#if DEBUGMODE == 1
	std::cout << "	ints utilizados(int1 & int2) - int1: " << value1 << " - int2: " << value2 << " - resultado empilhado: " << result <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_iastore(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_iastore -- completo -- linha: " << __LINE__ << std::endl;
#endif

	int value = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	unsigned int index = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	if(this->frameStack.top().operandStack.top() == 0){
		std::cout << " ------NullPointerException------ " << std::endl;
	}

	std::vector<int>* arrayref = (std::vector<int>*) this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	(*arrayref)[index+1] = value;

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	int valuef;
	copyBitsetDword((char*) &valuef, (char*) &(*arrayref)[index+1]);
	std::cout << "	Index no array: " << index << " -- Antigo topo do operand stack(int carregado): " << valuef << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_iconst_m1(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_iconst_m1 -- linha: " << __LINE__ << std::endl;
#endif

	int value = -1;

	this->frameStack.top().operandStack.push(value);

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	valor empilhado(topo do operand stack): " << value <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_iconst_0(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_iconst_0 -- linha: " << __LINE__ << std::endl;
#endif

	int value = 0;

	this->frameStack.top().operandStack.push(value);

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	valor empilhado(topo do operand stack): " << value <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_iconst_1(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_iconst_1 -- linha: " << __LINE__ << std::endl;
#endif

	int value = 1;

	this->frameStack.top().operandStack.push(value);

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	valor empilhado(topo do operand stack): " << value <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_iconst_2(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_iconst_2 -- linha: " << __LINE__ << std::endl;
#endif

	int value = 2;

	this->frameStack.top().operandStack.push(value);

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	valor empilhado(topo do operand stack): " << value <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_iconst_3(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_iconst_3 -- linha: " << __LINE__ << std::endl;
#endif

	int value = 3;

	this->frameStack.top().operandStack.push(value);

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	valor empilhado(topo do operand stack): " << value <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_iconst_4(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_iconst_4 -- linha: " << __LINE__ << std::endl;
#endif

	int value = 4;

	this->frameStack.top().operandStack.push(value);

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	valor empilhado(topo do operand stack): " << value <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_iconst_5(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_iconst_5 -- linha: " << __LINE__ << std::endl;
#endif

	int value = 5;

	this->frameStack.top().operandStack.push(value);

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	valor empilhado(topo do operand stack): " << value <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_idiv(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_idiv -- linha: " << __LINE__ << std::endl;
#endif

	int value1, value2;

	value2 = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	value1 = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	if(value2 == 0){
		std::cout << "	ArithmeticException " << std::endl;
	}

	int result;

	result =  value1 / value2;

	this->frameStack.top().operandStack.push(result);

	this->frameStack.top().cp++;

#if DEBUGMODE == 1
	std::cout << "	ints utilizados(int1 / int2) - int1: " << value1 << " - int2: " << value2 << " - resultado empilhado: " << result <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_if_acmpeq(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_if_acmpeq -- linha: " << __LINE__ << std::endl;
#endif

    int value2 = this->frameStack.top().operandStack.top();
    this->frameStack.top().operandStack.pop();
    int value1 = this->frameStack.top().operandStack.top();
    this->frameStack.top().operandStack.pop();

	if (value1 == value2) {
		short branchOffset = 0;
   		char* auxshort = ( char* ) &branchOffset;
   		auxshort[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+1];
   		auxshort[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+2];
        this->frameStack.top().cp += branchOffset;
    } else {
		this->frameStack.top().cp += 3;
    }

#if DEBUGMODE == 1
	std::cout << "	valores comparados em int(int1 = int2) - int1: " << value1 << " - int2: " << value2 << " - cp final: " << this->frameStack.top().cp <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_if_acmpne(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_if_acmpne -- linha: " << __LINE__ << std::endl;
#endif

    int value2 = this->frameStack.top().operandStack.top();
    this->frameStack.top().operandStack.pop();
    int value1 = this->frameStack.top().operandStack.top();
    this->frameStack.top().operandStack.pop();

	if (value1 != value2) {
		short branchOffset = 0;
   		char* auxshort = ( char* ) &branchOffset;
   		auxshort[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+1];
   		auxshort[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+2];
        this->frameStack.top().cp += branchOffset;
    } else {
		this->frameStack.top().cp += 3;
    }

#if DEBUGMODE == 1
	std::cout << "	valores comparados em int(int1 != int2) - int1: " << value1 << " - int2: " << value2 << " - cp final: " << this->frameStack.top().cp <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_if_icmpeq(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_if_icmpeq -- linha: " << __LINE__ << std::endl;
#endif

    int value2 = this->frameStack.top().operandStack.top();
    this->frameStack.top().operandStack.pop();
    int value1 = this->frameStack.top().operandStack.top();
    this->frameStack.top().operandStack.pop();

	if (value1 == value2) {
		short branchOffset = 0;
   		char* auxshort = ( char* ) &branchOffset;
   		auxshort[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+1];
   		auxshort[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+2];
        this->frameStack.top().cp += branchOffset;
    } else {
		this->frameStack.top().cp += 3;
    }

#if DEBUGMODE == 1
	std::cout << "	valores comparados(int1 = int2) - int1: " << value1 << " - int2: " << value2 << " - cp final: " << this->frameStack.top().cp <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_if_icmpne(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_if_icmpne -- linha: " << __LINE__ << std::endl;
#endif

    int value2 = this->frameStack.top().operandStack.top();
    this->frameStack.top().operandStack.pop();
    int value1 = this->frameStack.top().operandStack.top();
    this->frameStack.top().operandStack.pop();

	if (value1 != value2) {
		short branchOffset = 0;
   		char* auxshort = ( char* ) &branchOffset;
   		auxshort[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+1];
   		auxshort[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+2];
        this->frameStack.top().cp += branchOffset;
    } else {
		this->frameStack.top().cp += 3;
    }

#if DEBUGMODE == 1
	std::cout << "	valores comparados em int(int1 != int2) - int1: " << value1 << " - int2: " << value2 << " - cp final: " << this->frameStack.top().cp <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_if_icmplt(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_if_icmplt -- linha: " << __LINE__ << std::endl;
#endif

    int value2 = this->frameStack.top().operandStack.top();
    this->frameStack.top().operandStack.pop();
    int value1 = this->frameStack.top().operandStack.top();
    this->frameStack.top().operandStack.pop();

	if (value1 < value2) {
		short branchOffset = 0;
   		char* auxshort = ( char* ) &branchOffset;
   		auxshort[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+1];
   		auxshort[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+2];
        this->frameStack.top().cp += branchOffset;
    } else {
		this->frameStack.top().cp += 3;
    }

#if DEBUGMODE == 1
	std::cout << "	valores comparados em int(int1 < int2) - int1: " << value1 << " - int2: " << value2 << " - cp final: " << this->frameStack.top().cp <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_if_icmple(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_if_icmple -- linha: " << __LINE__ << std::endl;
#endif

    int value2 = this->frameStack.top().operandStack.top();
    this->frameStack.top().operandStack.pop();
    int value1 = this->frameStack.top().operandStack.top();
    this->frameStack.top().operandStack.pop();

	if (value1 <= value2) {
		short branchOffset = 0;
   		char* auxshort = ( char* ) &branchOffset;
   		auxshort[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+1];
   		auxshort[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+2];
        this->frameStack.top().cp += branchOffset;
    } else {
		this->frameStack.top().cp += 3;
    }

#if DEBUGMODE == 1
	std::cout << "	valores comparados em int(int1 <= int2) - int1: " << value1 << " - int2: " << value2 << " - cp final: " << this->frameStack.top().cp <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_if_icmpgt(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_if_icmpgt -- linha: " << __LINE__ << std::endl;
#endif

    int value2 = this->frameStack.top().operandStack.top();
    this->frameStack.top().operandStack.pop();
    int value1 = this->frameStack.top().operandStack.top();
    this->frameStack.top().operandStack.pop();

	if (value1 > value2) {
		short branchOffset = 0;
   		char* auxshort = ( char* ) &branchOffset;
   		auxshort[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+1];
   		auxshort[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+2];
        this->frameStack.top().cp += branchOffset;
    } else {
		this->frameStack.top().cp += 3;
    }

#if DEBUGMODE == 1
	std::cout << "	valores comparados em int(int1 > int2) - int1: " << value1 << " - int2: " << value2 << " - cp final: " << this->frameStack.top().cp <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_if_icmpge(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_if_icmpge -- linha: " << __LINE__ << std::endl;
#endif

    int value2 = this->frameStack.top().operandStack.top();
    this->frameStack.top().operandStack.pop();
    int value1 = this->frameStack.top().operandStack.top();
    this->frameStack.top().operandStack.pop();

	if (value1 >= value2) {
		short branchOffset = 0;
   		char* auxshort = ( char* ) &branchOffset;
   		auxshort[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+1];
   		auxshort[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+2];
        this->frameStack.top().cp += branchOffset;
    } else {
		this->frameStack.top().cp += 3;
    }

#if DEBUGMODE == 1
	std::cout << "	valores comparados em int(int1 >= int2) - int1: " << value1 << " - int2: " << value2 << " - cp final: " << this->frameStack.top().cp <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_ifeq(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_ifeq -- linha: " << __LINE__ << std::endl;
#endif

    int value = this->frameStack.top().operandStack.top();
    this->frameStack.top().operandStack.pop();

	if (value == 0) {
		short branchOffset = 0;
   		char* auxshort = ( char* ) &branchOffset;
   		auxshort[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+1];
   		auxshort[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+2];
        this->frameStack.top().cp += branchOffset;
    } else {
		this->frameStack.top().cp += 3;
    }

#if DEBUGMODE == 1
	std::cout << "	valor comparado em int: " << value << " - cp final: " << this->frameStack.top().cp <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_ifne(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_ifne -- linha: " << __LINE__ << std::endl;
#endif

    int value = this->frameStack.top().operandStack.top();
    this->frameStack.top().operandStack.pop();

	if (value != 0) {
		short branchOffset = 0;
   		char* auxshort = ( char* ) &branchOffset;
   		auxshort[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+1];
   		auxshort[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+2];
        this->frameStack.top().cp += branchOffset;
    } else {
		this->frameStack.top().cp += 3;
    }

#if DEBUGMODE == 1
	std::cout << "	valor comparado em int: " << value << " - cp final: " << this->frameStack.top().cp <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_iflt(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_iflt -- linha: " << __LINE__ << std::endl;
#endif

    int value = this->frameStack.top().operandStack.top();
    this->frameStack.top().operandStack.pop();

	if (value < 0) {
		short branchOffset = 0;
   		char* auxshort = ( char* ) &branchOffset;
   		auxshort[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+1];
   		auxshort[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+2];
        this->frameStack.top().cp += branchOffset;
    } else {
		this->frameStack.top().cp += 3;
    }

#if DEBUGMODE == 1
	std::cout << "	valor comparado em int: " << value << " - cp final: " << this->frameStack.top().cp <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_ifle(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_ifle -- linha: " << __LINE__ << std::endl;
#endif

    int value = this->frameStack.top().operandStack.top();
    this->frameStack.top().operandStack.pop();

	if (value <= 0) {
		short branchOffset = 0;
   		char* auxshort = ( char* ) &branchOffset;
   		auxshort[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+1];
   		auxshort[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+2];
        this->frameStack.top().cp += branchOffset;
    } else {
		this->frameStack.top().cp += 3;
    }

#if DEBUGMODE == 1
	std::cout << "	valor comparado em int: " << value << " - cp final: " << this->frameStack.top().cp <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_ifgt(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_ifgt -- linha: " << __LINE__ << std::endl;
#endif

    int value = this->frameStack.top().operandStack.top();
    this->frameStack.top().operandStack.pop();

	if (value > 0) {
		short branchOffset = 0;
   		char* auxshort = ( char* ) &branchOffset;
   		auxshort[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+1];
   		auxshort[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+2];
        this->frameStack.top().cp += branchOffset;
    } else {
		this->frameStack.top().cp += 3;
    }

#if DEBUGMODE == 1
	std::cout << "	valor comparado em int: " << value << " - cp final: " << this->frameStack.top().cp <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_ifge(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_ifge -- linha: " << __LINE__ << std::endl;
#endif

    int value = this->frameStack.top().operandStack.top();
    this->frameStack.top().operandStack.pop();

	if (value >= 0) {
		short branchOffset = 0;
   		char* auxshort = ( char* ) &branchOffset;
   		auxshort[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+1];
   		auxshort[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+2];
        this->frameStack.top().cp += branchOffset;
    } else {
		this->frameStack.top().cp += 3;
    }

#if DEBUGMODE == 1
	std::cout << "	valor comparado em int: " << value << " - cp final: " << this->frameStack.top().cp <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_ifnonnull(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_ifnonnull -- linha: " << __LINE__ << std::endl;
#endif

    int value = this->frameStack.top().operandStack.top();
    this->frameStack.top().operandStack.pop();

	if (value != 0) {
		short branchOffset = 0;
   		char* auxshort = ( char* ) &branchOffset;
   		auxshort[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+1];
   		auxshort[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+2];
        this->frameStack.top().cp += branchOffset;
    } else {
		this->frameStack.top().cp += 3;
    }

#if DEBUGMODE == 1
	std::cout << "	valor comparado em int: " << value << " - cp final: " << this->frameStack.top().cp <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_ifnull(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_ifnull -- linha: " << __LINE__ << std::endl;
#endif

    int value = this->frameStack.top().operandStack.top();
    this->frameStack.top().operandStack.pop();

	if (value == 0) {
		short branchOffset = 0;
   		char* auxshort = ( char* ) &branchOffset;
   		auxshort[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+1];
   		auxshort[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+2];
        this->frameStack.top().cp += branchOffset;
    } else {
		this->frameStack.top().cp += 3;
    }

#if DEBUGMODE == 1
	std::cout << "	valor comparado em int: " << value << " - cp final: " << this->frameStack.top().cp <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_iinc(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_iinc -- linha: " << __LINE__ << std::endl;
#endif
	unsigned short index;

	if(this->wideFlag){
		index = 0;
   		char* auxshort = ( char* ) &index;
   		auxshort[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+1];
   		auxshort[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+2];
   		this->wideFlag = false;
	}else{
		index = (unsigned short) this->frameStack.top().bytecode[this->frameStack.top().cp+1];
	}

	short auxconst;

	if(this->wideFlag){
		auxconst = 0;
   		char* auxshort = ( char* ) &index;
   		auxshort[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+3];
   		auxshort[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+4];
   		this->wideFlag = false;
	}else{
		auxconst = this->frameStack.top().bytecode[this->frameStack.top().cp+2];
	}

#if DEBUGMODE == 1
	int valueb = this->frameStack.top().arrayLocalVariable[index];
#endif
	this->frameStack.top().arrayLocalVariable[index] += auxconst;


	if(this->wideFlag){
		this->frameStack.top().cp+=5;
		this->wideFlag = false;
	}else{
		this->frameStack.top().cp+=3;
	}



#if DEBUGMODE == 1
	std::cout << "	index: " << (int) index << " - valor anterior: " << valueb  << " - valor a ser somado: " << auxconst << " - valor final: " << this->frameStack.top().arrayLocalVariable[(unsigned short) index]  <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_iload(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_iload -- completo -- linha: " << __LINE__  << std::endl;
#endif

	unsigned short index;

	if(this->wideFlag){
		index = 0;
   		char* auxshort = ( char* ) &index;
   		auxshort[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+1];
   		auxshort[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+2];
	}else{
		index = (unsigned short) this->frameStack.top().bytecode[this->frameStack.top().cp+1];
	}

	int value = this->frameStack.top().arrayLocalVariable[index];

	this->frameStack.top().operandStack.push(value);

	if(this->wideFlag){
		this->frameStack.top().cp +=3;
		this->wideFlag = false;
	}else{
		this->frameStack.top().cp +=2;
	}

#if DEBUGMODE == 1
	std::cout << "	Index da local variable: " << (unsigned short) index <<  " -  int carregado da variavel (topo do operandStack)"  << this->frameStack.top().operandStack.top() << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_iload_0(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_iload_0 -- completo -- linha: " << __LINE__  << std::endl;
#endif
	this->frameStack.top().operandStack.push(this->frameStack.top().arrayLocalVariable[0]);
	this->frameStack.top().cp ++;
#if DEBUGMODE == 1
	std::cout << "	int carregado da variavel (topo do operandStack): "  << this->frameStack.top().operandStack.top() << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_iload_1(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_iload_1 -- completo -- linha: " << __LINE__  << std::endl;
#endif
	this->frameStack.top().operandStack.push(this->frameStack.top().arrayLocalVariable[1]);
	this->frameStack.top().cp ++;
#if DEBUGMODE == 1
	std::cout << "	int carregado da variavel (topo do operandStack): "  << this->frameStack.top().operandStack.top() << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_iload_2(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_iload_2 -- completo -- linha: " << __LINE__  << std::endl;
#endif
	this->frameStack.top().operandStack.push(this->frameStack.top().arrayLocalVariable[2]);
	this->frameStack.top().cp ++;
#if DEBUGMODE == 1
	std::cout << "	int carregado da variavel (topo do operandStack): "  << this->frameStack.top().operandStack.top() << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_iload_3(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_iload_3 -- completo -- linha: " << __LINE__  << std::endl;
#endif
	this->frameStack.top().operandStack.push(this->frameStack.top().arrayLocalVariable[3]);
	this->frameStack.top().cp ++;
#if DEBUGMODE == 1
	std::cout << "	int carregado da variavel (topo do operandStack): "  << this->frameStack.top().operandStack.top() << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_imul(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_imul -- linha: " << __LINE__ << std::endl;
#endif

	int value1, value2;

	value2 = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	value1 = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	int result;

	result =  value1 * value2;

	this->frameStack.top().operandStack.push(result);

	this->frameStack.top().cp++;

#if DEBUGMODE == 1
	std::cout << "	ints utilizados(int1 * int2) - int1: " << value1 << " - int2: " << value2 << " - resultado empilhado: " << result <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_ineg(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_ineg -- linha: " << __LINE__ << std::endl;
#endif

	int value;

	value = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	int result;

	result =  -value;

	this->frameStack.top().operandStack.push(result);

	this->frameStack.top().cp++;

#if DEBUGMODE == 1
	std::cout << "	int utilizado: " << value << " - resultado empilhado: " << result <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_ior(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_ior -- linha: " << __LINE__ << std::endl;
#endif

	int value1, value2;

	value2 = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	value1 = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	int result;

	result =  value1 | value2;

	this->frameStack.top().operandStack.push(result);

	this->frameStack.top().cp++;

#if DEBUGMODE == 1
	std::cout << "	ints utilizados(int1 | int2) - int1: " << value1 << " - int2: " << value2 << " - resultado empilhado: " << result <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_irem(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_irem -- linha: " << __LINE__ << std::endl;
#endif

	int value1, value2;

	value2 = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	value1 = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	if(value2 == 0){
		std::cout << "	ArithmeticException " << std::endl;
	}

	int result;

	result =  value1 % value2;

	this->frameStack.top().operandStack.push(result);

	this->frameStack.top().cp++;

#if DEBUGMODE == 1
	std::cout << "	ints utilizados(int1 % int2) - int1: " << value1 << " - int2: " << value2 << " - resultado empilhado: " << result <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_ireturn(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_ireturn -- completo -- linha: " << __LINE__   << std::endl;
#endif
    int retorno = this->frameStack.top().operandStack.top();

    this->frameStack.pop();

    this->frameStack.top().operandStack.push(retorno);

#if DEBUGMODE == 1
	int aux;
	copyBitsetDword((char*) &aux, (char*) &this->frameStack.top().operandStack.top());
   std::cout << "	int retornado: " << aux  << std::endl;
	std::cout << " ++++++++++++++++++++++++++++ " << this->frameStack.size()+1 << " ++++++++++++++++++++++++++++	" << std::endl;
#endif
}

void RuntimeEngine::i_ishl(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_ishl -- linha: " << __LINE__ << std::endl;
#endif

	int value1, value2;

	value2 = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	value1 = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	value2 = value2 & 0x1F;

	int result;

	result =  value1 << value2;

	this->frameStack.top().operandStack.push(result);

	this->frameStack.top().cp++;

#if DEBUGMODE == 1
	std::cout << "	ints utilizados(int1 << int2) - int1: " << value1 << " - int2: " << value2 << " - resultado empilhado: " << result <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_ishr(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_ishr -- linha: " << __LINE__ << std::endl;
#endif

	int value1, value2;

	value2 = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	value1 = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	value2 = value2 & 0x1F;

	int result;

	result =  value1 >> value2;

	this->frameStack.top().operandStack.push(result);

	this->frameStack.top().cp++;

#if DEBUGMODE == 1
	std::cout << "	ints utilizados(int1 >> int2) - int1: " << value1 << " - int2: " << value2 << " - resultado empilhado: " << result <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_istore(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_istore -- linha: " << __LINE__   << std::endl;
#endif

	unsigned short index;

	if(this->wideFlag){
		index = 0;
   		char* auxshort = ( char* ) &index;
   		auxshort[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+1];
   		auxshort[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+2];
	}else{
		index = (unsigned short) this->frameStack.top().bytecode[this->frameStack.top().cp+1];
	}

	int value = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	this->frameStack.top().arrayLocalVariable[index]= value;

	if(this->wideFlag){
		this->frameStack.top().cp +=3;
		this->wideFlag = false;
	}else{
		this->frameStack.top().cp +=2;
	}

#if DEBUGMODE == 1
	std::cout << "	Index da local variable: " << (unsigned short) index <<  " -  int carregado na variavel: "  << this->frameStack.top().arrayLocalVariable[index] << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_istore_0(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_istore_0 -- linha: " << __LINE__   << std::endl;
#endif

	int value = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	this->frameStack.top().arrayLocalVariable[0]= value;

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	int carregado na variavel: "  << this->frameStack.top().arrayLocalVariable[0] << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_istore_1(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_istore_1 -- linha: " << __LINE__   << std::endl;
#endif

	int value = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	this->frameStack.top().arrayLocalVariable[1]= value;

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	int carregado na variavel: "  << this->frameStack.top().arrayLocalVariable[1] << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_istore_2(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_istore_2 -- linha: " << __LINE__   << std::endl;
#endif

	int value = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	this->frameStack.top().arrayLocalVariable[2]= value;

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	int carregado na variavel: "  << this->frameStack.top().arrayLocalVariable[2] << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_istore_3(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_istore_3 -- linha: " << __LINE__   << std::endl;
#endif

	int value = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	this->frameStack.top().arrayLocalVariable[3]= value;

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	int carregado na variavel: "  << this->frameStack.top().arrayLocalVariable[3] << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_isub(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_isub -- linha: " << __LINE__ << std::endl;
#endif

	int value1, value2;

	value2 = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	value1 = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	int result;

	result =  value1 - value2;

	this->frameStack.top().operandStack.push(result);

	this->frameStack.top().cp++;

#if DEBUGMODE == 1
	std::cout << "	ints utilizados(int1 - int2) - int1: " << value1 << " - int2: " << value2 << " - resultado empilhado: " << result <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_iushr(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_iushr -- linha: " << __LINE__ << std::endl;
#endif

	unsigned int value1, value2;

	value2 = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	value1 = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	value2 = value2 & 0x1F;

	unsigned int result;

	result =  value1 >> value2;

	this->frameStack.top().operandStack.push(result);

	this->frameStack.top().cp++;

#if DEBUGMODE == 1
	std::cout << "	ints utilizados(int1 >> int2) - int1: " << value1 << " - int2: " << value2 << " - resultado empilhado: " << result <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_ixor(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_ixor -- linha: " << __LINE__ << std::endl;
#endif

	int value1, value2;

	value2 = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	value1 = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	int result;

	result =  value1 ^ value2;

	this->frameStack.top().operandStack.push(result);

	this->frameStack.top().cp++;

#if DEBUGMODE == 1
	std::cout << "	ints utilizados(int1 ^ int2) - int1: " << value1 << " - int2: " << value2 << " - resultado empilhado: " << result <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_jsr(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_jsr -- linha: " << __LINE__ << std::endl;
#endif

	short branchOffset = 0;
		char* auxshort = ( char* ) &branchOffset;
		auxshort[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+1];
		auxshort[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+2];
    this->frameStack.top().cp += branchOffset;

	int address;
	address = this->frameStack.top().cp + 3;
	this->frameStack.top().operandStack.push(address);

    this->frameStack.top().cp += branchOffset;

#if DEBUGMODE == 1
	std::cout << "	BranchOffset: " << branchOffset << " - address empilhado(topo do operand stack): " << this->frameStack.top().operandStack.top() <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_jsr_w(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_jsr_w -- linha: " << __LINE__ << std::endl;
#endif

	int branchOffset = 0;
		char* auxshort = ( char* ) &branchOffset;
		auxshort[3] = this->frameStack.top().bytecode[this->frameStack.top().cp+1];
		auxshort[2] = this->frameStack.top().bytecode[this->frameStack.top().cp+2];
		auxshort[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+3];
		auxshort[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+4];
	this->frameStack.top().cp += branchOffset;

	int address;
	address = this->frameStack.top().cp + 5;
	this->frameStack.top().operandStack.push(address);

	this->frameStack.top().cp += branchOffset;

#if DEBUGMODE == 1
	std::cout << "	BranchOffset: " << branchOffset << " - address empilhado(topo do operand stack): " << this->frameStack.top().operandStack.top() <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_l2d(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_l2d -- linha: " << __LINE__ << std::endl;
#endif

	int upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	int lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	long long value;

	uniteDWords((char*)&value, (char*)&upperAux, (char*)&lowerAux);

	double result;

	result = (double) value;

	splitQWord((char*)&result, (char*)&upperAux, (char*)&lowerAux);

	this->frameStack.top().operandStack.push(lowerAux);
	this->frameStack.top().operandStack.push(upperAux);

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	valor a ser convertido(long): " << value << " - valor empilhado(double): " << result <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_l2f(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_l2f -- linha: " << __LINE__ << std::endl;
#endif

	int upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	int lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	long long value;

	uniteDWords((char*)&value, (char*)&upperAux, (char*)&lowerAux);

	float result;

	result = (float) value;

	int aux;

	copyBitsetDword((char*) &aux, (char*) &result);

	this->frameStack.top().operandStack.push(aux);

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	valor a ser convertido(long): " << value << " - valor empilhado(float): " << result <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_l2i(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_l2i -- linha: " << __LINE__ << std::endl;
#endif

	int upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	int lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	long long value;

	uniteDWords((char*)&value, (char*)&upperAux, (char*)&lowerAux);

	int result;

	result = (int) value;

	this->frameStack.top().operandStack.push(result);

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	valor a ser convertido(long): " << value << " - valor empilhado(int): " << result <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_ladd(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_ladd -- linha: " << __LINE__ << std::endl;
#endif

	int upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	int lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	long long value2;

	uniteDWords((char*)&value2, (char*)&upperAux, (char*)&lowerAux);

	upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	long long value1;

	uniteDWords((char*)&value1, (char*)&upperAux, (char*)&lowerAux);

	long long result = value1 + value2;

	splitQWord((char*)&result, (char*)&upperAux, (char*)&lowerAux);

	this->frameStack.top().operandStack.push(lowerAux);
	this->frameStack.top().operandStack.push(upperAux);

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	longs utilizados(long1 + long2) - long1: " << value1 << " - long2: " << value2 << " - resultado empilhado: " << result <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_laload(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_laload -- linha: " << __LINE__ << std::endl;
#endif

	unsigned int index = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	if(this->frameStack.top().operandStack.top() == 0){
		std::cout << " ------NullPointerException------ " << std::endl;
	}

	std::vector<int>* arrayref = (std::vector<int>*) this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	/*
	 * o long � colocado em 2 ints no array, na posi��o index*2 � o upperHalf e index*2+1 � o lowerHalf
	 */

	this->frameStack.top().operandStack.push((*arrayref)[(index*2)+2]);
	this->frameStack.top().operandStack.push((*arrayref)[(index*2)+1]);

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	long long value;
	int upperAux = (*arrayref)[index*2];
	int lowerAux = (*arrayref)[index*2+1];
	uniteDWords((char*)&value, (char*)&upperAux, (char*)&lowerAux);
	std::cout << "	Index no array: " << index << " -- valor long dado push no operand stack(valor carregado): " << value << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_land(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_land -- linha: " << __LINE__ << std::endl;
#endif

	int upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	int lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	long long value2;

	uniteDWords((char*)&value2, (char*)&upperAux, (char*)&lowerAux);

	upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	long long value1;

	uniteDWords((char*)&value1, (char*)&upperAux, (char*)&lowerAux);

	long long result = value1 & value2;

	splitQWord((char*)&result, (char*)&upperAux, (char*)&lowerAux);

	this->frameStack.top().operandStack.push(lowerAux);
	this->frameStack.top().operandStack.push(upperAux);

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	longs utilizados(long1 & long2) - long1: " << value1 << " - long2: " << value2 << " - resultado empilhado: " << result <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_lastore(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_lastore -- linha: " << __LINE__ << std::endl;
#endif

	int upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	int lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	unsigned int index = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	if(this->frameStack.top().operandStack.top() == 0){
		std::cout << " ------NullPointerException------ " << std::endl;
	}

	std::vector<int>* arrayref = (std::vector<int>*) this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	/*
	 * o double � colocado em 2 ints no array, na posi��o index*2 � o upperHalf e index*2+1 � o lowerHalf
	 */

	(*arrayref)[(index*2)+1] = upperAux;

	(*arrayref)[(index*2)+2] = lowerAux;

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	long long value;
	uniteDWords((char*)&value, (char*)&upperAux, (char*)&lowerAux);
	std::cout << "	Index no array: " << index << " -- valor long dado push no operand stack(valor carregado): " << value << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_lcmp(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_lcmp -- linha: " << __LINE__ << std::endl;
#endif

	int upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	int lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	long long value2;

	uniteDWords((char*)&value2, (char*)&upperAux, (char*)&lowerAux);

	upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	long long value1;

	uniteDWords((char*)&value1, (char*)&upperAux, (char*)&lowerAux);

	if(value1 > value2){
		this->frameStack.top().operandStack.push(1);
	}else if(value1 == value2){
		this->frameStack.top().operandStack.push(0);
	}else if(value1 < value2){
		this->frameStack.top().operandStack.push(-1);
	}

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	longs comparados - long1: " << value1 << " - long2: " << value2 << " - resultado empilhado(topo do operand stack): " << this->frameStack.top().operandStack.top() <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_lconst_0(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_lconst_0 -- linha: " << __LINE__ << std::endl;
#endif

	long long value = 0;
	int upperAux, lowerAux;

	splitQWord((char*)&value, (char*)&upperAux, (char*)&lowerAux);

	this->frameStack.top().operandStack.push(lowerAux);
	this->frameStack.top().operandStack.push(upperAux);

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	valor empilhado(topo do operand stack): " << value <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_lconst_1(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_lconst_1 -- linha: " << __LINE__ << std::endl;
#endif

	long long value = 1;
	int upperAux, lowerAux;

	splitQWord((char*)&value, (char*)&upperAux, (char*)&lowerAux);

	this->frameStack.top().operandStack.push(lowerAux);
	this->frameStack.top().operandStack.push(upperAux);

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	valor empilhado(topo do operand stack): " << value <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_ldiv(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_ldiv -- linha: " << __LINE__ << std::endl;
#endif

	int upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	int lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	long long value2;

	uniteDWords((char*)&value2, (char*)&upperAux, (char*)&lowerAux);

	upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	long long value1;

	uniteDWords((char*)&value1, (char*)&upperAux, (char*)&lowerAux);

	if(value2 == 0){
		std::cout << "	ArithmeticException " << std::endl;
	}

	long long result = value1 / value2;

	splitQWord((char*)&result, (char*)&upperAux, (char*)&lowerAux);

	this->frameStack.top().operandStack.push(lowerAux);
	this->frameStack.top().operandStack.push(upperAux);

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	longs utilizados(long1 / long2) - long1: " << value1 << " - long2: " << value2 << " - resultado empilhado: " << result <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_lload(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_lload -- completo -- linha: " << __LINE__  << std::endl;
#endif
	unsigned short index;

	if(this->wideFlag){
		index = 0;
   		char* auxshort = ( char* ) &index;
   		auxshort[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+1];
   		auxshort[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+2];
	}else{
		index = (unsigned short) this->frameStack.top().bytecode[this->frameStack.top().cp+1];
	}

	int upperAux = this->frameStack.top().arrayLocalVariable[(unsigned short) index];
	int lowerAux = this->frameStack.top().arrayLocalVariable[(unsigned short) index+1];
	this->frameStack.top().operandStack.push(lowerAux);
	this->frameStack.top().operandStack.push(upperAux);
	if(this->wideFlag){
		this->frameStack.top().cp +=3;
		this->wideFlag = false;
	}else{
		this->frameStack.top().cp +=2;
	}
#if DEBUGMODE == 1
	long long value;
	uniteDWords((char*)&value, (char*)&upperAux, (char*)&lowerAux);
	std::cout << "	Index da local variable: " << (unsigned short) index <<  " -  long carregada da variavel (topo do operandStack)"  << value << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_lload_0(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_lload_0 -- completo -- linha: " << __LINE__  << std::endl;
#endif
	int upperAux = this->frameStack.top().arrayLocalVariable[(unsigned short) 0];
	int lowerAux = this->frameStack.top().arrayLocalVariable[(unsigned short) 1];
	this->frameStack.top().operandStack.push(lowerAux);
	this->frameStack.top().operandStack.push(upperAux);
	this->frameStack.top().cp ++;
#if DEBUGMODE == 1
	long long value;
	uniteDWords((char*)&value, (char*)&upperAux, (char*)&lowerAux);
	std::cout << "	Index da local variable: " << 0 <<  " -  long carregada da variavel (topo do operandStack)"  << value << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_lload_1(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_lload_1 -- completo -- linha: " << __LINE__  << std::endl;
#endif
	int upperAux = this->frameStack.top().arrayLocalVariable[(unsigned short) 1];
	int lowerAux = this->frameStack.top().arrayLocalVariable[(unsigned short) 2];
	this->frameStack.top().operandStack.push(lowerAux);
	this->frameStack.top().operandStack.push(upperAux);
	this->frameStack.top().cp ++;
#if DEBUGMODE == 1
	long long value;
	uniteDWords((char*)&value, (char*)&upperAux, (char*)&lowerAux);
	std::cout << "	Index da local variable: " << 1 <<  " -  long carregada da variavel (topo do operandStack)"  << value << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_lload_2(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_lload_2 -- completo -- linha: " << __LINE__  << std::endl;
#endif
	int upperAux = this->frameStack.top().arrayLocalVariable[(unsigned short) 2];
	int lowerAux = this->frameStack.top().arrayLocalVariable[(unsigned short) 3];
	this->frameStack.top().operandStack.push(lowerAux);
	this->frameStack.top().operandStack.push(upperAux);
	this->frameStack.top().cp ++;
#if DEBUGMODE == 1
	long long value;
	uniteDWords((char*)&value, (char*)&upperAux, (char*)&lowerAux);
	std::cout << "	Index da local variable: " << 2 <<  " -  long carregada da variavel (topo do operandStack)"  << value << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_lload_3(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_lload_3 -- completo -- linha: " << __LINE__  << std::endl;
#endif
	int upperAux = this->frameStack.top().arrayLocalVariable[(unsigned short) 3];
	int lowerAux = this->frameStack.top().arrayLocalVariable[(unsigned short) 4];
	this->frameStack.top().operandStack.push(lowerAux);
	this->frameStack.top().operandStack.push(upperAux);
	this->frameStack.top().cp ++;
#if DEBUGMODE == 1
	long long value;
	uniteDWords((char*)&value, (char*)&upperAux, (char*)&lowerAux);
	std::cout << "	Index da local variable: " << 3 <<  " -  long carregada da variavel (topo do operandStack)"  << value << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_lmul(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_lmul -- linha: " << __LINE__ << std::endl;
#endif

	int upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	int lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	long long value2;

	uniteDWords((char*)&value2, (char*)&upperAux, (char*)&lowerAux);

	upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	long long value1;

	uniteDWords((char*)&value1, (char*)&upperAux, (char*)&lowerAux);

	long long result = value1 * value2;

	splitQWord((char*)&result, (char*)&upperAux, (char*)&lowerAux);

	this->frameStack.top().operandStack.push(lowerAux);
	this->frameStack.top().operandStack.push(upperAux);

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	longs utilizados(long1 * long2) - long1: " << value1 << " - long2: " << value2 << " - resultado empilhado: " << result <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_lneg(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_lneg -- linha: " << __LINE__ << std::endl;
#endif

	int upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	int lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	long long value;

	uniteDWords((char*)&value, (char*)&upperAux, (char*)&lowerAux);

	long long result = -value;

	splitQWord((char*)&result, (char*)&upperAux, (char*)&lowerAux);

	this->frameStack.top().operandStack.push(lowerAux);
	this->frameStack.top().operandStack.push(upperAux);

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	long utilizado: " << value  << " - resultado empilhado: " << result <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_lor(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_lor -- linha: " << __LINE__ << std::endl;
#endif

	int upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	int lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	long long value2;

	uniteDWords((char*)&value2, (char*)&upperAux, (char*)&lowerAux);

	upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	long long value1;

	uniteDWords((char*)&value1, (char*)&upperAux, (char*)&lowerAux);

	long long result = value1 | value2;

	splitQWord((char*)&result, (char*)&upperAux, (char*)&lowerAux);

	this->frameStack.top().operandStack.push(lowerAux);
	this->frameStack.top().operandStack.push(upperAux);

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	longs utilizados(long1 | long2) - long1: " << value1 << " - long2: " << value2 << " - resultado empilhado: " << result <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_lrem(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_lrem -- linha: " << __LINE__ << std::endl;
#endif

	int upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	int lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	long long value2;

	uniteDWords((char*)&value2, (char*)&upperAux, (char*)&lowerAux);

	upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	long long value1;

	uniteDWords((char*)&value1, (char*)&upperAux, (char*)&lowerAux);

	if(value2 == 0){
		std::cout << "	ArithmeticException " << std::endl;
	}

	long long result = value1 % value2;

	splitQWord((char*)&result, (char*)&upperAux, (char*)&lowerAux);

	this->frameStack.top().operandStack.push(lowerAux);
	this->frameStack.top().operandStack.push(upperAux);

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	longs utilizados(long1 % long2) - long1: " << value1 << " - long2: " << value2 << " - resultado empilhado: " << result <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_lreturn(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_lreturn -- completo -- linha: " << __LINE__   << std::endl;
#endif
	int upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();
	int lowerAux = this->frameStack.top().operandStack.top();

    this->frameStack.pop();

    this->frameStack.top().operandStack.push(lowerAux);
	this->frameStack.top().operandStack.push(upperAux);
#if DEBUGMODE == 1
	long long value;
	uniteDWords((char*)&value, (char*)&upperAux, (char*)&lowerAux);
   std::cout << "	long retornado: " << value  << std::endl;
	std::cout << " ++++++++++++++++++++++++++++ " << this->frameStack.size()+1 << " ++++++++++++++++++++++++++++	" << std::endl;
#endif
}

void RuntimeEngine::i_lshl(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_lshl -- linha: " << __LINE__ << std::endl;
#endif

	int value2 = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	int upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	int lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	long long value1;

	uniteDWords((char*)&value1, (char*)&upperAux, (char*)&lowerAux);

	value2 &= 0x3f;

	long long result = value1 << value2;

	splitQWord((char*)&result, (char*)&upperAux, (char*)&lowerAux);

	this->frameStack.top().operandStack.push(lowerAux);
	this->frameStack.top().operandStack.push(upperAux);

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	longs utilizados(long << int) - long: " << value1 << " - int: " << value2 << " - resultado empilhado: " << result <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_lshr(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_lshr -- linha: " << __LINE__ << std::endl;
#endif

	int value2 = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	int upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	int lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();


	long long value1;

	uniteDWords((char*)&value1, (char*)&upperAux, (char*)&lowerAux);

	value2 &= 0x3f;

	long long result = value1 >> value2;

	splitQWord((char*)&result, (char*)&upperAux, (char*)&lowerAux);

	this->frameStack.top().operandStack.push(lowerAux);
	this->frameStack.top().operandStack.push(upperAux);

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	longs utilizados(long >> int) - long: " << value1 << " - int: " << value2 << " - resultado empilhado: " << result <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_lstore(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_lstore -- linha: " << __LINE__   << std::endl;
#endif

	unsigned short index;

	if(this->wideFlag){
		index = 0;
   		char* auxshort = ( char* ) &index;
   		auxshort[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+1];
   		auxshort[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+2];
	}else{
		index = (unsigned short) this->frameStack.top().bytecode[this->frameStack.top().cp+1];
	}

	int upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();
	int lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	this->frameStack.top().arrayLocalVariable[index]= upperAux;

	this->frameStack.top().arrayLocalVariable[index+1]= lowerAux;

	if(this->wideFlag){
		this->frameStack.top().cp +=3;
		this->wideFlag = false;
	}else{
		this->frameStack.top().cp +=2;
	}

#if DEBUGMODE == 1
	long long value;
	uniteDWords((char*)&value, (char*)&this->frameStack.top().arrayLocalVariable[index], (char*)&this->frameStack.top().arrayLocalVariable[index+1]);
	std::cout << "	index da variavel: " << (unsigned short) index  << " - long carregado nas variaveis index e index+1: "  << value << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_lstore_0(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_lstore_0 -- linha: " << __LINE__   << std::endl;
#endif

	int upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();
	int lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	this->frameStack.top().arrayLocalVariable[0]= upperAux;

	this->frameStack.top().arrayLocalVariable[1]= lowerAux;

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	long long value;
	uniteDWords((char*)&value, (char*)&this->frameStack.top().arrayLocalVariable[0], (char*)&this->frameStack.top().arrayLocalVariable[1]);
	std::cout << "	index da variavel: " << (unsigned short) 0  << " - long carregado nas variaveis index e index+1: "  << value << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_lstore_1(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_lstore_1 -- linha: " << __LINE__   << std::endl;
#endif

	int upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();
	int lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	this->frameStack.top().arrayLocalVariable[1]= upperAux;

	this->frameStack.top().arrayLocalVariable[2]= lowerAux;

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	long long value;
	uniteDWords((char*)&value, (char*)&this->frameStack.top().arrayLocalVariable[0], (char*)&this->frameStack.top().arrayLocalVariable[1]);
	std::cout << "	index da variavel: " << (unsigned short) 1  << " - long carregado nas variaveis index e index+1: "  << value << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_lstore_2(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_lstore_2 -- linha: " << __LINE__   << std::endl;
#endif

	int upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();
	int lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	this->frameStack.top().arrayLocalVariable[2]= upperAux;

	this->frameStack.top().arrayLocalVariable[3]= lowerAux;

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	long long value;
	uniteDWords((char*)&value, (char*)&this->frameStack.top().arrayLocalVariable[0], (char*)&this->frameStack.top().arrayLocalVariable[1]);
	std::cout << "	index da variavel: " << (unsigned short) 2  << " - long carregado nas variaveis index e index+1: "  << value << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_lstore_3(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_lstore_3 -- linha: " << __LINE__   << std::endl;
#endif

	int upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();
	int lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	this->frameStack.top().arrayLocalVariable[3]= upperAux;

	this->frameStack.top().arrayLocalVariable[4]= lowerAux;

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	long long value;
	uniteDWords((char*)&value, (char*)&this->frameStack.top().arrayLocalVariable[0], (char*)&this->frameStack.top().arrayLocalVariable[1]);
	std::cout << "	index da variavel: " << (unsigned short) 3  << " - long carregado nas variaveis index e index+1: "  << value << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_lsub(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_lsub -- linha: " << __LINE__ << std::endl;
#endif

	int upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	int lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	long long value2;

	uniteDWords((char*)&value2, (char*)&upperAux, (char*)&lowerAux);

	upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	long long value1;

	uniteDWords((char*)&value1, (char*)&upperAux, (char*)&lowerAux);

	long long result = value1 - value2;

	splitQWord((char*)&result, (char*)&upperAux, (char*)&lowerAux);

	this->frameStack.top().operandStack.push(lowerAux);
	this->frameStack.top().operandStack.push(upperAux);

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	longs utilizados(long1 - long2) - long1: " << value1 << " - long2: " << value2 << " - resultado empilhado: " << result <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_lushr(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_lushr -- linha: " << __LINE__ << std::endl;
#endif

	int value2 = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	int upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	int lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	unsigned long long value1;

	uniteDWords((char*)&value1, (char*)&upperAux, (char*)&lowerAux);

	value2 &= 0x3f;

	long long result = value1 >> value2;

	splitQWord((char*)&result, (char*)&upperAux, (char*)&lowerAux);

	this->frameStack.top().operandStack.push(lowerAux);
	this->frameStack.top().operandStack.push(upperAux);

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	longs utilizados(long >> int) - long: " << value1 << " - int: " << value2 << " - resultado empilhado: " << result <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_lxor(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_lxor -- linha: " << __LINE__ << std::endl;
#endif

	int upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	int lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	long long value2;

	uniteDWords((char*)&value2, (char*)&upperAux, (char*)&lowerAux);

	upperAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	lowerAux = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	long long value1;

	uniteDWords((char*)&value1, (char*)&upperAux, (char*)&lowerAux);

	long long result = value1 ^ value2;

	splitQWord((char*)&result, (char*)&upperAux, (char*)&lowerAux);

	this->frameStack.top().operandStack.push(lowerAux);
	this->frameStack.top().operandStack.push(upperAux);

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	longs utilizados(long1 ^ long2) - long1: " << value1 << " - long2: " << value2 << " - resultado empilhado: " << result <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_nop(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_nop -- completo -- linha: " << __LINE__ << std::endl;
#endif

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	opera��o n�o efetua nada, core do processador aproveita o tempo para questinar sua exist�ncia e seu papel no mundo " <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_pop(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_pop -- completo -- linha: " << __LINE__ << std::endl;
#endif

	this->frameStack.top().operandStack.pop();

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	retirado 1 elemento do topo do operand stack" <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_pop2(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_pop2 -- completo -- linha: " << __LINE__ << std::endl;
#endif

	this->frameStack.top().operandStack.pop();
	this->frameStack.top().operandStack.pop();

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	retirado 2 elementos do topo do operand stack" <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_ret(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_ret -- completo -- linha: " << __LINE__ << std::endl;
#endif

	unsigned short index;

	if(this->wideFlag){
		index = 0;
   		char* auxshort = ( char* ) &index;
   		auxshort[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+1];
   		auxshort[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+2];
   		this->wideFlag = false;
	}else{
		index = (unsigned short) this->frameStack.top().bytecode[this->frameStack.top().cp+1];
	}

	int address = this->frameStack.top().arrayLocalVariable[(unsigned short) index];

	this->frameStack.top().cp = address;

#if DEBUGMODE == 1
	std::cout << "	" <<  std::endl << std::endl;
#endif
}

void RuntimeEngine::i_return(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_return -- completo -- linha: " << __LINE__   << std::endl;
#endif

    this->frameStack.top().cp = -1;

#if DEBUGMODE == 1
   std::cout << "	controle retornado "  << std::endl;
	std::cout << " ++++++++++++++++++++++++++++ " << this->frameStack.size()<< " ++++++++++++++++++++++++++++	" << std::endl;
#endif
}

void RuntimeEngine::i_saload(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_saload -- completo -- linha: " << __LINE__ << std::endl;
#endif

	unsigned int index = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	if(this->frameStack.top().operandStack.top() == 0){
		std::cout << " ------NullPointerException------ " << std::endl;
	}

	std::vector<int>* arrayref = (std::vector<int>*) this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	this->frameStack.top().operandStack.push((*arrayref)[index+1]);

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	short value;
	value = (short) this->frameStack.top().operandStack.top();
	std::cout << "	Index no array: " << index << " -- Topo do operand stack(short carregado): " << value << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_sastore(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_sastore -- completo -- linha: " << __LINE__ << std::endl;
#endif

	int value = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	value <<= 16;
	value >>= 16;

	unsigned int index = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	if(this->frameStack.top().operandStack.top() == 0){
		std::cout << " ------NullPointerException------ " << std::endl;
	}

	std::vector<int>* arrayref = (std::vector<int>*) this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	(*arrayref)[index+1] = value;

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	int valuef;
	copyBitsetDword((char*) &valuef, (char*) &(*arrayref)[index+1]);
	std::cout << "	Index no array: " << index << " -- Antigo topo do operand stack(short carregado): " << valuef << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_sipush(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_sipush -- completo -- linha: " << __LINE__ << std::endl;
#endif

	short valor = 0;
    char* auxshort = ( char* ) &valor;
    auxshort[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+1];
    auxshort[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+2];
    this->frameStack.top().operandStack.push((int)valor);

	this->frameStack.top().cp +=3;

#if DEBUGMODE == 1
	std::cout << "	short pushed: " << valor << std::endl<< std::endl;
#endif
}

void RuntimeEngine::i_swap(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_swap -- linha: " << __LINE__ << std::endl;
#endif

	int value1 = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	int value2 = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	this->frameStack.top().operandStack.push(value2);
	this->frameStack.top().operandStack.push(value1);

	this->frameStack.top().cp ++;

#if DEBUGMODE == 1
	std::cout << "	ultimos 2 valores do operando stack (da base para o top): " <<  value1 << " - " << this->frameStack.top().operandStack.top() << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_newarray(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_newarray -- linha: " << __LINE__ << std::endl;
#endif

	unsigned char atype = this->frameStack.top().bytecode[this->frameStack.top().cp+1];
	int count = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	std::vector<int>* aux = new std::vector<int>;

	if((atype == 4) || (atype == 8) || (atype == 5) || (atype == 9) || (atype == 6) || (atype == 10)){
		aux->resize(count+1);
		aux->data()[0] = 1;
		this->frameStack.top().operandStack.push((int)aux);
	}else if((atype == 7) || (atype == 11)){
		aux->resize((count*2)+1);
		aux->data()[0] = 2;
		this->frameStack.top().operandStack.push((int)aux);
	}

	this->frameStack.top().cp+=2;

#if DEBUGMODE == 1
	std::cout << "	criado array de tipo(code): " << (int) atype << " - com numero de elementos: " << count << std::endl << std::endl;
#endif

}

void RuntimeEngine::i_invokestatic(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_invokestatic -- completo" << std::endl;
#endif

	unsigned short index = 0;
	char* auxshort = ( char* ) &index;
	auxshort[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+1];
	auxshort[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+2];
	MethodRefConstantPool* methodRef = static_cast<MethodRefConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) index-1));
	ClassRefConstantPool* classRef = static_cast<ClassRefConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) methodRef->classRefIndex-1));
	StringConstantPool* className = static_cast<StringConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) classRef->nameIndex-1));
	NameTypeConstantPool* nameType = static_cast<NameTypeConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) methodRef->nameTypeIndex-1));
	StringConstantPool* methodName = static_cast<StringConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) nameType->nameIndex-1));
	StringConstantPool* methodType = static_cast<StringConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) nameType->type-1));

	for(unsigned int i=0; i <= methodArea->classArray.size(); i++){
//		std::cout << static_cast<StringConstantPool*>(methodArea->classArray[i]->constantPoolArray[static_cast<ClassRefConstantPool*>(methodArea->classArray[i]->constantPoolArray[methodArea->classArray[i]->thisClassIndex-1])->nameIndex-1])->str;
//		std::cout << methodType->str;
		if(i == methodArea->classArray.size()){
			methodArea->classArray.push_back(new ClassReader);
			methodArea->classArray.back()->loadClassFileInfo(className->str + ".class");
		}
		if(static_cast<StringConstantPool*>(methodArea->classArray[i]->constantPoolArray[static_cast<ClassRefConstantPool*>(methodArea->classArray[i]->constantPoolArray[methodArea->classArray[i]->thisClassIndex-1])->nameIndex-1])->str == className->str){
			this->frameStack.top().cp +=3;
			int k = 0, numParametros = 0;
			std::stack<int> parametroArray;
			std::stack<char> parametroOrder;
			while (methodType->str[k] != ')'){
				if((methodType->str[k] == 'I') || (methodType->str[k] == 'B') || (methodType->str[k] == 'C') || (methodType->str[k] == 'F') || (methodType->str[k] == 'S') || (methodType->str[k] == 'Z') || (methodType->str[k] == 'L') || (methodType->str[k] == '[')){
					parametroOrder.push('a');
					numParametros++;

					if(methodType->str[k] == 'L'){
						parametroOrder.push('a');
						numParametros++;
						while(methodType->str[k] != ';'){
							k++;
						}
					}else if(methodType->str[k] == '['){
						parametroOrder.push('a');
						numParametros++;
						while(methodType->str[k] == '['){
							k++;
						}
						k++;
					}

				}else if((methodType->str[k] == 'D') || (methodType->str[k] == 'J')){

					parametroOrder.push('b');
					numParametros += 2;
				}
				k++;
			}
			while(!parametroOrder.empty()){
				if(parametroOrder.top() == 'a'){

					parametroArray.push(this->frameStack.top().operandStack.top());
					this->frameStack.top().operandStack.pop();

					parametroOrder.pop();

				}else if(parametroOrder.top() == 'b'){

					int upperAux = this->frameStack.top().operandStack.top();
					this->frameStack.top().operandStack.pop();

					int lowerAux = this->frameStack.top().operandStack.top();
					this->frameStack.top().operandStack.pop();

					parametroArray.push(lowerAux);
					parametroArray.push(upperAux);

					parametroOrder.pop();

				}
			}
			this->frameStack.push(Frame(methodName->str, methodArea->classArray[i],(int) i));
			if(!parametroArray.empty()){
				int index = 0;
				for(int i = 0; i < numParametros ;i++){
					this->frameStack.top().arrayLocalVariable[index] = parametroArray.top();
					std::cout << parametroArray.top() << " - " << numParametros;
					parametroArray.pop();
					index++;
				}
			}
			break;
		}
	}

#if DEBUGMODE == 1
	std::cout << " ++++++++++++++++++++++++++++ " << this->frameStack.size() << " ++++++++++++++++++++++++++++	" << std::endl;
	std::cout << " 	Class: " << className->str << " - Nome do metodo: " << methodName->str << " - Tipo do metodo: " << methodType->str << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_new(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_new -- completo" << std::endl;
#endif

	unsigned short index = 0;
	char* auxshort = ( char* ) &index;
	auxshort[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+1];
	auxshort[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+2];
	//std::cout << this->frameStack.top().constantPool[(unsigned int) index-1]->tag;
	if(this->frameStack.top().constantPool->at((unsigned int) index-1)->tag == 7){

		Object* newObject;

		ClassRefConstantPool* classRef = static_cast<ClassRefConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) index-1));
		StringConstantPool* className = static_cast<StringConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) classRef->nameIndex-1));

		for(unsigned int i=0; i <= methodArea->classArray.size(); i++){
			if(i == methodArea->classArray.size()){
				methodArea->classArray.push_back(new ClassReader);
				methodArea->classArray.back()->loadClassFileInfo(className->str + ".class");
			}
			if(static_cast<StringConstantPool*>(methodArea->classArray[i]->constantPoolArray[static_cast<ClassRefConstantPool*>(methodArea->classArray[i]->constantPoolArray[methodArea->classArray[i]->thisClassIndex-1])->nameIndex-1])->str == className->str){

				newObject = new Object;

				newObject->classIndex = i;

				#if DEBUGMODE == 1
					std::cout << "	Objeto criado, index da classe: " << newObject->classIndex << " Class name: " << className->str << std::endl << std::endl;
				#endif

				break;
			}
		}


		this->frameStack.top().operandStack.push((int) newObject);

	}else if(this->frameStack.top().constantPool->at((unsigned int) index-1)->tag == 11){
#if DEBUGMODE == 1
		InterMethodRefConstantPool* interMethodRef = static_cast<InterMethodRefConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) index-1));
		ClassRefConstantPool* classRef = static_cast<ClassRefConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) interMethodRef->classRefIndex-1));
		StringConstantPool* className = static_cast<StringConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) classRef->nameIndex-1));
		NameTypeConstantPool* nameType = static_cast<NameTypeConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) interMethodRef->nameTypeIndex-1));
		StringConstantPool* interMethodName = static_cast<StringConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) nameType->nameIndex-1));
		StringConstantPool* interMethodType = static_cast<StringConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) nameType->type-1));
#endif
		#if DEBUGMODE == 1
				std::cout << "	" << className->str << " - " << interMethodName->str << " - " << interMethodType->str << std::endl;
				std::cout << " exception: InstantiationError " << std::endl << std::endl;
		#endif

	}


	this->frameStack.top().cp+=3;
}

void RuntimeEngine::i_invokespecial(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_invokespecial -- linha: " << __LINE__ << std::endl;
#endif
	unsigned short index = 0;
	char* auxshort = ( char* ) &index;
	auxshort[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+1];
	auxshort[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+2];
	MethodRefConstantPool* methodRef = static_cast<MethodRefConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) index-1));
	ClassRefConstantPool* classRef = static_cast<ClassRefConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) methodRef->classRefIndex-1));
	StringConstantPool* className = static_cast<StringConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) classRef->nameIndex-1));
	NameTypeConstantPool* nameType = static_cast<NameTypeConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) methodRef->nameTypeIndex-1));
	StringConstantPool* methodName = static_cast<StringConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) nameType->nameIndex-1));
	StringConstantPool* methodType = static_cast<StringConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) nameType->type-1));

	if((className->str == "java/lang/Object") && (methodName->str == "<init>")){
		#if DEBUGMODE == 1
			std::cout << " 	Class: " << className->str << " - Nome do metodo: " << methodName->str << " - Tipo do metodo: " << methodType->str << std::endl << std::endl;
		#endif
		this->frameStack.top().operandStack.pop();
		this->frameStack.top().cp +=3;
		return;
	}

	int k = 0, numParametros = 0;
	std::stack<int> parametroArray;
	std::stack<char> parametroOrder;
	while (methodType->str[k] != ')'){
		if((methodType->str[k] == 'I') || (methodType->str[k] == 'B') || (methodType->str[k] == 'C') || (methodType->str[k] == 'F') || (methodType->str[k] == 'S') || (methodType->str[k] == 'Z') || (methodType->str[k] == 'L') || (methodType->str[k] == '[')){
			parametroOrder.push('a');
			numParametros++;

			if(methodType->str[k] == 'L'){
				parametroOrder.push('a');
				numParametros++;
				while(methodType->str[k] != ';'){
					k++;
				}
			}else if(methodType->str[k] == '['){
				parametroOrder.push('a');
				numParametros++;
				while(methodType->str[k] == '['){
					k++;
				}
				k++;
			}

		}else if((methodType->str[k] == 'D') || (methodType->str[k] == 'J')){

			parametroOrder.push('b');
			numParametros += 2;
		}
		k++;
	}
	while(!parametroOrder.empty()){
		if(parametroOrder.top() == 'a'){

			parametroArray.push(this->frameStack.top().operandStack.top());
			this->frameStack.top().operandStack.pop();

			parametroOrder.pop();

		}else if(parametroOrder.top() == 'b'){

			int upperAux = this->frameStack.top().operandStack.top();
			this->frameStack.top().operandStack.pop();

			int lowerAux = this->frameStack.top().operandStack.top();
			this->frameStack.top().operandStack.pop();

			parametroArray.push(lowerAux);
			parametroArray.push(upperAux);

			parametroOrder.pop();

		}
	}



	int objRef = this->frameStack.top().operandStack.top();

	this->frameStack.top().operandStack.pop();

	Object* aux = (Object*) objRef;

	this->frameStack.top().cp +=3;

	for(int j = 0; j <= methodArea->classArray[aux->classIndex]->methodCount; j++){

		if(j == methodArea->classArray[aux->classIndex]->methodCount){

			ClassRefConstantPool* superClassRef = static_cast<ClassRefConstantPool*>(methodArea->classArray[aux->classIndex]->constantPoolArray[(methodArea->classArray[aux->classIndex]->superClassIndex)-1]);
			StringConstantPool* superClassName = static_cast<StringConstantPool*>(methodArea->classArray[aux->classIndex]->constantPoolArray[superClassRef->nameIndex]);

			for(unsigned int i=0; i <= methodArea->classArray.size(); i++){
				if(i == methodArea->classArray.size()){
					methodArea->classArray.push_back(new ClassReader);
					methodArea->classArray.back()->loadClassFileInfo(superClassName->str + ".class");
				}
				if(static_cast<StringConstantPool*>(methodArea->classArray[i]->constantPoolArray[static_cast<ClassRefConstantPool*>(methodArea->classArray[i]->constantPoolArray[methodArea->classArray[i]->thisClassIndex-1])->nameIndex-1])->str == superClassName->str){
					this->frameStack.push(Frame(methodName->str, methodArea->classArray[i],objRef));
				}
			}
		}

		if(static_cast<StringConstantPool*>(methodArea->classArray[aux->classIndex]->constantPoolArray[(methodArea->classArray[aux->classIndex]->methodArray[j].getNameIndex()-1)])->str == methodName->str){
			this->frameStack.push(Frame(methodName->str, methodArea->classArray[aux->classIndex],objRef));
			break;
		}
	}

	if(!parametroArray.empty()){
		int index = 1;
		for(int i = 0; i < numParametros ;i++){
			this->frameStack.top().arrayLocalVariable[index] = parametroArray.top();
			std::cout << parametroArray.top() << " - " << numParametros;
			parametroArray.pop();
			index++;
		}
	}

#if DEBUGMODE == 1
	std::cout << " ++++++++++++++++++++++++++++ " << this->frameStack.size() << " ++++++++++++++++++++++++++++	" << std::endl;
	std::cout << " 	Class: " << className->str << " - Nome do metodo: " << methodName->str << " - Tipo do metodo: " << methodType->str << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_putstatic(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_putstatic -- linha: " << __LINE__ << std::endl;
#endif

	unsigned short index = 0;
	char* auxshort = ( char* ) &index;
	auxshort[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+1];
	auxshort[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+2];

	FieldRefConstantPool* fieldRef = static_cast<FieldRefConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) index-1));
	ClassRefConstantPool* classRef = static_cast<ClassRefConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) fieldRef->classRefIndex-1));
	StringConstantPool* className = static_cast<StringConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) classRef->nameIndex-1));
	NameTypeConstantPool* nameType = static_cast<NameTypeConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) fieldRef->nameTypeIndex-1));
	StringConstantPool* fieldName = static_cast<StringConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) nameType->nameIndex-1));
	StringConstantPool* fieldDescriptor = static_cast<StringConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) nameType->type-1));

#if DEBUGMODE == 1
	std::cout << " 	Class: " << className->str << " - Nome do fied: " << fieldName->str << " - descritor do field: " << fieldDescriptor->str << std::endl;
#endif

	for(unsigned int i=0; i <= methodArea->classArray.size(); i++){
		if(i == methodArea->classArray.size()){
			methodArea->classArray.push_back(new ClassReader);
			methodArea->classArray.back()->loadClassFileInfo(className->str + ".class");
		}
		if(static_cast<StringConstantPool*>(methodArea->classArray[i]->constantPoolArray[static_cast<ClassRefConstantPool*>(methodArea->classArray[i]->constantPoolArray[methodArea->classArray[i]->thisClassIndex-1])->nameIndex-1])->str == className->str){

			for(unsigned int j=0; j <= methodArea->classArray[i]->fieldCount; j++){

				if(static_cast<StringConstantPool*>(methodArea->classArray[i]->constantPoolArray[(methodArea->classArray[i]->fieldArray[j].getNameIndex())-1])->str == fieldName->str){

					if((fieldDescriptor->str[0] == 'D') || (fieldDescriptor->str[0] == 'J')){

						int upperAux = this->frameStack.top().operandStack.top();
						this->frameStack.top().operandStack.pop();

						int lowerAux = this->frameStack.top().operandStack.top();
						this->frameStack.top().operandStack.pop();

						methodArea->classArray[i]->fieldArray[j].value[0] = upperAux;
						methodArea->classArray[i]->fieldArray[j].value[1] = lowerAux;

						#if DEBUGMODE == 1
							std::cout << " 	bitset colocado no field static: " << methodArea->classArray[i]->fieldArray[j].value[0] << " - " << methodArea->classArray[i]->fieldArray[j].value[1] << std::endl << std::endl;
						#endif

					}else{

						int value = this->frameStack.top().operandStack.top();
						this->frameStack.top().operandStack.pop();

						methodArea->classArray[i]->fieldArray[j].value[0] = value;

						#if DEBUGMODE == 1
							std::cout << " 	bitset colocado no field static: " << methodArea->classArray[i]->fieldArray[j].value[0] << std::endl << std::endl;
						#endif

					}
					break;
				}
			}
			break;
		}
	}


	this->frameStack.top().cp+=3;
}

void RuntimeEngine::i_getstatic(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_getstatic -- linha: " << __LINE__ << std::endl;
#endif

	unsigned short index = 0;
	char* auxshort = ( char* ) &index;
	auxshort[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+1];
	auxshort[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+2];

	FieldRefConstantPool* fieldRef = static_cast<FieldRefConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) index-1));
	ClassRefConstantPool* classRef = static_cast<ClassRefConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) fieldRef->classRefIndex-1));
	StringConstantPool* className = static_cast<StringConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) classRef->nameIndex-1));
	NameTypeConstantPool* nameType = static_cast<NameTypeConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) fieldRef->nameTypeIndex-1));
	StringConstantPool* fieldName = static_cast<StringConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) nameType->nameIndex-1));
	StringConstantPool* fieldDescriptor = static_cast<StringConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) nameType->type-1));

#if DEBUGMODE == 1
	std::cout << " 	Class: " << className->str << " - Nome do field: " << fieldName->str << " - descritor do field: " << fieldDescriptor->str << std::endl;
#endif

	if(className->str == "java/lang/System"){
		this->frameStack.top().operandStack.push(0);
		#if DEBUGMODE == 1
			std::cout << " 	dado push em 0 no operand stack pois java/lang/System nao esta sendo acessada" << std::endl << std::endl;
		#endif
	}else{
		for(unsigned int i=0; i <= methodArea->classArray.size(); i++){
			if(i == methodArea->classArray.size()){
				methodArea->classArray.push_back(new ClassReader);
				methodArea->classArray.back()->loadClassFileInfo(className->str + ".class");
			}
			if(static_cast<StringConstantPool*>(methodArea->classArray[i]->constantPoolArray[static_cast<ClassRefConstantPool*>(methodArea->classArray[i]->constantPoolArray[methodArea->classArray[i]->thisClassIndex-1])->nameIndex-1])->str == className->str){

				for(unsigned int j=0; j <= methodArea->classArray[i]->fieldCount; j++){

					if(static_cast<StringConstantPool*>(methodArea->classArray[i]->constantPoolArray[(methodArea->classArray[i]->fieldArray[j].getNameIndex())-1])->str == fieldName->str){

						if((fieldDescriptor->str[0] == 'D') || (fieldDescriptor->str[0] == 'J')){

							int upperAux, lowerAux;

							methodArea->classArray[i]->fieldArray[j].value[0] = upperAux;
							methodArea->classArray[i]->fieldArray[j].value[1] = lowerAux;

							this->frameStack.top().operandStack.push(lowerAux);
							this->frameStack.top().operandStack.push(upperAux);

							#if DEBUGMODE == 1
								std::cout << " 	bitset pushed: " << methodArea->classArray[i]->fieldArray[j].value[0] << " - " << methodArea->classArray[i]->fieldArray[j].value[1] << std::endl << std::endl;
							#endif

						}else{

							int value;

							methodArea->classArray[i]->fieldArray[j].value[0] = value;

							this->frameStack.top().operandStack.push(value);

							#if DEBUGMODE == 1
								std::cout << " 	bitset pushed: " << methodArea->classArray[i]->fieldArray[j].value[0] << std::endl << std::endl;
							#endif

						}
						break;
					}
				}
				break;
			}
		}
	}
	this->frameStack.top().cp+=3;
}

void RuntimeEngine::i_putfield(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_putfield -- linha: " << __LINE__ << std::endl;
#endif

	unsigned short index = 0;
	char* auxshort = ( char* ) &index;
	auxshort[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+1];
	auxshort[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+2];

	FieldRefConstantPool* fieldRef = static_cast<FieldRefConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) index-1));
	ClassRefConstantPool* classRef = static_cast<ClassRefConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) fieldRef->classRefIndex-1));
	StringConstantPool* className = static_cast<StringConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) classRef->nameIndex-1));
	NameTypeConstantPool* nameType = static_cast<NameTypeConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) fieldRef->nameTypeIndex-1));
	StringConstantPool* fieldName = static_cast<StringConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) nameType->nameIndex-1));
	StringConstantPool* fieldDescriptor = static_cast<StringConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) nameType->type-1));

#if DEBUGMODE == 1
	std::cout << " 	Class: " << className->str << " - Nome do field: " << fieldName->str << " - descritor do field: " << fieldDescriptor->str << std::endl;
#endif

	for(unsigned int i=0; i <= methodArea->classArray.size(); i++){
		if(i == methodArea->classArray.size()){
			methodArea->classArray.push_back(new ClassReader);
			methodArea->classArray.back()->loadClassFileInfo(className->str + ".class");
		}
		if(static_cast<StringConstantPool*>(methodArea->classArray[i]->constantPoolArray[static_cast<ClassRefConstantPool*>(methodArea->classArray[i]->constantPoolArray[methodArea->classArray[i]->thisClassIndex-1])->nameIndex-1])->str == className->str){

			for(unsigned int j=0; j < methodArea->classArray[i]->fieldCount; j++){

				if(static_cast<StringConstantPool*>(methodArea->classArray[i]->constantPoolArray[(methodArea->classArray[i]->fieldArray[j].getNameIndex())-1])->str == fieldName->str){

					if((fieldDescriptor->str[0] == 'D') || (fieldDescriptor->str[0] == 'J')){

						int upperAux = this->frameStack.top().operandStack.top();
						this->frameStack.top().operandStack.pop();

						int lowerAux = this->frameStack.top().operandStack.top();
						this->frameStack.top().operandStack.pop();

						int aux = this->frameStack.top().operandStack.top();
						this->frameStack.top().operandStack.pop();

						Object* objRef = (Object*) aux;

						for(unsigned int k=0; k <= objRef->fieldArray.size() ; k++){
							if(k == objRef->fieldArray.size()){
								FieldObj newfield;
								objRef->fieldArray.push_back(newfield);
								objRef->fieldArray.back().name = fieldName->str;
								objRef->fieldArray.back().descriptor = fieldDescriptor->str;
							}
							if(objRef->fieldArray[k].name == fieldName->str){
								objRef->fieldArray[k].value[0] = upperAux;
								objRef->fieldArray[k].value[1] = lowerAux;

								#if DEBUGMODE == 1
									std::cout << " 	bitset colocado no field: " << objRef->fieldArray[k].value[0] << " - " << objRef->fieldArray[k].value[1] << std::endl << std::endl;
								#endif

								break;
							}
						}
					}else{

						int value = this->frameStack.top().operandStack.top();
						this->frameStack.top().operandStack.pop();

						int aux = this->frameStack.top().operandStack.top();
						this->frameStack.top().operandStack.pop();

						Object* objRef = (Object*) aux;

						for(unsigned int k=0; k <= objRef->fieldArray.size() ; k++){
							if(k == objRef->fieldArray.size()){
								FieldObj newfield;
								objRef->fieldArray.push_back(newfield);
								objRef->fieldArray.back().name = fieldName->str;
								objRef->fieldArray.back().descriptor = fieldDescriptor->str;
							}
							if(objRef->fieldArray[k].name == fieldName->str){
								objRef->fieldArray[k].value[0] = value;

								#if DEBUGMODE == 1
									std::cout << " 	bitset colocado no field: " << objRef->fieldArray[k].value[0] << std::endl << std::endl;
								#endif

								break;
							}
						}
					}
					break;
				}
			}
			break;
		}
	}


	this->frameStack.top().cp+=3;
}

void RuntimeEngine::i_getfield(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_getfield -- linha: " << __LINE__ << std::endl;
#endif

	unsigned short index = 0;
	char* auxshort = ( char* ) &index;
	auxshort[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+1];
	auxshort[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+2];

	FieldRefConstantPool* fieldRef = static_cast<FieldRefConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) index-1));
	ClassRefConstantPool* classRef = static_cast<ClassRefConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) fieldRef->classRefIndex-1));
	StringConstantPool* className = static_cast<StringConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) classRef->nameIndex-1));
	NameTypeConstantPool* nameType = static_cast<NameTypeConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) fieldRef->nameTypeIndex-1));
	StringConstantPool* fieldName = static_cast<StringConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) nameType->nameIndex-1));
	StringConstantPool* fieldDescriptor = static_cast<StringConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) nameType->type-1));

#if DEBUGMODE == 1
	std::cout << " 	Class: " << className->str << " - Nome do field: " << fieldName->str << " - descritor do field: " << fieldDescriptor->str << std::endl;
#endif

	for(unsigned int i=0; i <= methodArea->classArray.size(); i++){
		if(i == methodArea->classArray.size()){
			methodArea->classArray.push_back(new ClassReader);
			methodArea->classArray.back()->loadClassFileInfo(className->str + ".class");
		}
		if(static_cast<StringConstantPool*>(methodArea->classArray[i]->constantPoolArray[static_cast<ClassRefConstantPool*>(methodArea->classArray[i]->constantPoolArray[methodArea->classArray[i]->thisClassIndex-1])->nameIndex-1])->str == className->str){

			for(unsigned int j=0; j < methodArea->classArray[i]->fieldCount; j++){

				if(static_cast<StringConstantPool*>(methodArea->classArray[i]->constantPoolArray[(methodArea->classArray[i]->fieldArray[j].getNameIndex())-1])->str == fieldName->str){

					if((fieldDescriptor->str[0] == 'D') || (fieldDescriptor->str[0] == 'J')){

						int aux = this->frameStack.top().operandStack.top();
						this->frameStack.top().operandStack.pop();

						Object* objRef = (Object*) aux;

						for(unsigned int k=0; k <= objRef->fieldArray.size() ; k++){

							if(objRef->fieldArray[k].name == fieldName->str){
								this->frameStack.top().operandStack.push(objRef->fieldArray[k].value[1]);
								this->frameStack.top().operandStack.push(objRef->fieldArray[k].value[0]);

								#if DEBUGMODE == 1
									std::cout << " 	bitset colocado no operand stack: " << objRef->fieldArray[k].value[0] << " - " << objRef->fieldArray[k].value[1] << std::endl << std::endl;
								#endif

								break;
							}
						}
					}else{

						int aux = this->frameStack.top().operandStack.top();
						this->frameStack.top().operandStack.pop();

						Object* objRef = (Object*) aux;

						for(unsigned int k=0; k <= objRef->fieldArray.size() ; k++){

							if(objRef->fieldArray[k].name == fieldName->str){
								this->frameStack.top().operandStack.push(objRef->fieldArray[k].value[0]);

								#if DEBUGMODE == 1
									std::cout << " 	bitset colocado no operandstack: " << objRef->fieldArray[k].value[0] << std::endl << std::endl;
								#endif

								break;
							}
						}
					}
					break;
				}
			}
			break;
		}
	}
	this->frameStack.top().cp+=3;
}

void RuntimeEngine::i_invokevirtual(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_invokevirtual -- linha: " << __LINE__ << std::endl;
#endif
	unsigned short index = 0;
	char* auxshort = ( char* ) &index;
	auxshort[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+1];
	auxshort[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+2];
	MethodRefConstantPool* methodRef = static_cast<MethodRefConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) index-1));
	ClassRefConstantPool* classRef = static_cast<ClassRefConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) methodRef->classRefIndex-1));
	StringConstantPool* className = static_cast<StringConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) classRef->nameIndex-1));
	NameTypeConstantPool* nameType = static_cast<NameTypeConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) methodRef->nameTypeIndex-1));
	StringConstantPool* methodName = static_cast<StringConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) nameType->nameIndex-1));
	StringConstantPool* methodType = static_cast<StringConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) nameType->type-1));

	if((className->str == "java/io/PrintStream") && (methodName->str == "println")){
		int k = 0;
		#if DEBUGMODE == 1
			std::cout << " 	println nao e acessado, foi usado o cout do c++:" << std::endl << "	" ;
		#endif
		while (methodType->str[k] != ')'){
			if(methodType->str[k] == 'I'){
				std::cout << this->frameStack.top().operandStack.top() << std::endl;
				this->frameStack.top().operandStack.pop();
				break;
			}else if(methodType->str[k] == 'Z'){
				if( this->frameStack.top().operandStack.top() == 0){
					std::cout << "false" << std::endl;
					break;
				}else{
					std::cout << "true" << std::endl;
					break;
				}
				this->frameStack.top().operandStack.pop();
			}else if(methodType->str[k] == 'D'){

				double doublenum;
				unsigned int intnum, intnum2;

				intnum = this->frameStack.top().operandStack.top();
				this->frameStack.top().operandStack.pop();
				intnum2 = this->frameStack.top().operandStack.top();
				this->frameStack.top().operandStack.pop();

				char* auxint;
				char* auxint2;
				char* auxdouble;

				auxint = (char*) &intnum;
				auxint2 = (char*) &intnum2;
				auxdouble = (char*) &doublenum;

				auxdouble[0] = auxint[0];
				auxdouble[1] = auxint[1];
				auxdouble[2] = auxint[2];
				auxdouble[3] = auxint[3];
				auxdouble[4] = auxint2[0];
				auxdouble[5] = auxint2[1];
				auxdouble[6] = auxint2[2];
				auxdouble[7] = auxint2[3];

				std::cout << doublenum << std::endl;
				break;

			}else if(methodType->str[k] == 'J'){

				long long longnum;
				unsigned int intnum, intnum2;

				intnum = this->frameStack.top().operandStack.top();
				this->frameStack.top().operandStack.pop();
				intnum2 = this->frameStack.top().operandStack.top();
				this->frameStack.top().operandStack.pop();

				char* auxint;
				char* auxint2;
				char* auxlong;

				auxint = (char*) &intnum;
				auxint2 = (char*) &intnum2;
				auxlong = (char*) &longnum;

				auxlong[0] = auxint[0];
				auxlong[1] = auxint[1];
				auxlong[2] = auxint[2];
				auxlong[3] = auxint[3];
				auxlong[4] = auxint2[0];
				auxlong[5] = auxint2[1];
				auxlong[6] = auxint2[2];
				auxlong[7] = auxint2[3];

				std::cout << longnum << std::endl;
				break;

			}else if(methodType->str[k] == 'F'){

				float floatnum;
				unsigned int intnum;

				intnum = this->frameStack.top().operandStack.top();
				this->frameStack.top().operandStack.pop();

				char* auxint;
				char* auxfloat;

				auxint = (char*) &intnum;
				auxfloat = (char*) &floatnum;

				auxfloat[0] = auxint[0];
				auxfloat[1] = auxint[1];
				auxfloat[2] = auxint[2];
				auxfloat[3] = auxint[3];

				std::cout << floatnum << std::endl;
				break;

			}else if(methodType->str[k] == 'C'){
				int aux = this->frameStack.top().operandStack.top();
				this->frameStack.top().operandStack.pop();

				std::cout << (unsigned char) aux << std::endl;
				break;
			}else if(methodType->str[k] == 'S'){
				int aux = this->frameStack.top().operandStack.top();
				this->frameStack.top().operandStack.pop();

				std::cout << (short) aux << std::endl;
				break;
			}else if(methodType->str[k] == 'B'){
				int aux = this->frameStack.top().operandStack.top();
				this->frameStack.top().operandStack.pop();

				std::cout <<(int)(char) aux << std::endl;
				break;
			}else if(methodType->str[k] == 'L'){

				int aux = this->frameStack.top().operandStack.top();
				this->frameStack.top().operandStack.pop();

				std::string* str = (std::string*) aux;

				std::cout << *str << std::endl;
				break;
			}
			k++;
		}

		#if DEBUGMODE == 1
			std::cout << std::endl << std::endl;
		#endif

		this->frameStack.top().operandStack.pop();
		this->frameStack.top().cp +=3;
		return;
	}

	int k = 0, numParametros = 0;
	std::stack<int> parametroArray;
	std::stack<char> parametroOrder;
	while (methodType->str[k] != ')'){
		if((methodType->str[k] == 'I') || (methodType->str[k] == 'B') || (methodType->str[k] == 'C') || (methodType->str[k] == 'F') || (methodType->str[k] == 'S') || (methodType->str[k] == 'Z') || (methodType->str[k] == 'L') || (methodType->str[k] == '[')){
			parametroOrder.push('a');
			numParametros++;

			if(methodType->str[k] == 'L'){
				parametroOrder.push('a');
				numParametros++;
				while(methodType->str[k] != ';'){
					k++;
				}
			}else if(methodType->str[k] == '['){
				parametroOrder.push('a');
				numParametros++;
				while(methodType->str[k] == '['){
					k++;
				}
				k++;
			}

		}else if((methodType->str[k] == 'D') || (methodType->str[k] == 'J')){

			parametroOrder.push('b');
			numParametros += 2;
		}
		k++;
	}
	while(!parametroOrder.empty()){
		if(parametroOrder.top() == 'a'){

			parametroArray.push(this->frameStack.top().operandStack.top());
			this->frameStack.top().operandStack.pop();

			parametroOrder.pop();

		}else if(parametroOrder.top() == 'b'){

			int upperAux = this->frameStack.top().operandStack.top();
			this->frameStack.top().operandStack.pop();

			int lowerAux = this->frameStack.top().operandStack.top();
			this->frameStack.top().operandStack.pop();

			parametroArray.push(lowerAux);
			parametroArray.push(upperAux);

			parametroOrder.pop();

		}
	}

	int objRef = this->frameStack.top().operandStack.top();

	this->frameStack.top().operandStack.pop();

	Object* aux = (Object*) objRef;

	this->frameStack.top().cp +=3;

	for(int j = 0; j <= methodArea->classArray[aux->classIndex]->methodCount; j++){

		if(j == methodArea->classArray[aux->classIndex]->methodCount){

			ClassRefConstantPool* superClassRef = static_cast<ClassRefConstantPool*>(methodArea->classArray[aux->classIndex]->constantPoolArray[(methodArea->classArray[aux->classIndex]->superClassIndex)-1]);
			StringConstantPool* superClassName = static_cast<StringConstantPool*>(methodArea->classArray[aux->classIndex]->constantPoolArray[superClassRef->nameIndex]);

			for(unsigned int i=0; i <= methodArea->classArray.size(); i++){
				if(i == methodArea->classArray.size()){
					methodArea->classArray.push_back(new ClassReader);
					methodArea->classArray.back()->loadClassFileInfo(superClassName->str + ".class");
				}
				if(static_cast<StringConstantPool*>(methodArea->classArray[i]->constantPoolArray[static_cast<ClassRefConstantPool*>(methodArea->classArray[i]->constantPoolArray[methodArea->classArray[i]->thisClassIndex-1])->nameIndex-1])->str == superClassName->str){
					this->frameStack.push(Frame(methodName->str, methodArea->classArray[i],objRef));
				}
			}


		}

		if(static_cast<StringConstantPool*>(methodArea->classArray[aux->classIndex]->constantPoolArray[(methodArea->classArray[aux->classIndex]->methodArray[j].getNameIndex()-1)])->str == methodName->str){
			this->frameStack.push(Frame(methodName->str, methodArea->classArray[aux->classIndex],objRef));
			break;
		}
	}

	if(!parametroArray.empty()){
		int index = 1;
		for(int i = 0; i < numParametros ;i++){
			this->frameStack.top().arrayLocalVariable[index] = parametroArray.top();
			std::cout << parametroArray.top() << " - " << numParametros;
			parametroArray.pop();
			index++;
		}
	}

#if DEBUGMODE == 1
	std::cout << " ++++++++++++++++++++++++++++ " << this->frameStack.size() << " ++++++++++++++++++++++++++++	" << std::endl;
	std::cout << " 	Class: " << className->str << " - Nome do metodo: " << methodName->str << " - Tipo do metodo: " << methodType->str << std::endl << std::endl;
#endif

}

void RuntimeEngine::i_wide(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_wide -- linha: " << __LINE__ << std::endl;
#endif
	this->wideFlag = true;
	this->frameStack.top().cp++;
#if DEBUGMODE == 1
	std::cout << "	wideFlag setado como true, prox instruc ser� modificada" << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_ldc(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_ldc -- linha: " << __LINE__ << std::endl;
#endif

	unsigned char index = (unsigned short) this->frameStack.top().bytecode[this->frameStack.top().cp+1];

	if(this->frameStack.top().constantPool->at((unsigned int) index-1)->tag == 3){

		this->frameStack.top().operandStack.push(static_cast<IntConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) index-1))->value);

		#if DEBUGMODE == 1
			std::cout << "	int colocado no operandStack: " << this->frameStack.top().operandStack.top() << std::endl << std::endl;
		#endif

	}else if(this->frameStack.top().constantPool->at((unsigned int) index-1)->tag == 4){

		float auxFloat = static_cast<FloatConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) index-1))->value;

		int auxInt;

		copyBitsetDword((char*) &auxInt, (char*) &auxFloat);

		this->frameStack.top().operandStack.push(auxInt);

		#if DEBUGMODE == 1
			std::cout << "	float colocado no operandStack: " << auxFloat << std::endl << std::endl;
		#endif

	}else if(this->frameStack.top().constantPool->at((unsigned int) index-1)->tag == 8){

		if(static_cast<StringRefConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) index-1))->init){
			this->frameStack.top().operandStack.push((int)&(static_cast<StringRefConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) index-1))->str));
		}else{
			static_cast<StringRefConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) index-1))->str = static_cast<StringConstantPool*>(this->frameStack.top().constantPool->at((static_cast<StringRefConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) index-1))->strIndex)-1))->str;
			this->frameStack.top().operandStack.push((int)&(static_cast<StringRefConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) index-1))->str));
			static_cast<StringRefConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) index-1))->init = true;
		}
		#if DEBUGMODE == 1
			std::cout << "	reference colocado no operandStack: " << this->frameStack.top().operandStack.top() << " - string que ela aponta: " << *((std::string*)this->frameStack.top().operandStack.top()) << std::endl << std::endl;
		#endif
	}else if(this->frameStack.top().constantPool->at((unsigned int) index-1)->tag == 7){

		ClassRefConstantPool* classRef = static_cast<ClassRefConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) index-1));
		StringConstantPool* className = static_cast<StringConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) classRef->nameIndex-1));

		for(unsigned int i=0; i <= methodArea->classArray.size(); i++){
			if(i == methodArea->classArray.size()){
				methodArea->classArray.push_back(new ClassReader);
				methodArea->classArray.back()->loadClassFileInfo(className->str + ".class");
			}
			if(static_cast<StringConstantPool*>(methodArea->classArray[i]->constantPoolArray[static_cast<ClassRefConstantPool*>(methodArea->classArray[i]->constantPoolArray[methodArea->classArray[i]->thisClassIndex-1])->nameIndex-1])->str == className->str){

				this->frameStack.top().operandStack.push(i);

				#if DEBUGMODE == 1
					std::cout << "	reference colocado no operandStack(index no stack de classes do methodArea): " << this->frameStack.top().operandStack.top() << std::endl << std::endl;
				#endif

				break;
			}
		}
	}else if(this->frameStack.top().constantPool->at((unsigned int) index-1)->tag == 10){
		#if DEBUGMODE == 1
			std::cout << "	method ldc ainda n�o implementado " << this->frameStack.top().operandStack.top() << std::endl << std::endl;
		#endif
	}else if(this->frameStack.top().constantPool->at((unsigned int) index-1)->tag == 15){
		#if DEBUGMODE == 1
			std::cout << "	handle ldc ainda n�o implementado " << this->frameStack.top().operandStack.top() << std::endl << std::endl;
		#endif
	}

	this->frameStack.top().cp +=2;

}

void RuntimeEngine::i_ldc_w(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_ldc_w -- linha: " << __LINE__ << std::endl;
#endif

	unsigned short index = 0;
	char* auxshort = ( char* ) &index;
	auxshort[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+1];
	auxshort[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+2];

	if(this->frameStack.top().constantPool->at((unsigned int) index-1)->tag == 3){

		this->frameStack.top().operandStack.push(static_cast<IntConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) index-1))->value);

		#if DEBUGMODE == 1
			std::cout << "	int colocado no operandStack: " << this->frameStack.top().operandStack.top() << std::endl << std::endl;
		#endif

	}else if(this->frameStack.top().constantPool->at((unsigned int) index-1)->tag == 4){

		float auxFloat = static_cast<FloatConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) index-1))->value;

		int auxInt;

		copyBitsetDword((char*) &auxInt, (char*) &auxFloat);

		this->frameStack.top().operandStack.push(auxInt);

		#if DEBUGMODE == 1
			std::cout << "	float colocado no operandStack: " << auxFloat << std::endl << std::endl;
		#endif

	}else if(this->frameStack.top().constantPool->at((unsigned int) index-1)->tag == 8){

		if(static_cast<StringRefConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) index-1))->init){
			this->frameStack.top().operandStack.push((int)&(static_cast<StringRefConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) index-1))->str));
		}else{
			static_cast<StringRefConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) index-1))->str = static_cast<StringConstantPool*>(this->frameStack.top().constantPool->at((static_cast<StringRefConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) index-1))->strIndex)-1))->str;
			this->frameStack.top().operandStack.push((int)&(static_cast<StringRefConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) index-1))->str));
			static_cast<StringRefConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) index-1))->init = true;
		}
		#if DEBUGMODE == 1
			std::cout << "	reference colocado no operandStack: " << this->frameStack.top().operandStack.top() << " - string que ela aponta: " << *((std::string*)this->frameStack.top().operandStack.top()) << std::endl << std::endl;
		#endif
	}else if(this->frameStack.top().constantPool->at((unsigned int) index-1)->tag == 7){

		ClassRefConstantPool* classRef = static_cast<ClassRefConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) index-1));
		StringConstantPool* className = static_cast<StringConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) classRef->nameIndex-1));

		for(unsigned int i=0; i <= methodArea->classArray.size(); i++){
			if(i == methodArea->classArray.size()){
				methodArea->classArray.push_back(new ClassReader);
				methodArea->classArray.back()->loadClassFileInfo(className->str + ".class");
			}
			if(static_cast<StringConstantPool*>(methodArea->classArray[i]->constantPoolArray[static_cast<ClassRefConstantPool*>(methodArea->classArray[i]->constantPoolArray[methodArea->classArray[i]->thisClassIndex-1])->nameIndex-1])->str == className->str){

				this->frameStack.top().operandStack.push(i);

				#if DEBUGMODE == 1
					std::cout << "	reference colocado no operandStack(index no stack de classes do methodArea): " << this->frameStack.top().operandStack.top() << std::endl << std::endl;
				#endif

				break;
			}
		}
	}else if(this->frameStack.top().constantPool->at((unsigned int) index-1)->tag == 10){
		#if DEBUGMODE == 1
			std::cout << "	method ldc ainda n�o implementado " << this->frameStack.top().operandStack.top() << std::endl << std::endl;
		#endif
	}else if(this->frameStack.top().constantPool->at((unsigned int) index-1)->tag == 15){
		#if DEBUGMODE == 1
			std::cout << "	handle ldc ainda n�o implementado " << this->frameStack.top().operandStack.top() << std::endl << std::endl;
		#endif
	}

	this->frameStack.top().cp +=3;

}

void RuntimeEngine::i_ldc2_w(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_ldc2_w -- linha: " << __LINE__ << std::endl;
#endif

	unsigned short index = 0;
	char* auxshort = ( char* ) &index;
	auxshort[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+1];
	auxshort[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+2];

	if(this->frameStack.top().constantPool->at((unsigned int) index-1)->tag == 5){

		long long auxLong = static_cast<LongConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) index-1))->value;

		int upperAux, lowerAux;

		splitQWord((char*)&auxLong, (char*)&upperAux, (char*)&lowerAux);

		this->frameStack.top().operandStack.push(lowerAux);
		this->frameStack.top().operandStack.push(upperAux);

		#if DEBUGMODE == 1
			std::cout << "	long colocado no operandStack: " << auxLong << std::endl << std::endl;
		#endif

	}else if(this->frameStack.top().constantPool->at((unsigned int) index-1)->tag == 6){

		double auxDouble = static_cast<DoubleConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) index-1))->value;

		int upperAux, lowerAux;

		splitQWord((char*)&auxDouble, (char*)&upperAux, (char*)&lowerAux);

		this->frameStack.top().operandStack.push(lowerAux);
		this->frameStack.top().operandStack.push(upperAux);

		#if DEBUGMODE == 1
			std::cout << "	double colocado no operandStack: " << auxDouble << std::endl << std::endl;
		#endif

	}

	this->frameStack.top().cp +=3;

}

void RuntimeEngine::i_invokeinterface(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_invokeinterface -- linha: " << __LINE__ << std::endl;
#endif
	unsigned short index = 0;
	char* auxshort = ( char* ) &index;
	auxshort[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+1];
	auxshort[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+2];
	InterMethodRefConstantPool* methodRef = static_cast<InterMethodRefConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) index-1));
#if DEBUGMODE == 1
	ClassRefConstantPool* classRef = static_cast<ClassRefConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) methodRef->classRefIndex-1));
	StringConstantPool* className = static_cast<StringConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) classRef->nameIndex-1));
#endif
	NameTypeConstantPool* nameType = static_cast<NameTypeConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) methodRef->nameTypeIndex-1));
	StringConstantPool* methodName = static_cast<StringConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) nameType->nameIndex-1));
	StringConstantPool* methodType = static_cast<StringConstantPool*>(this->frameStack.top().constantPool->at((unsigned int) nameType->type-1));

	int k = 0, numParametros = 0;
	std::stack<int> parametroArray;
	std::stack<char> parametroOrder;
	while (methodType->str[k] != ')'){
		if((methodType->str[k] == 'I') || (methodType->str[k] == 'B') || (methodType->str[k] == 'C') || (methodType->str[k] == 'F') || (methodType->str[k] == 'S') || (methodType->str[k] == 'Z') || (methodType->str[k] == 'L') || (methodType->str[k] == '[')){
			parametroOrder.push('a');
			numParametros++;

			if(methodType->str[k] == 'L'){
				parametroOrder.push('a');
				numParametros++;
				while(methodType->str[k] != ';'){
					k++;
				}
			}else if(methodType->str[k] == '['){
				parametroOrder.push('a');
				numParametros++;
				while(methodType->str[k] == '['){
					k++;
				}
				k++;
			}

		}else if((methodType->str[k] == 'D') || (methodType->str[k] == 'J')){

			parametroOrder.push('b');
			numParametros += 2;
		}
		k++;
	}
	while(!parametroOrder.empty()){
		if(parametroOrder.top() == 'a'){

			parametroArray.push(this->frameStack.top().operandStack.top());
			this->frameStack.top().operandStack.pop();

			parametroOrder.pop();

		}else if(parametroOrder.top() == 'b'){

			int upperAux = this->frameStack.top().operandStack.top();
			this->frameStack.top().operandStack.pop();

			int lowerAux = this->frameStack.top().operandStack.top();
			this->frameStack.top().operandStack.pop();

			parametroArray.push(lowerAux);
			parametroArray.push(upperAux);

			parametroOrder.pop();

		}
	}
	int objRef = this->frameStack.top().operandStack.top();

	this->frameStack.top().operandStack.pop();

	Object* aux = (Object*) objRef;

	this->frameStack.top().cp +=5;

	for(int j = 0; j <= methodArea->classArray[aux->classIndex]->methodCount; j++){

		if(j == methodArea->classArray[aux->classIndex]->methodCount){

			ClassRefConstantPool* superClassRef = static_cast<ClassRefConstantPool*>(methodArea->classArray[aux->classIndex]->constantPoolArray[(methodArea->classArray[aux->classIndex]->superClassIndex)-1]);
			StringConstantPool* superClassName = static_cast<StringConstantPool*>(methodArea->classArray[aux->classIndex]->constantPoolArray[superClassRef->nameIndex]);

			for(unsigned int i=0; i <= methodArea->classArray.size(); i++){
				if(i == methodArea->classArray.size()){
					methodArea->classArray.push_back(new ClassReader);
					methodArea->classArray.back()->loadClassFileInfo(superClassName->str + ".class");
				}
				if(static_cast<StringConstantPool*>(methodArea->classArray[i]->constantPoolArray[static_cast<ClassRefConstantPool*>(methodArea->classArray[i]->constantPoolArray[methodArea->classArray[i]->thisClassIndex-1])->nameIndex-1])->str == superClassName->str){
					this->frameStack.push(Frame(methodName->str, methodArea->classArray[i],objRef));
				}
			}


		}

		if(static_cast<StringConstantPool*>(methodArea->classArray[aux->classIndex]->constantPoolArray[(methodArea->classArray[aux->classIndex]->methodArray[j].getNameIndex()-1)])->str == methodName->str){
			this->frameStack.push(Frame(methodName->str, methodArea->classArray[aux->classIndex],objRef));
			break;
		}
	}

	if(!parametroArray.empty()){
		int index = 1;
		for(int i = 0; i < numParametros ;i++){
			this->frameStack.top().arrayLocalVariable[index] = parametroArray.top();
			std::cout << parametroArray.top() << " - " << numParametros;
			parametroArray.pop();
			index++;
		}
	}
#if DEBUGMODE == 1
	std::cout << " ++++++++++++++++++++++++++++ " << this->frameStack.size() << " ++++++++++++++++++++++++++++	" << std::endl;
	std::cout << " 	Class: " << className->str << " - Nome do metodo: " << methodName->str << " - Tipo do metodo: " << methodType->str << std::endl << std::endl;
#endif
}

int createArray(int countsIndex, std::vector<int>* counts){

	if(counts->data()[countsIndex] == 0){
		return 0;
	}else{
		std::vector<int>* refarray = new std::vector<int>;
		refarray->resize((counts->data()[countsIndex])+1);
		refarray->data()[0] = 1;
		for (int i = 1; i < counts->data()[countsIndex]+1 ; i++){
			refarray->data()[i] = createArray(countsIndex+1, counts);
		}

		return (int) refarray;
	}
}

void RuntimeEngine::i_multianewarray(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_multianewarray -- linha: " << __LINE__  << std::endl;
#endif
	unsigned short index = 0;
	char* auxshort = ( char* ) &index;
	auxshort[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+1];
	auxshort[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+2];

	unsigned char dimensions = this->frameStack.top().bytecode[this->frameStack.top().cp+3];

	std::vector<int> counts;

	for(int i = 0; i < dimensions ; i++){
		counts.push_back(this->frameStack.top().operandStack.top());
		this->frameStack.top().operandStack.pop();

		if(counts.back() == 0){
			break;
		}
	}
	counts.push_back(0);
	int refarrayfinal = createArray(0, &counts);

	this->frameStack.top().operandStack.push(refarrayfinal);

	this->frameStack.top().cp+=4;

#if DEBUGMODE == 1
	std::cout<< "	reference para o novo array (topo do operandStack): "  << this->frameStack.top().operandStack.top() << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_tableswitch(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_tableswitch -- linha: " << __LINE__  << std::endl;
#endif

	int auxOffset = (4-((this->frameStack.top().cp+1)%4));

	if(auxOffset == 4){
		auxOffset = 0;
	}

	int defaultBytes = 0;
	char* auxint = ( char* ) &defaultBytes;
	auxint[3] = this->frameStack.top().bytecode[this->frameStack.top().cp+auxOffset+1];
	auxint[2] = this->frameStack.top().bytecode[this->frameStack.top().cp+auxOffset+2];
	auxint[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+auxOffset+3];
	auxint[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+auxOffset+4];

	int lowbytes = 0;
	auxint = ( char* ) &lowbytes;
	auxint[3] = this->frameStack.top().bytecode[this->frameStack.top().cp+auxOffset+5];
	auxint[2] = this->frameStack.top().bytecode[this->frameStack.top().cp+auxOffset+6];
	auxint[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+auxOffset+7];
	auxint[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+auxOffset+8];

	int highbytes = 0;
	auxint = ( char* ) &highbytes;
	auxint[3] = this->frameStack.top().bytecode[this->frameStack.top().cp+auxOffset+9];
	auxint[2] = this->frameStack.top().bytecode[this->frameStack.top().cp+auxOffset+10];
	auxint[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+auxOffset+11];
	auxint[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+auxOffset+12];

	auxOffset += 12;

	int index = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	if((index < lowbytes) || (index > highbytes)){
		this->frameStack.top().cp += defaultBytes;
		index = -4;
	}else{
		index -= lowbytes;
		index *= 4;

		int branchOffset = 0;
		auxint = ( char* ) &branchOffset;
		auxint[3] = this->frameStack.top().bytecode[this->frameStack.top().cp+auxOffset+1+index];
		auxint[2] = this->frameStack.top().bytecode[this->frameStack.top().cp+auxOffset+2+index];
		auxint[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+auxOffset+3+index];
		auxint[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+auxOffset+4+index];

		this->frameStack.top().cp += branchOffset;
	}

#if DEBUGMODE == 1
	std::cout << "	index tomado(se index for -1 default jump foi tomado): " << index/4 << " - cp final: " << this->frameStack.top().cp << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_lookupswitch(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_lookupswitch -- linha: " << __LINE__  << std::endl;
#endif

	int auxOffset = (4-((this->frameStack.top().cp+1)%4));

	if(auxOffset == 4){
		auxOffset = 0;
	}

	int defaultBytes = 0;
	char* auxint = ( char* ) &defaultBytes;
	auxint[3] = this->frameStack.top().bytecode[this->frameStack.top().cp+auxOffset+1];
	auxint[2] = this->frameStack.top().bytecode[this->frameStack.top().cp+auxOffset+2];
	auxint[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+auxOffset+3];
	auxint[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+auxOffset+4];

	int npairs = 0;
	auxint = ( char* ) &npairs;
	auxint[3] = this->frameStack.top().bytecode[this->frameStack.top().cp+auxOffset+5];
	auxint[2] = this->frameStack.top().bytecode[this->frameStack.top().cp+auxOffset+6];
	auxint[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+auxOffset+7];
	auxint[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+auxOffset+8];


	auxOffset += 8;

	int key = this->frameStack.top().operandStack.top();
	this->frameStack.top().operandStack.pop();

	int i;
	for(i = 0; i < npairs ; i++){

		int match = 0;
		auxint = ( char* ) &match;
		auxint[3] = this->frameStack.top().bytecode[this->frameStack.top().cp+auxOffset+1+auxOffset];
		auxint[2] = this->frameStack.top().bytecode[this->frameStack.top().cp+auxOffset+2+auxOffset];
		auxint[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+auxOffset+3+auxOffset];
		auxint[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+auxOffset+4+auxOffset];

		if(key == match){
			int branchOffset = 0;
			auxint = ( char* ) &branchOffset;
			auxint[3] = this->frameStack.top().bytecode[this->frameStack.top().cp+auxOffset+5+auxOffset];
			auxint[2] = this->frameStack.top().bytecode[this->frameStack.top().cp+auxOffset+6+auxOffset];
			auxint[1] = this->frameStack.top().bytecode[this->frameStack.top().cp+auxOffset+7+auxOffset];
			auxint[0] = this->frameStack.top().bytecode[this->frameStack.top().cp+auxOffset+8+auxOffset];

			this->frameStack.top().cp += branchOffset;
			break;
		}else{
			auxOffset += 8;
		}

	}
	if(i == npairs){
		this->frameStack.top().cp += defaultBytes;
	}

#if DEBUGMODE == 1
	std::cout << "	key tomada: " << key << " - cp final: " << this->frameStack.top().cp << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_instanceof(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_instanceof -- linha: " << __LINE__   << std::endl;
#endif
	this->frameStack.top().cp++;
#if DEBUGMODE == 1
	std::cout << "	intruction opcional " << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_monitorenter(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_monitorenter -- linha: " << __LINE__   << std::endl;
#endif
	this->frameStack.top().cp++;
#if DEBUGMODE == 1
	std::cout << "	intruction opcional " << std::endl << std::endl;
#endif
}

void RuntimeEngine::i_monitorexit(){
#if DEBUGMODE == 1
	std::cout << " efetuado i_monitorexit -- linha: " << __LINE__   << std::endl;
#endif
	this->frameStack.top().cp++;
#if DEBUGMODE == 1
	std::cout << "	intruction opcional " << std::endl << std::endl;
#endif
}

/*
 *  parei aqui
 * 	copyBitsetDword((char*) &to, (char*) &from);
 * 	splitQWord((char*)&value, (char*)&upperAux, (char*)&lowerAux);
 * 	uniteDWords((char*)&value, (char*)&upperAux, (char*)&lowerAux);
 *
 * 	falta implementar:
 *
 *
 *
 */

void RuntimeEngine::initInstructionsVector() {
	this->instructionsVector.reserve(202);
    this->instructionsVector[0x00] = &RuntimeEngine::i_nop;
    this->instructionsVector[0x01] = &RuntimeEngine::i_aconst_null;
    this->instructionsVector[0x02] = &RuntimeEngine::i_iconst_m1;
    this->instructionsVector[0x03] = &RuntimeEngine::i_iconst_0;
    this->instructionsVector[0x04] = &RuntimeEngine::i_iconst_1;
    this->instructionsVector[0x05] = &RuntimeEngine::i_iconst_2;
    this->instructionsVector[0x06] = &RuntimeEngine::i_iconst_3;
    this->instructionsVector[0x07] = &RuntimeEngine::i_iconst_4;
    this->instructionsVector[0x08] = &RuntimeEngine::i_iconst_5;
    this->instructionsVector[0x09] = &RuntimeEngine::i_lconst_0;
    this->instructionsVector[0x0a] = &RuntimeEngine::i_lconst_1;
    this->instructionsVector[0x0b] = &RuntimeEngine::i_fconst_0;
    this->instructionsVector[0x0c] = &RuntimeEngine::i_fconst_1;
    this->instructionsVector[0x0d] = &RuntimeEngine::i_fconst_2;
    this->instructionsVector[0x0e] = &RuntimeEngine::i_dconst_0;
    this->instructionsVector[0x0f] = &RuntimeEngine::i_dconst_1;
    this->instructionsVector[0x10] = &RuntimeEngine::i_bipush;
    this->instructionsVector[0x11] = &RuntimeEngine::i_sipush;
    this->instructionsVector[0x12] = &RuntimeEngine::i_ldc;
    this->instructionsVector[0x13] = &RuntimeEngine::i_ldc_w;
    this->instructionsVector[0x14] = &RuntimeEngine::i_ldc2_w;
    this->instructionsVector[0x15] = &RuntimeEngine::i_iload;
    this->instructionsVector[0x16] = &RuntimeEngine::i_lload;
    this->instructionsVector[0x17] = &RuntimeEngine::i_fload;
    this->instructionsVector[0x18] = &RuntimeEngine::i_dload;
    this->instructionsVector[0x19] = &RuntimeEngine::i_aload;
    this->instructionsVector[0x1a] = &RuntimeEngine::i_iload_0;
    this->instructionsVector[0x1b] = &RuntimeEngine::i_iload_1;
    this->instructionsVector[0x1c] = &RuntimeEngine::i_iload_2;
    this->instructionsVector[0x1d] = &RuntimeEngine::i_iload_3;
    this->instructionsVector[0x1e] = &RuntimeEngine::i_lload_0;
    this->instructionsVector[0x1f] = &RuntimeEngine::i_lload_1;
    this->instructionsVector[0x20] = &RuntimeEngine::i_lload_2;
    this->instructionsVector[0x21] = &RuntimeEngine::i_lload_3;
    this->instructionsVector[0x22] = &RuntimeEngine::i_fload_0;
    this->instructionsVector[0x23] = &RuntimeEngine::i_fload_1;
    this->instructionsVector[0x24] = &RuntimeEngine::i_fload_2;
    this->instructionsVector[0x25] = &RuntimeEngine::i_fload_3;
    this->instructionsVector[0x26] = &RuntimeEngine::i_dload_0;
    this->instructionsVector[0x27] = &RuntimeEngine::i_dload_1;
    this->instructionsVector[0x28] = &RuntimeEngine::i_dload_2;
    this->instructionsVector[0x29] = &RuntimeEngine::i_dload_3;
    this->instructionsVector[0x2a] = &RuntimeEngine::i_aload_0;
    this->instructionsVector[0x2b] = &RuntimeEngine::i_aload_1;
    this->instructionsVector[0x2c] = &RuntimeEngine::i_aload_2;
    this->instructionsVector[0x2d] = &RuntimeEngine::i_aload_3;
    this->instructionsVector[0x2e] = &RuntimeEngine::i_iaload;
    this->instructionsVector[0x2f] = &RuntimeEngine::i_laload;
    this->instructionsVector[0x30] = &RuntimeEngine::i_faload;
    this->instructionsVector[0x31] = &RuntimeEngine::i_daload;
    this->instructionsVector[0x32] = &RuntimeEngine::i_aaload;
    this->instructionsVector[0x33] = &RuntimeEngine::i_baload;
    this->instructionsVector[0x34] = &RuntimeEngine::i_caload;
    this->instructionsVector[0x35] = &RuntimeEngine::i_saload;
    this->instructionsVector[0x36] = &RuntimeEngine::i_istore;
    this->instructionsVector[0x37] = &RuntimeEngine::i_lstore;
    this->instructionsVector[0x38] = &RuntimeEngine::i_fstore;
    this->instructionsVector[0x39] = &RuntimeEngine::i_dstore;
    this->instructionsVector[0x3a] = &RuntimeEngine::i_astore;
    this->instructionsVector[0x3b] = &RuntimeEngine::i_istore_0;
    this->instructionsVector[0x3c] = &RuntimeEngine::i_istore_1;
    this->instructionsVector[0x3d] = &RuntimeEngine::i_istore_2;
    this->instructionsVector[0x3e] = &RuntimeEngine::i_istore_3;
    this->instructionsVector[0x3f] = &RuntimeEngine::i_lstore_0;
    this->instructionsVector[0x40] = &RuntimeEngine::i_lstore_1;
    this->instructionsVector[0x41] = &RuntimeEngine::i_lstore_2;
    this->instructionsVector[0x42] = &RuntimeEngine::i_lstore_3;
    this->instructionsVector[0x43] = &RuntimeEngine::i_fstore_0;
    this->instructionsVector[0x44] = &RuntimeEngine::i_fstore_1;
    this->instructionsVector[0x45] = &RuntimeEngine::i_fstore_2;
    this->instructionsVector[0x46] = &RuntimeEngine::i_fstore_3;
    this->instructionsVector[0x47] = &RuntimeEngine::i_dstore_0;
    this->instructionsVector[0x48] = &RuntimeEngine::i_dstore_1;
    this->instructionsVector[0x49] = &RuntimeEngine::i_dstore_2;
    this->instructionsVector[0x4a] = &RuntimeEngine::i_dstore_3;
    this->instructionsVector[0x4b] = &RuntimeEngine::i_astore_0;
    this->instructionsVector[0x4c] = &RuntimeEngine::i_astore_1;
    this->instructionsVector[0x4d] = &RuntimeEngine::i_astore_2;
    this->instructionsVector[0x4e] = &RuntimeEngine::i_astore_3;
    this->instructionsVector[0x4f] = &RuntimeEngine::i_iastore;
    this->instructionsVector[0x50] = &RuntimeEngine::i_lastore;
    this->instructionsVector[0x51] = &RuntimeEngine::i_fastore;
    this->instructionsVector[0x52] = &RuntimeEngine::i_dastore;
    this->instructionsVector[0x53] = &RuntimeEngine::i_aastore;
    this->instructionsVector[0x54] = &RuntimeEngine::i_bastore;
    this->instructionsVector[0x55] = &RuntimeEngine::i_castore;
    this->instructionsVector[0x56] = &RuntimeEngine::i_sastore;
    this->instructionsVector[0x57] = &RuntimeEngine::i_pop;
    this->instructionsVector[0x58] = &RuntimeEngine::i_pop2;
    this->instructionsVector[0x59] = &RuntimeEngine::i_dup;
    this->instructionsVector[0x5a] = &RuntimeEngine::i_dup_x1;
    this->instructionsVector[0x5b] = &RuntimeEngine::i_dup_x2;
    this->instructionsVector[0x5c] = &RuntimeEngine::i_dup2;
    this->instructionsVector[0x5d] = &RuntimeEngine::i_dup2_x1;
    this->instructionsVector[0x5e] = &RuntimeEngine::i_dup2_x2;
    this->instructionsVector[0x5f] = &RuntimeEngine::i_swap;
    this->instructionsVector[0x60] = &RuntimeEngine::i_iadd;
    this->instructionsVector[0x61] = &RuntimeEngine::i_ladd;
    this->instructionsVector[0x62] = &RuntimeEngine::i_fadd;
     this->instructionsVector[0x63] = &RuntimeEngine::i_dadd;
     this->instructionsVector[0x64] = &RuntimeEngine::i_isub;
     this->instructionsVector[0x65] = &RuntimeEngine::i_lsub;
     this->instructionsVector[0x66] = &RuntimeEngine::i_fsub;
     this->instructionsVector[0x67] = &RuntimeEngine::i_dsub;
     this->instructionsVector[0x68] = &RuntimeEngine::i_imul;
     this->instructionsVector[0x69] = &RuntimeEngine::i_lmul;
     this->instructionsVector[0x6a] = &RuntimeEngine::i_fmul;
     this->instructionsVector[0x6b] = &RuntimeEngine::i_dmul;
     this->instructionsVector[0x6c] = &RuntimeEngine::i_idiv;
     this->instructionsVector[0x6d] = &RuntimeEngine::i_ldiv;
     this->instructionsVector[0x6e] = &RuntimeEngine::i_fdiv;
     this->instructionsVector[0x6f] = &RuntimeEngine::i_ddiv;
     this->instructionsVector[0x70] = &RuntimeEngine::i_irem;
     this->instructionsVector[0x71] = &RuntimeEngine::i_lrem;
     this->instructionsVector[0x72] = &RuntimeEngine::i_frem;
     this->instructionsVector[0x73] = &RuntimeEngine::i_drem;
     this->instructionsVector[0x74] = &RuntimeEngine::i_ineg;
     this->instructionsVector[0x75] = &RuntimeEngine::i_lneg;
     this->instructionsVector[0x76] = &RuntimeEngine::i_fneg;
     this->instructionsVector[0x77] = &RuntimeEngine::i_dneg;
     this->instructionsVector[0x78] = &RuntimeEngine::i_ishl;
     this->instructionsVector[0x79] = &RuntimeEngine::i_lshl;
     this->instructionsVector[0x7a] = &RuntimeEngine::i_ishr;
     this->instructionsVector[0x7b] = &RuntimeEngine::i_lshr;
     this->instructionsVector[0x7c] = &RuntimeEngine::i_iushr;
     this->instructionsVector[0x7d] = &RuntimeEngine::i_lushr;
     this->instructionsVector[0x7e] = &RuntimeEngine::i_iand;
     this->instructionsVector[0x7f] = &RuntimeEngine::i_land;
     this->instructionsVector[0x80] = &RuntimeEngine::i_ior;
     this->instructionsVector[0x81] = &RuntimeEngine::i_lor;
     this->instructionsVector[0x82] = &RuntimeEngine::i_ixor;
     this->instructionsVector[0x83] = &RuntimeEngine::i_lxor;
     this->instructionsVector[0x84] = &RuntimeEngine::i_iinc;
     this->instructionsVector[0x85] = &RuntimeEngine::i_i2l;
     this->instructionsVector[0x86] = &RuntimeEngine::i_i2f;
     this->instructionsVector[0x87] = &RuntimeEngine::i_i2d;
     this->instructionsVector[0x88] = &RuntimeEngine::i_l2i;
     this->instructionsVector[0x89] = &RuntimeEngine::i_l2f;
     this->instructionsVector[0x8a] = &RuntimeEngine::i_l2d;
     this->instructionsVector[0x8b] = &RuntimeEngine::i_f2i;
     this->instructionsVector[0x8c] = &RuntimeEngine::i_f2l;
     this->instructionsVector[0x8d] = &RuntimeEngine::i_f2d;
     this->instructionsVector[0x8e] = &RuntimeEngine::i_d2i;
     this->instructionsVector[0x8f] = &RuntimeEngine::i_d2l;
     this->instructionsVector[0x90] = &RuntimeEngine::i_d2f;
     this->instructionsVector[0x91] = &RuntimeEngine::i_i2b;
     this->instructionsVector[0x92] = &RuntimeEngine::i_i2c;
     this->instructionsVector[0x93] = &RuntimeEngine::i_i2s;
     this->instructionsVector[0x94] = &RuntimeEngine::i_lcmp;
     this->instructionsVector[0x95] = &RuntimeEngine::i_fcmpl;
     this->instructionsVector[0x96] = &RuntimeEngine::i_fcmpg;
     this->instructionsVector[0x97] = &RuntimeEngine::i_dcmpl;
     this->instructionsVector[0x98] = &RuntimeEngine::i_dcmpg;
     this->instructionsVector[0x99] = &RuntimeEngine::i_ifeq;
     this->instructionsVector[0x9a] = &RuntimeEngine::i_ifne;
     this->instructionsVector[0x9b] = &RuntimeEngine::i_iflt;
     this->instructionsVector[0x9c] = &RuntimeEngine::i_ifge;
     this->instructionsVector[0x9d] = &RuntimeEngine::i_ifgt;
     this->instructionsVector[0x9e] = &RuntimeEngine::i_ifle;
     this->instructionsVector[0x9f] = &RuntimeEngine::i_if_icmpeq;
     this->instructionsVector[0xa0] = &RuntimeEngine::i_if_icmpne;
     this->instructionsVector[0xa1] = &RuntimeEngine::i_if_icmplt;
     this->instructionsVector[0xa2] = &RuntimeEngine::i_if_icmpge;
     this->instructionsVector[0xa3] = &RuntimeEngine::i_if_icmpgt;
     this->instructionsVector[0xa4] = &RuntimeEngine::i_if_icmple;
     this->instructionsVector[0xa5] = &RuntimeEngine::i_if_acmpeq;
     this->instructionsVector[0xa6] = &RuntimeEngine::i_if_acmpne;
     this->instructionsVector[0xa7] = &RuntimeEngine::i_goto;
     this->instructionsVector[0xa8] = &RuntimeEngine::i_jsr;
     this->instructionsVector[0xa9] = &RuntimeEngine::i_ret;
     this->instructionsVector[0xaa] = &RuntimeEngine::i_tableswitch;
     this->instructionsVector[0xab] = &RuntimeEngine::i_lookupswitch;
     this->instructionsVector[0xac] = &RuntimeEngine::i_ireturn;
     this->instructionsVector[0xad] = &RuntimeEngine::i_lreturn;
     this->instructionsVector[0xae] = &RuntimeEngine::i_freturn;
     this->instructionsVector[0xaf] = &RuntimeEngine::i_dreturn;
     this->instructionsVector[0xb0] = &RuntimeEngine::i_areturn;
     this->instructionsVector[0xb1] = &RuntimeEngine::i_return;
     this->instructionsVector[0xb2] = &RuntimeEngine::i_getstatic;
     this->instructionsVector[0xb3] = &RuntimeEngine::i_putstatic;
     this->instructionsVector[0xb4] = &RuntimeEngine::i_getfield;
     this->instructionsVector[0xb5] = &RuntimeEngine::i_putfield;
     this->instructionsVector[0xb6] = &RuntimeEngine::i_invokevirtual;
     this->instructionsVector[0xb7] = &RuntimeEngine::i_invokespecial;
     this->instructionsVector[0xb8] = &RuntimeEngine::i_invokestatic;
     this->instructionsVector[0xb9] = &RuntimeEngine::i_invokeinterface;
     this->instructionsVector[0xbb] = &RuntimeEngine::i_new;
     this->instructionsVector[0xbc] = &RuntimeEngine::i_newarray;
     this->instructionsVector[0xbd] = &RuntimeEngine::i_anewarray;
     this->instructionsVector[0xbe] = &RuntimeEngine::i_arraylength;
     this->instructionsVector[0xbf] = &RuntimeEngine::i_athrow;
     this->instructionsVector[0xc0] = &RuntimeEngine::i_checkcast;
     this->instructionsVector[0xc1] = &RuntimeEngine::i_instanceof;
     this->instructionsVector[0xc2] = &RuntimeEngine::i_monitorenter;
     this->instructionsVector[0xc3] = &RuntimeEngine::i_monitorexit;
     this->instructionsVector[0xc4] = &RuntimeEngine::i_wide;
     this->instructionsVector[0xc5] = &RuntimeEngine::i_multianewarray;
     this->instructionsVector[0xc6] = &RuntimeEngine::i_ifnull;
     this->instructionsVector[0xc7] = &RuntimeEngine::i_ifnonnull;
     this->instructionsVector[0xc8] = &RuntimeEngine::i_goto_w;
     this->instructionsVector[0xc9] = &RuntimeEngine::i_jsr_w;
}
