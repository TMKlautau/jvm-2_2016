/*
 * Attributes.cpp
 *
 *  Created on: Nov 6, 2016
 *      Author: TitoKlautau
 */

#include "Attributes.h"

ObjAttribute::ObjAttribute(unsigned short nameIndexIn, unsigned int infoLengthIn, std::string  infoIn)
	:nameIndex(nameIndexIn), infoLength(infoLengthIn), info(infoIn){}

unsigned short ObjAttribute::getNameIndex(){
	return this->nameIndex;
}

unsigned int ObjAttribute::getInfoLength(){
	return this->infoLength;
}

std::string ObjAttribute::getInfo(){
	return this->info;
}

ConstantValueAttribute::ConstantValueAttribute(unsigned short nameIndexIn, unsigned int infoLengthIn, std::string  infoIn, unsigned short constantIndexIn)
	: ObjAttribute(nameIndexIn, infoLengthIn, infoIn), constantIndex(constantIndexIn){}

unsigned short ConstantValueAttribute::getConstantIndex(){
	return this->constantIndex;
}

CodeAttribute::CodeAttribute(unsigned short nameIndexIn, unsigned int infoLengthIn, std::string  infoIn, unsigned short maxStackIn, unsigned short maxLocalsIn, unsigned int codeArrayCountIn, std::string codeArrayIn, unsigned short exceptionTableCountIn, std::vector<ObjExceptionTable> exceptionTableArrayIn, unsigned short attributesCountIn, std::vector<ObjAttribute*> attributesArrayIn)
	: ObjAttribute(nameIndexIn, infoLengthIn, infoIn), maxStack(maxStackIn), maxLocals(maxLocalsIn), codeArrayCount(codeArrayCountIn), codeArray(codeArrayIn), exceptionTableCount(exceptionTableCountIn), exceptionTableArray(exceptionTableArrayIn), attributesCount(attributesCountIn){
	this->attributesArray = attributesArrayIn;
}

ObjExceptionTable::ObjExceptionTable(unsigned short startPcIn ,unsigned short endPcIn, unsigned short handlerPcIn, unsigned short catchTypeIn)
	: startPc(startPcIn), endPc(endPcIn), handlerPc(handlerPcIn), catchType(catchTypeIn){ }

unsigned short CodeAttribute::getMaxStack(){
	return this->maxStack;
}

unsigned short CodeAttribute::getMaxLocals(){
	return this->maxLocals;
}

unsigned int CodeAttribute::getCodeArrayCount(){
	return this->codeArrayCount;
}

std::string CodeAttribute::getCodeArray(){
	return this->codeArray;
}

unsigned short CodeAttribute::getExceptionTableCount(){
	return this->exceptionTableCount;
}

std::vector<ObjExceptionTable> CodeAttribute::getExceptionTableArray(){
	return this->exceptionTableArray;
}

unsigned short CodeAttribute::getAttributesCount(){
	return this->attributesCount;
}

std::vector<ObjAttribute*> CodeAttribute::getAttributesArray(){
	return this->attributesArray;
}

SourceFileAttribute::SourceFileAttribute(unsigned short nameIndexIn, unsigned int infoLengthIn, std::string  infoIn, unsigned short sourceFileIndexIn)
	: ObjAttribute(nameIndexIn, infoLengthIn, infoIn), sourceFileIndex(sourceFileIndexIn){}

unsigned short SourceFileAttribute::getSourceFileIndex(){
	return this->sourceFileIndex;
}
